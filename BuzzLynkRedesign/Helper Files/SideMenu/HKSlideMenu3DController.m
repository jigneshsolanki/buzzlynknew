//
//  HKSlideMenu3DController
//  SlideMenu3D
//
//  Created by Edgar on 4/6/15.
//  Copyright (c) 2015 hunk. All rights reserved.
//

#import "HKSlideMenu3DController.h"
#import "AppDelegate.h"
#import "Constants.h"

@interface HKSlideMenu3DController (){
    AppDelegate *appDelegate;
}

@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;
@property (nonatomic, strong) UIViewController *mainContainer;
@property (nonatomic, strong) UIViewController *menuContainer;
@property (nonatomic, strong) UIImageView *bgImageContainer;
@property (nonatomic, strong) UIPanGestureRecognizer *panGestureRecognizer;
@property (nonatomic, assign) CGPoint draggingPoint;
@property (nonatomic, assign) CGFloat distanceOpenMenu;


@end

@implementation HKSlideMenu3DController

- (void)didRotate:(NSNotification *)notification {
    
    
    CGRect fMain = _mainContainer.view.frame;
    
    if (CGRectGetMinX(fMain) == 0) {
        CALayer *layer = _menuContainer.view.layer;
        CATransform3D rotationAndPerspectiveTransform = CATransform3DIdentity;
        layer.transform = rotationAndPerspectiveTransform;
    }
}

- (void)setup {
    
    //int width = IS_IPAD ? 500 : 120;
    int width =  70;
    
    _distanceOpenMenu = self.view.frame.size.width-width;
    //_distanceOpenMenu = self.view.frame.size.width-width;
    //self.view.backgroundColor = [UIColor colorWithRed:0.0 green:102/255.0 blue:204/255.0 alpha:1.0];
    self.view.backgroundColor = [UIColor clearColor];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRotate:) name:UIDeviceOrientationDidChangeNotification object:nil];
    
    _bgImageContainer = [[UIImageView alloc] initWithFrame:self.view.bounds];
    _bgImageContainer.contentMode = UIViewContentModeScaleAspectFill;// UIViewContentModeTopLeft;
    //_bgImageContainer.layer.zPosition = IS_IPAD?-1000:-2000;
    _bgImageContainer.layer.zPosition = -2000;
    [self.view addSubview:_bgImageContainer];
    
    _menuContainer = [[UIViewController alloc] init];
    _menuContainer.view.layer.anchorPoint = CGPointMake(1.0, 0.5);
    _menuContainer.view.frame = self.view.bounds;
    //_menuContainer.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, 208.0, self.view.frame.size.height);

    _menuContainer.view.backgroundColor = [UIColor clearColor];
       [self addChildViewController:_menuContainer];
    [self.view addSubview:_menuContainer.view];
    [_menuContainer didMoveToParentViewController:self];
    
    _mainContainer = [[UIViewController alloc] init];
    _mainContainer.view.frame = self.view.bounds;
    _mainContainer.view.backgroundColor = [UIColor clearColor];
    [self addChildViewController:_mainContainer];
    [self.view addSubview:_mainContainer.view];
    [_mainContainer didMoveToParentViewController:self];
    
    [self addPanGestures];
    [self setBlackShadow:_mainContainer.view];

    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    [self setup];
    [self setNeedsStatusBarAppearanceUpdate];

}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (void)setBlackShadow:(UIView *)view {
    view.layer.masksToBounds = NO;
    view.layer.shadowColor = [UIColor blackColor].CGColor;
    view.layer.shadowOpacity = 0.5;
    view.layer.shadowRadius = 13;
    //(right,down) also (-right,-down)
    view.layer.shadowOffset = CGSizeMake(0.0f, 0.8f);
}
- (void)setMenuViewController:(UIViewController *)menuViewController {
    
    if (_menuViewController) {
        [_menuViewController willMoveToParentViewController:nil];
        [_menuViewController removeFromParentViewController];
        [_menuViewController.view removeFromSuperview];
    }
    
    _menuViewController = menuViewController;
    _menuViewController.view.frame = self.view.bounds;
    [_menuContainer addChildViewController:_menuViewController];
    [_menuContainer.view addSubview:menuViewController.view];
    [_menuContainer didMoveToParentViewController:_menuViewController];
}

- (void)setMainViewController:(UIViewController *)mainViewController {
    
    [self closeMenu];

    if (_mainViewController == mainViewController) {
        if (CGRectGetMinX(_mainContainer.view.frame) == _distanceOpenMenu) {
           // [self closeMenu];
        }
    }
    
    if (_mainViewController) {
        [_mainViewController willMoveToParentViewController:nil];
        [_mainViewController removeFromParentViewController];
        [_mainViewController.view removeFromSuperview];
    }
    
    _mainViewController = mainViewController;
    _mainViewController.view.frame = self.view.bounds;
    [_mainContainer addChildViewController:_mainViewController];
    [_mainContainer.view addSubview:_mainViewController.view];
    [_mainViewController didMoveToParentViewController:_mainContainer];
    [UIViewController attemptRotationToDeviceOrientation];
    
    if (CGRectGetMinX(_mainContainer.view.frame) == _distanceOpenMenu) {
        [self closeMenu];
    }
}

-(void)setBackgroundImage:(UIImage *)backgroundImage{
    _bgImageContainer.image = backgroundImage;
}

-(void)setBackgroundImageContentMode:(UIViewContentMode)backgroundImageContentMode{
    _bgImageContainer.contentMode = backgroundImageContentMode;
}

- (void)toggleMenu {
    CGRect fMain = _mainContainer.view.frame;
    if (CGRectGetMinX(fMain) == _distanceOpenMenu) {
        [self closeMenu];
    }else{
        [self openMenu];
    }
}

-(void)openMenu{
    [self addTapGestures];
    
    CGRect fMain = _mainContainer.view.frame;
    fMain.origin.x = _distanceOpenMenu;
    
    [UIView animateWithDuration:0.7 delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        
        
        CALayer *layer = _mainContainer.view.layer;
        
        layer.zPosition = 1000;
        
        CATransform3D tRotate = CATransform3DIdentity;
        tRotate.m34 = 1.0/ -500;
        tRotate = CATransform3DRotate(tRotate, 0.0 * M_PI / 180.0f, 0, 5, 0);
        
        
        CATransform3D tScale = CATransform3DIdentity;
        tScale.m34 = 1.0/ -500;
        tScale = CATransform3DScale(tScale, 1.0, 1.0, 1.0);
        //        SlidingLayer.transform = tScale;
        
        layer.transform = CATransform3DConcat(tScale, tRotate);
        //        slidingView.layer.transform = CATransform3DConcat(tScale, tRotate);
        //        slidingView.layer.transform = CATransform3DConcat(tRotate, tScale);
        
        _mainContainer.view.frame = fMain;

    } completion:^(BOOL finished) {
    }];
    //menuView in 45
  /*  CALayer *layer = _menuContainer.view.layer;
    layer.zPosition = -1000;
    CATransform3D t = CATransform3DIdentity;
    t.m34 = 1.0/ -500;
    t = CATransform3DRotate(t, -35.0f * M_PI / 180.0f, 0, 1, 0);
    layer.transform = t;
    _menuContainer.view.alpha = 0.3;
    
    [UIView animateWithDuration:1.0
                          delay:0.0
         usingSpringWithDamping:0.5
          initialSpringVelocity:0.5
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                     } completion:^(BOOL finished) {
                         CALayer *layer = _menuContainer.view.layer;
                         CATransform3D rotationAndPerspectiveTransform = CATransform3DIdentity;
                            layer.transform = rotationAndPerspectiveTransform;
                         _menuContainer.view.alpha = 1.0;
                     }];
    
    [UIView animateWithDuration:0.3
                          delay:0.1
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                       
                     }
                     completion:^(BOOL finished){
                         
                     }];
   */
}

-(void)closeMenu{
    
    CGRect fMain = _mainContainer.view.frame;
    fMain.origin.x = 0;
    
    
    
    [UIView animateWithDuration:0.7 delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        
        // NSLog(@"slider close.");
        
        _mainContainer.view.transform = CGAffineTransformMakeScale(1.0, 1.0);
        CALayer *SlidingLayer = _mainContainer.view.layer;
        SlidingLayer.zPosition = 1000;
        CATransform3D tRotate = CATransform3DIdentity;
        tRotate.m34 = 1.0/ -500;
        tRotate = CATransform3DRotate(tRotate, 0.0f * M_PI / 180.0f, 0, 1, 0);
        SlidingLayer.transform = tRotate;
        
        CATransform3D tScale = CATransform3DIdentity;
        tScale.m34 = 1.0/ -500;
        tScale = CATransform3DScale(tScale, 1.0, 1.0, 1.0);
        SlidingLayer.transform = tScale;
        
        SlidingLayer.transform = CATransform3DConcat(tRotate, tScale);
        SlidingLayer.transform = CATransform3DConcat(tScale, tRotate);
        

        
        _mainContainer.view.frame =CGRectMake(0, 0, appDelegate.window.frame.size.width, appDelegate.window.frame.size.height) ;

    } completion:^(BOOL finished) {
        [self removeTapGestures];

       //this is just  for mantain buttons's frames... UIButton
    }];
   /* [UIView animateWithDuration:1.0
                          delay:0.0
         usingSpringWithDamping:0.9
          initialSpringVelocity:0.6
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         
                         _mainContainer.view.frame = fMain;
                         
                     } completion:^(BOOL finished) {
                         [self removeTapGestures];
                     }];
    
    
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         CALayer *layer = _menuContainer.view.layer;
                         layer.zPosition = -1000;
                         CATransform3D t = CATransform3DIdentity;
                         t.m34 = 1.0/ -500;
                         t = CATransform3DRotate(t, -35.0f * M_PI / 180.0f, 0, 1, 0);
                      //   layer.transform = t;
                      //   _menuContainer.view.alpha = 0.3;
                     }
                     completion:^(BOOL finished){
                         
                     }];
    */
}


#pragma mark - Tap Gestures tapMainAction
- (void)addTapGestures {
    if (!self.tapGestureRecognizer) {
        self.mainViewController.view.userInteractionEnabled = NO;
        self.tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapMainAction:)];
        [_mainContainer.view addGestureRecognizer:self.tapGestureRecognizer];
    }
}

- (void)removeTapGestures {
    [_mainContainer.view removeGestureRecognizer:self.tapGestureRecognizer];
    self.tapGestureRecognizer = nil;
    self.mainViewController.view.userInteractionEnabled = YES;
}

- (void)tapMainAction:(id)sender {
    [self closeMenu];
}


#pragma mark - Pan Gesture Recognizer
-(void)setEnablePan:(BOOL)enablePan{
    _enablePan = enablePan;
    if (_enablePan) {
        [self addPanGestures];
    }else{
        [self removePanGestures];
    }
}

- (void)addPanGestures {
    if (!self.panGestureRecognizer) {
        self.panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panDetected:)];
        self.panGestureRecognizer.delegate = self;
        [_mainContainer.view addGestureRecognizer:self.panGestureRecognizer];
    }
}

- (void)removePanGestures {
    [_mainContainer.view removeGestureRecognizer:self.panGestureRecognizer];
    self.panGestureRecognizer = nil;
}

- (void)panDetected:(UIPanGestureRecognizer *)aPanRecognizer{
    
    CGPoint translation = [aPanRecognizer translationInView:aPanRecognizer.view];
    CGPoint velocity = [aPanRecognizer velocityInView:aPanRecognizer.view];
    
    if (aPanRecognizer.state == UIGestureRecognizerStateBegan) {
        self.draggingPoint = translation;
    }else if (aPanRecognizer.state == UIGestureRecognizerStateChanged) {
        
        CGFloat offset = fabs(self.draggingPoint.x - translation.x);
        
        if (offset == 0) {
            return;
        }
        
        self.draggingPoint = translation;
        
        if (velocity.x <= 0) {
            offset = -offset;
        }
        
        CGRect f = _mainContainer.view.frame;
        CGFloat offsetView = f.origin.x + offset;
        CGFloat min = 0;
        CGFloat max = _distanceOpenMenu;
        
        if (offsetView <= min) {
            return;
        }
        
        if (offsetView >= max) {
            return;
        }
        
        f.origin.x += offset;
        _mainContainer.view.frame = f;
        
        // 0 -> 35
        // 210 -> 0
        // newAngle= origin.x * 35 / 210
        CGFloat newAngle = (( (_distanceOpenMenu-f.origin.x ) * 35) / _distanceOpenMenu)*-1;
        
        CALayer *layer = _menuContainer.view.layer;
        CATransform3D t = CATransform3DIdentity;
        layer.zPosition = -1000;
        t.m34 = 1.0/ -500;
        t = CATransform3DRotate(t, newAngle * M_PI / 180.0f, 0, 1, 0);
        layer.transform = t;
        
        CGFloat newAlpha = ((0.7*(f.origin.x))/_distanceOpenMenu)+0.3;
        _menuContainer.view.alpha = newAlpha;
        
    }else if ( aPanRecognizer.state == UIGestureRecognizerStateEnded || aPanRecognizer.state == UIGestureRecognizerStateCancelled ) {
        
        CGRect fMain = _mainContainer.view.frame;
        
        CGFloat newSeg = 1.0;
        BOOL closeMenu = TRUE;
        CGFloat new3dSeg = 0.3;
        
        if (fMain.origin.x >= _distanceOpenMenu/2) {
            [self addTapGestures];
            newSeg = (_distanceOpenMenu-fMain.origin.x) / _distanceOpenMenu;
            new3dSeg = ((_distanceOpenMenu-fMain.origin.x) *0.3 ) / _distanceOpenMenu;
            fMain.origin.x = _distanceOpenMenu;
            closeMenu = FALSE;
        }else{
            [self removeTapGestures];
            newSeg = fMain.origin.x / _distanceOpenMenu;
            new3dSeg = ((fMain.origin.x) *0.3 ) / _distanceOpenMenu;
            fMain.origin.x = 0;
        }
        
        [UIView animateWithDuration:newSeg
                              delay:0.0
             usingSpringWithDamping:0.5
              initialSpringVelocity:0.5
                            options:UIViewAnimationOptionCurveLinear
                         animations:^{
                             _mainContainer.view.frame = fMain;
                         } completion:^(BOOL finished) {
                             
                         }];
        
        CGFloat newAngle = 0.0;
        CGFloat newAlpha = 1.0;
        if (closeMenu) {
            newAngle = -35.0f;
            newAlpha = 0.3;
        }
        
        [UIView animateWithDuration:new3dSeg
                              delay:0.1
                            options:UIViewAnimationOptionCurveLinear
                         animations:^{
                             CALayer *layer = _menuContainer.view.layer;
                             layer.zPosition = -1000;
                             CATransform3D t = CATransform3DIdentity;
                             t.m34 = 1.0/ -500;
                             t = CATransform3DRotate(t, newAngle * M_PI / 180.0f, 0, 1, 0);
                             layer.transform = t;
                             _menuContainer.view.alpha = newAlpha;
                         }
                         completion:^(BOOL finished){
                             
                         }];
    }
}

- (NSUInteger)supportedInterfaceOrientations{
    if (_mainViewController) {
        return [_mainViewController supportedInterfaceOrientations];
    }
    
    return UIInterfaceOrientationMaskAll;
}

@end