//
//  CommonMethod.h
//  buzzLink2411
//
//  Created by sotsys036 on 24/11/15.
//  Copyright © 2015 sotsys036. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CommonMethod : NSObject


+(void)openSideMenu;

+(void)closeSideMenu;

+(BOOL)isNetworkAvailable;

+(NSString *)setDayStatus;

+(void)showLoadingIndicatorOfLightDray;
+(void)hideLoaderLightGray;

+(void)showLoadingIndicator;
+(void)hideLoader;

+(CGFloat)setTitleofScreen;

+(NSString *)getUpcomingDate:(double)startDate endDate:(double)enddate;

+(NSDate *)convertDateInLocalTimeZone:(double)timeStamp;



+(NSDate *)convertDateInLocalTimeZoneUpcoingEvent:(double)timeStamp;


+(NSString *)getDateFromFullDate:(double)startDate;

+(NSString *)getDateForOnlyday:(double)startDate;

+(NSString *)getMonthFromFullDate:(double)startDate;

+(NSString *)displayOpenTime:(double)startTime;

+(NSString *)displayTime:(double)startDate;

+(NSDate *)getCurrentTime;
+(NSInteger)getCurrentHours;
+(NSString *)getCurrentWeekDay;

+(NSMutableArray *)sortAnArrayByHotevent:(NSMutableArray*) webserviceArr;

+(CGFloat)cellVenueTimeFont;

+(NSString *)displayDateInformate:(double)startDate endDate:(double)enddate;
+(NSString *)displayHours:(double)startDate endDate:(double)endDate;

+(NSString *)displayForUpcoming:(double)startDate endDate:(double)enddate;

+(BOOL)isTimeZoneIn24Hours;

+(NSInteger)dateDifference:(NSDate *)strDate EndDate:(NSDate *)endDate;

+ (BOOL)isBetweenHour:(double)beginDate andDate:(double)endDate;

+(NSDate *)displayStartDate:(double)unixMilliseconds;

+ (BOOL)isBetweenDate:(double)beginDate andDate:(double)endDate;

+(BOOL)validateEmailWithString:(NSString*)email;
+(BOOL)validateURL:(NSString *)strUrl;

+(CGFloat)cellPriceFontSize;

+(NSString *)daySuffixForDate:(NSString *)strDate;

+(CGFloat)LeftMenuMenuTitle;
+(CGFloat)LeftMenuSubTitle;

+(CGFloat)pageMenuFontSize;
+(CGFloat)pageMenuSepWidth;

+(CGFloat)cellCounterFontSize;
+(CGFloat)cellCityNameFontSize;
+(CGFloat)cellTextFontSize;
+(CGFloat)cellTimeNDateFontSize;

+(CGFloat)cellHeight;

+(CGFloat)buttonFontSize;

+ (CGPathRef)fancyShadowForRect:(CGRect)rect;

+(UIColor *)getRandomColor;
+(NSString *)displayOpenTimeMANTHAN:(double)startTime;

@end
