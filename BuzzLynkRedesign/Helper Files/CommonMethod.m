//
//  CommonMethod.m
//  buzzLink2411
//
//  Created by sotsys036 on 24/11/15.
//  Copyright © 2015 sotsys036. All rights reserved.
//

#import "CommonMethod.h"
#import "Constants.h"

@implementation CommonMethod


+(void)openSideMenu{
    [AppDel.slideMenuVC toggleMenu];
}

+(void)closeSideMenu{
    [AppDel.slideMenuVC closeMenu];
}

+(NSString *)displayForUpcoming:(double)startDate endDate:(double)enddate{
    //NSTimeInterval timeInterval=[[dict objectForKey:@"event_start_datetime"] doubleValue];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:startDate];
    
    NSDateFormatter *dateformatter=[[NSDateFormatter alloc]init];
    [dateformatter setLocale:[NSLocale currentLocale]];
    [dateformatter setDateFormat:@"dd MMM,yyyy"];
    
    NSString *strEventStartDate=[dateformatter stringFromDate:date];
    
    //timeInterval = [[dict objectForKey:@"event_end_datetime"] doubleValue];
    date = [NSDate dateWithTimeIntervalSince1970:enddate];
    
    NSString *strEventEndDate = [dateformatter stringFromDate:date];
    
    return [NSString stringWithFormat:@"%@ to %@",strEventStartDate,strEventEndDate];
}

+(NSString *)displayHours:(double)startDate endDate:(double)endDate{
    
    NSDate *date = [self convertDateInLocalTimeZone:startDate];
    
    NSDateFormatter *dateformatter=[[NSDateFormatter alloc]init];
    [dateformatter setLocale:[NSLocale currentLocale]];
    [dateformatter setDateFormat:@"hh:mma"];
    
    NSString *strEventStartDate=[dateformatter stringFromDate:date];
    
    date = [self convertDateInLocalTimeZone:endDate];
    
    //[dateformatter setDateFormat:@"dd"];
    
    NSString *strEventEndDate = [dateformatter stringFromDate:date];
    strEventEndDate = [NSString stringWithFormat:@"%@",strEventEndDate];
    
    return [NSString stringWithFormat:@"Hours %@-%@",strEventStartDate,strEventEndDate];
    
}

+(NSMutableArray *)sortAnArrayByHotevent:(NSMutableArray*) webserviceArr{
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]initWithKey:@"event_is_hot" ascending:NO];

    return [[webserviceArr sortedArrayUsingDescriptors:@[sortDescriptor]] mutableCopy];
    
}

+(NSString *)displayTime:(double)startDate{
    
    NSDate *date = [self convertDateInLocalTimeZone:startDate];
    
    NSDateFormatter *dateformatter=[[NSDateFormatter alloc]init];
    [dateformatter setLocale:[NSLocale currentLocale]];
    [dateformatter setDateFormat:@"hh a"];
    
    NSString *strEventStartDate=[dateformatter stringFromDate:date];
    
    return strEventStartDate;
}

+(NSString *)getDateFromFullDate:(double)startDate{
	
	//NSLog(@"%f",startDate);
    NSDate *date = [self convertDateInLocalTimeZone:startDate];
	//NSLog(@"%@",date);
    
    NSDateFormatter *dateformatter=[[NSDateFormatter alloc]init];
    [dateformatter setLocale:[NSLocale currentLocale]];
    [dateformatter setDateFormat:@"dd"];
    
    return [dateformatter stringFromDate:date];
    
}

+(NSString *)getDateForOnlyday:(double)startDate{
	
	//NSLog(@"%f",startDate);
	
	//NSDate *date = [self convertDateInLocalTimeZone:startDate];
	//NSLog(@"%@",date);
	
	NSDate *date = [NSDate dateWithTimeIntervalSince1970:startDate];
	NSDateFormatter *dateformatter=[[NSDateFormatter alloc]init];
	[dateformatter setLocale:[NSLocale currentLocale]];
	[dateformatter setDateFormat:@"dd"];
	
	return [dateformatter stringFromDate:date];
	
}

+(NSString *)getMonthFromFullDate:(double)startDate{
    
    NSDate *date = [self convertDateInLocalTimeZone:startDate];
    
    NSDateFormatter *dateformatter=[[NSDateFormatter alloc]init];
    [dateformatter setLocale:[NSLocale currentLocale]];
    [dateformatter setDateFormat:@"MMM"];
	NSString *strtemp = [dateformatter stringFromDate:date];
	return [strtemp uppercaseString];
	
}

+(NSString *)getUpcomingDate:(double)startDate endDate:(double)enddate{
	
	//NSLog(@"StartTimeStamp:%f",startDate);
	//NSLog(@"EndTimeStamp:%f",enddate);
	
	NSTimeZone *TimeZoneUTC = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    NSString *strStartDate = @"";
    NSDate *date = [self convertDateInLocalTimeZoneUpcoingEvent:startDate];
    NSDateFormatter *dateformatter=[[NSDateFormatter alloc]init];
	[dateformatter setTimeZone:TimeZoneUTC];
    [dateformatter setDateFormat:@"MMM,dd YYYY"];
    
    strStartDate = [dateformatter stringFromDate:date];
    
    date = [self convertDateInLocalTimeZoneUpcoingEvent:enddate];
    
	[dateformatter setDateFormat:@"MMM,dd YYYY"];
	
	//NSLog(@"%@",[NSString stringWithFormat:@"%@-%@",strStartDate,[dateformatter stringFromDate:date]]);
	
	
    return  [NSString stringWithFormat:@"%@-%@",strStartDate,[dateformatter stringFromDate:date]];
    
}

+(NSString *)displayDateInformate:(double)startDate endDate:(double)enddate{
    
    NSDate *date = [self convertDateInLocalTimeZone:startDate];
    
    NSDateFormatter *dateformatter=[[NSDateFormatter alloc]init];
    [dateformatter setLocale:[NSLocale currentLocale]];
    [dateformatter setDateFormat:@"MMMM dd, YYYY"];
    
    NSString *strEventStartDate=[dateformatter stringFromDate:date];
    
    /*date = [self convertDateInLocalTimeZone:enddate];
    
    [dateformatter setDateFormat:@"dd MMM"];
    
    NSString *strEventEndDate = [dateformatter stringFromDate:date];
    //strEventEndDate = [NSString stringWithFormat:@"%@%@",strEventEndDate,[CommonMethod daySuffixForDate:strEventEndDate]];
    
    return [NSString stringWithFormat:@"%@ - %@",strEventStartDate,strEventEndDate];*/
    
    return [NSString stringWithFormat:@"%@",strEventStartDate];
    
}

+(NSString *)displayOpenTimeMANTHAN:(double)startTime
{
	NSString *str = @"";
	NSString *str1 = @"";
	NSString *str2 = @"";
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
	
	NSDate *startDate = [self convertDateInLocalTimeZoneUpcoingEvent:startTime];
	
	[dateFormatter setDateFormat:@"dd"];
	str1 =[dateFormatter stringFromDate:startDate];
	[dateFormatter setDateFormat:@"MMM"];
	str2 = [dateFormatter stringFromDate:startDate];
	
	
	str = [NSString stringWithFormat:@"%@\n%@",str1,[str2 uppercaseString]];
	
	
	//    str = [dateFormatter stringFromDate:startDate];
	
	return str;
}



+(NSString *)getCurrentWeekDay{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEEE"];
    
    return [dateFormatter stringFromDate:[NSDate date]];
}

+(NSDate *)getCurrentTime{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mm:ss a"];
    return [dateFormatter dateFromString:[dateFormatter stringFromDate:[NSDate date]]];
}

+(NSInteger)getCurrentHours{
    //NSDate* now = [NSDate date];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *dateComponents = [gregorian components:(NSCalendarUnitHour  ) fromDate:[NSDate date]];
    return  [dateComponents hour];
}

+(NSString *)setDayStatus{
    NSString *str = @"";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH.mm"];
    NSString *strCurrentTime = [dateFormatter stringFromDate:[NSDate date]];
    
    //NSLog(@"Check float value: %.2f",[strCurrentTime floatValue]);
    if ([strCurrentTime floatValue] >= 18.00 || ([strCurrentTime containsString:@"PM"] || [strCurrentTime containsString:@"pm"])){
        str = @"Tonight";
    }else{
        str = @"Today";
    }
    
    return str;
}


+(NSDate *)convertDateInLocalTimeZoneUpcoingEvent:(double)timeStamp{
	
	NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeStamp];
	// NSTimeZone *tz = [NSTimeZone localTimeZone];
	
	NSTimeZone *tz = [NSTimeZone timeZoneWithName:@"UTC"];
	
	//NSLog(@"%@",tz);
	NSInteger seconds = [tz secondsFromGMTForDate: date];
	
	return [NSDate dateWithTimeInterval: seconds sinceDate: date];
}

+(NSDate *)convertDateInLocalTimeZone:(double)timeStamp{
	
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeStamp];
    NSTimeZone *tz = [NSTimeZone localTimeZone];
	//NSTimeZone *tz = [NSTimeZone timeZoneWithName:@"UTC"];
	
	//NSLog(@"%@",tz);
    NSInteger seconds = [tz secondsFromGMTForDate: date];
	
    return [NSDate dateWithTimeInterval: seconds sinceDate: date];
}

+(BOOL)isTimeZoneIn24Hours{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateStyle:NSDateFormatterNoStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    NSString *dateString = [formatter stringFromDate:[NSDate date]];
    NSRange amRange = [dateString rangeOfString:[formatter AMSymbol]];
    NSRange pmRange = [dateString rangeOfString:[formatter PMSymbol]];
    return amRange.location == NSNotFound && pmRange.location == NSNotFound;
    
}

+(NSString *)displayStartDate:(double)unixMilliseconds {
	
	
	NSDate *date = [NSDate dateWithTimeIntervalSince1970:unixMilliseconds];
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
	[dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
	//[dateFormatter setLocale:[NSLocale currentLocale]];
	[dateFormatter setDateFormat:@"MM-dd-yyyy hh:mm a"];
	//[dateFormatter setDateFormat:@"hh:mma"];
	
	//NSLog(@"The date is %@", [dateFormatter stringFromDate:date]);
	
	
	return [dateFormatter stringFromDate:date];
}


+ (BOOL)isBetweenHour:(double)beginDate andDate:(double)endDate{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mm a"];
    
    NSDate *startDate = [self convertDateInLocalTimeZone:beginDate];
	
    NSDate *eDate = [self convertDateInLocalTimeZone:endDate];
    
    NSDate *date = [NSDate date];
    NSTimeInterval ti = [date timeIntervalSince1970];
    NSDate *currentDate = [self convertDateInLocalTimeZone:ti];
    
    if ([currentDate compare:startDate] ==  NSOrderedDescending && [currentDate compare:eDate] == NSOrderedAscending){
        return YES;
    }else{
        return NO;
    }
    
}

+(NSString *)displayOpenTime:(double)startTime{
    NSString *str = @"";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
    
    NSDate *startDate = [self convertDateInLocalTimeZone:startTime];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSCalendarUnitHour) fromDate:startDate];
    NSInteger hour = [components hour];
    
    if([CommonMethod isTimeZoneIn24Hours])
	{
        str = [NSString stringWithFormat:@"Open%ld",(long)hour];
    }else{
        if(hour > 12)
		{
            str = [NSString stringWithFormat:@"Open\n%ld PM",(long)hour - 12];
        }else{
            str = [NSString stringWithFormat:@"Open\n%ld AM",(long)hour];
        }
    }

    return str;
}

+ (BOOL) isBetweenDate:(double)beginDate andDate:(double)endDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
    NSDate *startDate = [self convertDateInLocalTimeZone:beginDate];
    NSDate *eDate = [self convertDateInLocalTimeZone:endDate];
    NSDate *date = [NSDate date];
    NSTimeInterval ti = [date timeIntervalSince1970];
    NSDate *currentDate = [self convertDateInLocalTimeZone:ti];
    
    if ([currentDate compare:startDate] ==  NSOrderedDescending && [currentDate compare:eDate] == NSOrderedAscending){
        return YES;
    }else{
        return NO;
    }

}

+(CGFloat)cellCityNameFontSize{
    CGFloat fSize = 12.0;
    
    if(IS_IPHONE_4 ){
        fSize = 12.0;
    }else if(IS_IPHONE_5){
        fSize = 14.0;
    }else if(IS_IPHONE_6){
        fSize = 16.0;
    }else{
        fSize = 18.0;
    }
    
    return fSize;
}

+(CGFloat)cellVenueTimeFont{
    CGFloat fSize = 12.0;
    
    if(IS_IPHONE_4 ){
        fSize = 13.0;
    }else if(IS_IPHONE_5){
        fSize = 14.0;
    }else if(IS_IPHONE_6){
        fSize = 15.0;
    }else{
        fSize = 16.0;
    }
    
    return fSize;
}

+(CGFloat)cellPriceFontSize{
    CGFloat fSize = 12.0;
    
    if(IS_IPHONE_4 ){
        fSize = 12.0;
    }else if(IS_IPHONE_5){
        fSize = 14.0;
    }else if(IS_IPHONE_6){
        fSize = 15.0;
    }else{
        fSize = 16.0;
    }
    
    return fSize;
}

+(void)showLoadingIndicatorOfLightDray{
	
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake((WINDOW_WIDTH/2)-27, ((WINDOW_HEIGHT-15)/2), 50, 50)];
    
    indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    indicator.color = [UIColor colorWithRed:(2.0/255.0) green:(2.0/255.0) blue:(91.0/255.0) alpha:1.0];
    [indicator startAnimating];
    [AppDel.window addSubview:indicator];
    
    //enableTouch(false);
}
+(void)hideLoaderLightGray{
	
    for(UIActivityIndicatorView *activity in AppDel.window.subviews){
        if([activity isKindOfClass:[UIActivityIndicatorView class]]){
            [activity removeFromSuperview];
        }
    }
    //enableTouch(true);
}

+(void)showLoadingIndicator{
	
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake((WINDOW_WIDTH/2)-27, ((WINDOW_HEIGHT-15)/2), 50, 50)];
    
    indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    indicator.color = [UIColor lightGrayColor];
    [indicator startAnimating];
    [AppDel.window addSubview:indicator];
    
    enableTouch(false);
}
+(void)hideLoader{
    for(UIActivityIndicatorView *activity in AppDel.window.subviews){
        if([activity isKindOfClass:[UIActivityIndicatorView class]]){
            [activity removeFromSuperview];
        }
    }
    
    enableTouch(true);
}

+(BOOL)isNetworkAvailable{
    
    [[NetConnection sharedReachability] setHostName:@"http://www.google.com"];
    NetworkStatus remoteHostStatus = [[NetConnection sharedReachability] internetConnectionStatus];
    
    if (remoteHostStatus == NotReachable)
        return NO;
    else if (remoteHostStatus == ReachableViaCarrierDataNetwork || remoteHostStatus == ReachableViaWiFiNetwork)
        return YES;
    return NO;
}

+(CGFloat)cellTimeNDateFontSize{
    CGFloat fSize = 12.0;
    
    if(IS_IPHONE_4 ){
        fSize = 12.0;
    }else if(IS_IPHONE_5){
        fSize = 14.0;
    }else if(IS_IPHONE_6){
        fSize = 16.0;
    }else{
        fSize = 18.0;
    }
    
    return fSize;
}

+(NSString *)daySuffixForDate:(NSString *)strDate {
    
    switch ([strDate integerValue]) {
        case 1:
        case 21:
        case 31: return @"st";
        case 2:
        case 22: return @"nd";
        case 3:
        case 23: return @"rd";
        default: return @"th";
    }
}

+(CGFloat)cellCounterFontSize{
    CGFloat fSize = 12.0;
    
    if(IS_IPHONE_4 ){
        fSize = 13.0;
    }else if(IS_IPHONE_5){
        fSize = 14.0;
    }else if(IS_IPHONE_6){
        fSize = 17.0;
    }else{
        fSize = 20.0;
    }
    
    return fSize;
}

+ (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

+(BOOL)validateURL:(NSString *)strUrl{
    NSString *urlRegEx =
    @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [urlTest evaluateWithObject:strUrl];
}

+(CGFloat)cellTextFontSize{
    CGFloat fSize = 12.0;
    
    if(IS_IPHONE_4 ){
        fSize = 14.0;
    }else if(IS_IPHONE_5){
        fSize = 16.0;
    }else if(IS_IPHONE_6){
        fSize = 19.0;
    }else{
        fSize = 22.0;
    }
    
    return fSize;
}

+(CGFloat)LeftMenuMenuTitle{
    CGFloat fsize = 12.0;
    
    if(IS_IPHONE_4){
        fsize = 19.0;
    }else if (IS_IPHONE_5){
        fsize = 20.0;
    }else if(IS_IPHONE_6){
        fsize = 23.0;
    }else{
        fsize = 25.0;
    }
    
    return fsize;
}

+(CGFloat)LeftMenuSubTitle{
    CGFloat fsize = 12.0;
    
    if(IS_IPHONE_4){
        fsize = 12.0;
    }else if (IS_IPHONE_5){
        fsize = 12.0;
    }else if(IS_IPHONE_6){
        fsize = 15.0;
    }else{
        fsize = 16.0;
    }
    
    return fsize;
}

+(CGFloat)cellHeight{
    CGFloat cellHeight = 120.0;
    
    if (IS_IPHONE_4) {
        cellHeight = 110.0;
    }else if(IS_IPHONE_5){
        cellHeight = 135.0;
    }else if(IS_IPHONE_6){
        cellHeight = 165.0;
    }else{
        cellHeight = 190.0;
    }
    return cellHeight;
}

+(NSInteger)dateDifference:(NSDate *)strDate EndDate:(NSDate *)endDate{
    
    NSTimeInterval distanceBetweenDates = [endDate timeIntervalSinceDate:strDate];
    double secondsInAnHour = 3600;
    return  distanceBetweenDates / secondsInAnHour;
}

+ (CGPathRef)fancyShadowForRect:(CGRect)rect
{
    CGSize size = rect.size;
    UIBezierPath* path = [UIBezierPath bezierPath];
    
    //right
    [path moveToPoint:CGPointZero];
    [path addLineToPoint:CGPointMake(size.width, 0.0f)];
    [path addLineToPoint:CGPointMake(size.width, size.height + 15.0f)];
    
    //curved bottom
    [path addCurveToPoint:CGPointMake(0.0, size.height + 15.0f)
            controlPoint1:CGPointMake(size.width - 15.0f, size.height)
            controlPoint2:CGPointMake(15.0f, size.height)];
    
    [path closePath];
    
    return path.CGPath;
}

+(CGFloat)pageMenuFontSize{
    
    CGFloat fontSize = 12.0;
    
    if (IS_IPHONE_4 || IS_IPHONE_5) {
        fontSize = 14.0;
    }else if(IS_IPHONE_6){
        fontSize = 17.0;
    }else{
        fontSize = 18.0;
    }
    
    return fontSize;
}

+(CGFloat)setTitleofScreen{
    CGFloat fontSize = 30.0;
    
    if (IS_IPHONE_4 || IS_IPHONE_5) {
        fontSize = 22.0;
    }else if(IS_IPHONE_6){
        fontSize = 25.0;
    }else{
        fontSize = 30.0;
    }
    
    return fontSize;
}


+(CGFloat)pageMenuSepWidth{
    CGFloat menuWidth = 50.0;
    
    if(IS_IPHONE_4 || IS_IPHONE_5){
        menuWidth = 100;
    }else if(IS_IPHONE_6){
        menuWidth = 118.0;
    }else {
        menuWidth = 132.0;
    }
    
    return menuWidth;
}


+(CGFloat)buttonFontSize{
    CGFloat btnFont = 14.0;
    
    if(IS_IPHONE_4 || IS_IPHONE_5){
        btnFont = 16.0;
    }else if(IS_IPHONE_6){
        btnFont = 19.0;
    }else{
        btnFont = 22.0;
    }
    
    return btnFont;
}

+(UIColor *)getRandomColor{
    
    CGFloat red = arc4random() % 255 / 255.0;
    CGFloat green = arc4random() % 255 / 255.0;
    CGFloat blue = arc4random() % 255 / 255.0;
    
    UIColor *color = [UIColor colorWithRed:red green:green blue:blue alpha:1.0];
    return color;
}


@end
