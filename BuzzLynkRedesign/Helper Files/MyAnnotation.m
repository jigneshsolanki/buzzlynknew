//
//  MyAnnotation.m
//  BuzzLink5
//
//  Created by sotsys036 on 19/12/15.
//  Copyright © 2015 sotsys036. All rights reserved.
//

#import "MyAnnotation.h"

@implementation MyAnnotation

@synthesize coordinate;

@synthesize title;

@synthesize time;

@synthesize isHot;

@synthesize isDefault;

@synthesize pintag;
@synthesize eventId;

@synthesize lat,longt;

@synthesize subTitle;

-(MKAnnotationView *)annotationview
{
    MKAnnotationView *annotationview = [[MKAnnotationView alloc]initWithAnnotation:self reuseIdentifier:@"PinAnnotation"];
    annotationview.enabled = YES;
    annotationview.canShowCallout = YES;
    
    return annotationview;
}

-(id)initWithCoordinate:(CLLocationCoordinate2D) c  title:(NSString *) t  subTitle:(NSString *)timed time:(NSString *)tim
{
    self.coordinate=c;
    self.time=tim;
    self.subTitle=timed;
    self.title=t;
    return self;
}

-(id)initWithCoordinate:(CLLocationCoordinate2D) c title:(NSString *)tit isHot:(BOOL)isH tag:(NSInteger)pinTag lat:(double)latitute  long:(double)longtitute evtID:(NSString *)eventID
{
    self =  [super init];
    
    if (self){
        self.coordinate=c;
        self.title=tit;
        self.isHot = isH;
        self.pintag = pinTag;
        self.lat = latitute;
        self.longt = longtitute;
        self.eventId = eventID;
        
        [self findDistance];
    }
    
    
    return self;
}

-(id)initWithCoordinate:(CLLocationCoordinate2D)co isDefault:(BOOL)isDef tag:(NSInteger)pinTag{
    self.isDefault = isDef;
    self.coordinate = co;
    self.pintag = pinTag;
    
    return self;
}

-(void)findDistance{
    
    if(self.lat || self.longt){
        
        CLLocation *currentLocation = [[CLLocation alloc]initWithLatitude:AppDel.crntLat longitude:AppDel.crntLon];
        
        CLLocation *serverLocation = [[CLLocation alloc]initWithLatitude:self.lat longitude:self.longt];
        
        CLLocationDistance meters = [currentLocation distanceFromLocation:serverLocation];
        
        self.strDistance = [NSString stringWithFormat:@"%.1f mi",(meters * 0.000621371)];
        
    }
    
}



@end
