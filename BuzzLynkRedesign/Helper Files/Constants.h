//
//
//
//  Created by SOTSYS036 on 24/11/15.
//  Copyright (c) 2015 SpaceO . All rights reserved.
//

#ifndef constant_h
#define constant_h

#import "AppDelegate.h"
#import <AFNetworking.h>
#import "UIImageView+WebCache.h"

#import <MapKit/MapKit.h>
#import "NetConnection.h"

#import "ViewController.h"


#define obj_Global               [Global sharedInstance]
#define AppDel                   ((AppDelegate *)[UIApplication sharedApplication].delegate)

#define APP_NAME @"BuzzLynk"
#define APP_VERSION @"1.0"

#define MySharedObj            [SharedClass objSharedClass]

#define NET_MSG         @"No internect connection"

#define    WINDOW_WIDTH     [UIScreen mainScreen].applicationFrame.size.width

#define    WINDOW_HEIGHT    [UIScreen mainScreen].applicationFrame.size.height

#define enableTouch(enable)   [[UIApplication sharedApplication].keyWindow setUserInteractionEnabled:enable];

//iOS Version
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

//AlertView Constants
#define DisplayAlert(msg) { UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:APP_NAME message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]; [alertView show];}

//#define baseURL_Page                    @"http://52.35.102.245/"
#define baseURL_Page                  @"http://main.spaceotechnologies.com/buzzlynk"
//#define baseURL_Page                  @"http://192.168.1.250/buzzlynk"

#define page_Term                     @"/pages/terms"
#define page_faq                      @"/pages/faq"
#define page_Privacy                  @"/pages/privacy_policy"

//Webservice method Constant

//#define baseUrlWebService               @"http://52.35.102.245/api/v1/"
#define baseUrlWebService             @"http://main.spaceotechnologies.com/buzzlynk/api/v1/"
//#define baseUrlWebService             @"http://192.168.1.250/buzzlynk/api/v1/"

#define buzzCategory        @"categories"
#define featureEvent        @"events/featured"
#define upcomingCategory    @"events/upcoming"
#define upcomingEvent       @"events/upcoming_events"
#define attractionEvent     @"events/attraction"
#define eventNvenue         @"categories/%@"
#define user_Registration   @"users/signup"
#define user_login          @"users/signin"
#define submitEvent         @"events"
#define atlBuzzCities       @"cities"
#define forgotPwd           @"users/send_reset_password_details"
#define searchWS            @"events/search"
#define searchHistory       @"/events/top_search"


//User Register Request Parameter
#define reg_firstName       @"firstname"
#define reg_lastName        @"lastname"
#define reg_email           @"email"
#define reg_password        @"password"
#define reg_imgattribute    @"image_attributes"


//Category Webservice Response Key
#define category_id     @"category_id"
#define category_name   @"category_name"
#define category_front  @"display_front"
#define category_slot   @"slot"
#define category_pic    @"category_pic"
#define category_total_event    @"total_events"

//Event and Venue Webservice Response Key
#define event_events                @"events"
#define event_parent_categories     @"parent_categories"


//Webservice Common Response Constant
#define  res_Code       @"code"
#define  res_Message    @"message"
#define  res_Categories @"categories"

//User Default
#define XGET_VALUE(k) [[NSUserDefaults standardUserDefaults]valueForKey:k]
#define XSET_VALUE(k,v) [[NSUserDefaults standardUserDefaults] setValue:v forKey:k];

#define XGET_OBJ(k) [[NSUserDefaults standardUserDefaults]objectForKey:k]
#define XSET_OBJ(k,v) [[NSUserDefaults standardUserDefaults] setObject:v forKey:k];

//Google Place API

#define placeAPI        @"BuzzLynkKey123"
#define API_KEY         @"AIzaSyCEbHxuoNQGSJmyxpz1BxcBVd2ZUOHFH-8"

#define enableTouch(enable)   [[UIApplication sharedApplication].keyWindow setUserInteractionEnabled:enable];

//Alert Message
#define DisplayAlert(msg) { UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:APP_NAME message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]; [alertView show];}


//Device Type
#define IS_IPAD                             (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE                           (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPHONE_4                         (IS_IPHONE && (MAX(([[UIScreen mainScreen] bounds].size.width),             ([[UIScreen mainScreen] bounds].size.height))) < 568.0)
#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && MAX([UIScreen mainScreen].bounds.size.height,[UIScreen mainScreen].bounds.size.width) == 667)
#define IS_IPHONE_6_Plus ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && MAX([UIScreen mainScreen].bounds.size.height,[UIScreen mainScreen].bounds.size.width) == 736)

//Color
#define RGB(r,g,b,a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
#define bluecolor RGB(61, 106, 179, 1.0)
#define greencolor RGB(21, 178, 75, 1.0)
#define orangecolor RGB(241, 90, 42, 1.0)

#define  OPENNOW   @"OPEN\nNOW"



#endif