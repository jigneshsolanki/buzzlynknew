//
//  MyAnnotation.h
//  BuzzLink5
//
//  Created by sotsys036 on 19/12/15.
//  Copyright © 2015 sotsys036. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "Constants.h"

@interface MyAnnotation : NSObject<MKAnnotation>{
    CLLocationCoordinate2D coordinate;
    NSString *title;
    NSString *subTitle;
    NSString *time;
    
}

@property (nonatomic)CLLocationCoordinate2D coordinate;

@property (nonatomic, copy) NSString *title;

@property (nonatomic, retain) NSString *subTitle;

@property (nonatomic) double lat;

@property (nonatomic) double longt;

@property (nonatomic)BOOL isHot;

@property (nonatomic) NSString *strDistance;

@property(nonatomic)BOOL isDefault;

@property(nonatomic)NSInteger pintag;

@property(nonatomic,retain)NSString *eventId;

@property (nonatomic,retain) NSString *time;

- (MKAnnotationView *) annotationview;

-(id)initWithCoordinate:(CLLocationCoordinate2D) c  title:(NSString *) t  subTitle:(NSString *)timed time:(NSString *)tim;

-(id)initWithCoordinate:(CLLocationCoordinate2D) c title:(NSString *)tit isHot:(BOOL)isH tag:(NSInteger)pinTag lat:(double)latitute  long:(double)longtitute evtID:(NSString *)eventID;

-(id)initWithCoordinate:(CLLocationCoordinate2D)co isDefault:(BOOL)isDef tag:(NSInteger)pinTag;

@end
