//
//  ImagePageControl.h
//  BuzzLynkRedesign
//
//  Created by sotsys036 on 22/04/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImagePageControl : UIPageControl{
    UIImage* activeImage;
    UIImage* inactiveImage;
}

@property(nonatomic, retain) UIImage* activeImage;
@property(nonatomic, retain) UIImage* inactiveImage;

@end
