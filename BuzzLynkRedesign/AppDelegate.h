//
//  AppDelegate.h
//  BuzzLynkRedesign
//
//  Created by sotsys036 on 02/03/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HKRotationNavigationController.h"
#import "HKSlideMenu3DController.h"
#import "SideMenuVCViewController.h"

#import <CoreLocation/CoreLocation.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate>{
    
	
	CLLocationManager * locationmanager;
	CLLocation *crnLoc;
	NSNumber *longitude;
	NSNumber *latitude;
	
	
	

}

@property (nonatomic) BOOL isAppleMapClick;
@property (nonatomic) BOOL isFromUpcoming;


@property (strong, nonatomic) UIWindow *window;

@property(nonatomic)CGFloat crntLat,crntLon;

@property(nonatomic)BOOL isFromCategory;

@property(nonatomic)BOOL isFromAnimation;

@property (strong, nonatomic) HKRotationNavigationController *navMainObj;
@property (strong, nonatomic) SideMenuVCViewController *sideMenuObj;
@property (strong, nonatomic) HKSlideMenu3DController *slideMenuVC;
@property (strong, nonatomic) UINavigationController *navigationController;

@end

