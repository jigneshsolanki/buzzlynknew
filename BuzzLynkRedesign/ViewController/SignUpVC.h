//
//  SignUpVC.h
//  BuzzLynkRedesign
//
//  Created by sotsys036 on 10/03/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@interface SignUpVC : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate,UIActionSheetDelegate>{
    
    __weak IBOutlet UITextField *txtName;
    __weak IBOutlet UITextField *txtEmail;
    __weak IBOutlet UITextField *txtUsername;
    __weak IBOutlet UITextField *txtPassword;
    
    __weak IBOutlet UIButton *btnSignUp;
    
    __weak IBOutlet UIImageView *imgProfile;
    
    AFHTTPRequestOperationManager *manager;
}

@end
