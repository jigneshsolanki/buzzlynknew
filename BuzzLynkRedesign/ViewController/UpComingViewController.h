//
//  UpComingViewController.h
//  BuzzLynkRedesign
//
//  Created by sotsys036 on 02/03/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@interface UpComingViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>{
    
    NSMutableArray *arrUpComing;
    
    __weak IBOutlet UICollectionView *clgUpcoming;
    
    AFHTTPRequestOperationManager *manager;
}

@end
