//
//  EventDetailVC.m
//  BuzzLynkRedesign
//
//  Created by sotsys036 on 09/03/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import "EventDetailVC.h"
#import "DetailImageCell.h"
#import "VenueDetailCell.h"

@interface EventDetailVC ()<UIActionSheetDelegate>
{
    NSString *strEventName,*strWebsite;
    NSString *strDateOfEvent,*strAddress,*strDescription;
    
    NSInteger counter;
    CLLocationCoordinate2D coord;
}

@property(nonatomic, strong) MapRequestModel *model;


@end

@implementation EventDetailVC

@synthesize strEventId;

//PARESH MAP
- (IBAction)carBtnPressed:(id)sender {
	
	
	UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select Map"
															 delegate:self
													cancelButtonTitle:@"Cancel"
											   destructiveButtonTitle:nil
													otherButtonTitles:@"Apple Map", @"Google Map",nil];
	actionSheet.delegate = self;
	[actionSheet showInView:self.view];
	
	
}

#pragma mark - UIActionSheet Delegate
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	//NSLog(@"Index:%ld",(long)buttonIndex);
	if(buttonIndex == 0) {
		//apple map
		AppDel.isAppleMapClick = YES;
		
		[self checkAppleMapAvailable];
	}
	else if(buttonIndex == 1) {
		//google map
		AppDel.isAppleMapClick = NO;
		[self checkGoogleMapAvailable];
	}
}
-(void)checkAppleMapAvailable {
	
	[OpenInGoogleMapsController sharedInstance].fallbackStrategy =
	kGoogleMapsFallbackAppleMaps;
	
	[self openDirectionsInGoogleMaps];
}
- (void)openDirectionsInGoogleMaps {
	GoogleDirectionsDefinition *directionsDefinition = [[GoogleDirectionsDefinition alloc] init];
    
	if (self.model.startCurrentLocation)
	{
		directionsDefinition.startingPoint = nil;
	}
	else {
		GoogleDirectionsWaypoint *startingPoint = [[GoogleDirectionsWaypoint alloc] init];
		startingPoint.queryString = self.model.startQueryString;
		CLLocationCoordinate2D latLong = CLLocationCoordinate2DMake(AppDel.crntLat, AppDel.crntLon);
		startingPoint.location = latLong;
		directionsDefinition.startingPoint = startingPoint; //paresh
		
//		directionsDefinition.startingPoint = [GoogleDirectionsWaypoint waypointWithQuery:@"ISCON Cross Road BRTS Bus Stop, Satellite Road, Satellite, Ahmedabad, Gujarat 380015"];
	}
	
	if (self.model.destinationCurrentLocation) {
		directionsDefinition.destinationPoint = nil;
	}
	else {
		GoogleDirectionsWaypoint *destination = [[GoogleDirectionsWaypoint alloc] init];
		destination.queryString = self.model.destinationQueryString;
		
        CLLocationCoordinate2D latLong = coord;
		destination.location = latLong;
		directionsDefinition.destinationPoint = destination; //paresh
		
//		directionsDefinition.destinationPoint = [GoogleDirectionsWaypoint waypointWithQuery:@"Maninagar Railway Station, Maninagar East, Ahmedabad, Gujarat"];
	}
	directionsDefinition.travelMode = [self travelModeAsGoogleMapsEnum:self.model.travelMode];
	
	[[OpenInGoogleMapsController sharedInstance] openDirections:directionsDefinition];
}

// Convert our app's "travel mode" to the official Google Enum
- (GoogleMapsTravelMode)travelModeAsGoogleMapsEnum:(TravelMode)appTravelMode {
	switch (appTravelMode) {
		case kTravelModeBicycling:
			return kGoogleMapsTravelModeBiking;
		case kTravelModeDriving:
			return kGoogleMapsTravelModeDriving;
		case kTravelModePublicTransit:
			return kGoogleMapsTravelModeTransit;
		case kTravelModeWalking:
			return kGoogleMapsTravelModeWalking;
		case kTravelModeNotSpecified:
			return 0;
	}
}

-(void)checkGoogleMapAvailable {
	
	if (![[OpenInGoogleMapsController sharedInstance] isGoogleMapsInstalled]) {
		
		 DisplayAlert(@"Google Maps not Installed");
		
		//DisplayAlertWithTitle(@"Google Maps not Installed", @"");
		return;
	}
	else {
		[self openDirectionsInGoogleMaps];
	}
}




- (IBAction)CarButtonPressed:(id)sender {
	
	//NSLog(@"Hello");
	
}
#pragma mark - ViewController Methods
- (void)viewDidLoad {
	
	//NSLog(@"GetEventDetail");
	
    [super viewDidLoad];
    
    [[UIApplication sharedApplication]setStatusBarStyle: UIStatusBarStyleDefault];
    
    
    vwDate.layer.cornerRadius = 3.0;
    
    vwEventDetail.layer.cornerRadius = 5.0;
    vwEventDetail.clipsToBounds = YES;
    
    vwVenueDetail.layer.cornerRadius = 5.0;
    vwVenueDetail.clipsToBounds = YES;
	
	VWeventStatus.layer.cornerRadius = 4.0;
	VWeventStatus.clipsToBounds = YES;
	
    
    tvDesc.editable = false;
    tvDesc.selectable = false;
    
    arryOperatingHours = [NSMutableArray array];
    arrKeys = [NSMutableArray array];
	arrykeyweakfromday =[NSMutableArray array];
    arrImages = [NSMutableArray array];
    
    manager  = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    
   // imagePageControls.pageIndicatorTintColor = [UIColor clearColor];
    
    //[imagePageControls setPageIndicatorImage:[UIImage imageNamed:@"InActiveDot"]];
    //[imagePageControls setCurrentPageIndicatorImage:[UIImage imageNamed:@"activeDot"]];
    
    [pageControlVW setPageIndicatorImage:[UIImage imageNamed:@"InActiveDot"]];
    [pageControlVW setCurrentPageIndicatorImage:[UIImage imageNamed:@"activeDot"]];
    
    [self callEventDetailWebService];
    
}

-(NSString *)displayStartTimeNEW:(double)unixMilliseconds {
	
	NSDate *date = [NSDate dateWithTimeIntervalSince1970:unixMilliseconds];
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	//[dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
	[dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
	[dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
	[dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
	//[dateFormatter setLocale:[NSLocale currentLocale]];
	[dateFormatter setDateFormat:@"hh:mm a"];
	//NSLog(@"The date is %@", [dateFormatter stringFromDate:date]);
	return [dateFormatter stringFromDate:date];
}


-(NSString *)displayStartTime:(double)unixMilliseconds {
	
	NSDate *date = [NSDate dateWithTimeIntervalSince1970:unixMilliseconds];
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	//[dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
	[dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
	[dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
	[dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
	//[dateFormatter setLocale:[NSLocale currentLocale]];
	[dateFormatter setDateFormat:@"hh:mma"];
	
	//NSLog(@"The date is %@", [dateFormatter stringFromDate:date]);
	return [dateFormatter stringFromDate:date];
}
-(NSString *)displayEndTime:(double)unixMilliseconds {
	
	NSDate *date = [NSDate dateWithTimeIntervalSince1970:unixMilliseconds];
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	//[dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
	[dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
	[dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
	[dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
	//[dateFormatter setLocale:[NSLocale currentLocale]];
	[dateFormatter setDateFormat:@"hh:mma"];
	
	//NSLog(@"The date is %@", [dateFormatter stringFromDate:date]);
	return [dateFormatter stringFromDate:date];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
}

#pragma mark - Private Methods

-(void)callEventDetailWebService{
    
    [CommonMethod showLoadingIndicator];
    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"],@"user_id", nil];
    
    [manager GET:[NSString stringWithFormat:@"%@%@",baseUrlWebService,[NSString stringWithFormat:@"events/%@",strEventId]] parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSError *error;
        NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        
        [CommonMethod hideLoader];
        
        if([[parsedObject objectForKey:res_Code] integerValue] == 200){
            
            arrImages = [[parsedObject objectForKey:@"event"] objectForKey:@"event_images"];
			
			
			//paresh change number:-
			chekphoneis = [[parsedObject objectForKey:@"event"] objectForKey:@"event_phone"];
		
			if (chekphoneis.length==0)
			{
				//NSLog(@"No Nomber");
				btncall.selected = true;
				btncall.userInteractionEnabled = NO;
			}else
			{
				btncall.userInteractionEnabled = YES;
				//NSLog(@"Have number");
			}
			
            if (arrImages.count > 0)
			{
                if (arrImages.count < 3)
				{
                    counter = arrImages.count;
					
					if (counter == 1)
					{
						pageControlVW.hidden = true;
						
					}
					
                }else
				{
                    counter = 3;
					
					pageControlVW.hidden = false;
					
                }
		
				
				
            }else
			{
                counter = 1;
				pageControlVW.hidden = true;
            }
            
            pageControlVW.numberOfPages = counter;
            
            [clgImages setDelegate:self];
            [clgImages setDataSource:self];
            
            [clgImages reloadData];
            
            tvDesc.textColor = [UIColor colorWithRed:(85.0/255.0) green:(85.0/255.0) blue:(85.0/255.0) alpha:0.8];
            
            if([[[parsedObject objectForKey:@"event"] objectForKey:@"event_type"] isEqualToString:@"event"]){
				
				//Green Button: with orange button in upcoming event:-
				
                lblVenueCatName.hidden = true;
                
                strEventName = [NSString stringWithFormat:@"%@",[[parsedObject objectForKey:@"event"] objectForKey:@"event_name"]];
                
                [lblEventVenueName setText:[NSString stringWithFormat:@"%@",[[parsedObject objectForKey:@"event"] objectForKey:@"event_name"]]];
                
                [lblCategory setText:[NSString stringWithFormat:@"%@",[[parsedObject objectForKey:@"event"] objectForKey:@"event_categories"]]];
                
                tvDesc.text = [NSString stringWithFormat:@"%@",[[parsedObject objectForKey:@"event"] objectForKey:@"event_description"]];
				
				//paresh change: getDateFromFullDate change:
				
                [lblDate setText:[NSString stringWithFormat:@"%@",[CommonMethod getDateForOnlyday:[[[parsedObject objectForKey:@"event"] objectForKey:@"event_start_datetime"] doubleValue]]]];
				
				//NSLog(@"%@",lblDate.text);
                
                [lblMonth setText:[NSString stringWithFormat:@"%@",[CommonMethod getMonthFromFullDate:[[[parsedObject objectForKey:@"event"] objectForKey:@"event_start_datetime"] doubleValue]]]];
				
				//NSLog(@"%@",[NSString stringWithFormat:@"%@",[[parsedObject objectForKey:@"event"] objectForKey:@"event_start_datetime"]]);
                if([[[parsedObject objectForKey:@"event"] objectForKey:@"event_price"] isEqualToString:@"FREE"]){
                    [lblPrice setText:[NSString stringWithFormat:@"%@",[[parsedObject objectForKey:@"event"] objectForKey:@"event_price"]]];
                }else{
                    [lblPrice setText:[NSString stringWithFormat:@"%@",[[parsedObject objectForKey:@"event"] objectForKey:@"event_price"]]];
                }
                
                if(![[[parsedObject objectForKey:@"event"] objectForKey:@"event_start_datetime"] isEqual: [NSNull null]] || ![[[parsedObject objectForKey:@"event"] objectForKey:@"event_end_datetime"] isEqual:[NSNull null]]){
                    
                    if([CommonMethod isBetweenDate:[[[parsedObject objectForKey:@"event"] objectForKey:@"event_start_datetime"] doubleValue] andDate:[[[parsedObject objectForKey:@"event"] objectForKey:@"event_end_datetime"] doubleValue]]){
						
						 [VWeventStatus setBackgroundColor:greencolor];
                         [lblEventStatus setText:OPENNOW];
                        
                    }else
					{
						
						NSString *strStarTimeFinal_24hr = [self displayStartTimeNEW:[[[parsedObject objectForKey:@"event"] objectForKey:@"event_start_datetime"] doubleValue]];
						
						
						[lblEventStatus setText:[NSString stringWithFormat:@"OPEN\n%@",strStarTimeFinal_24hr]];
						
                   //     [lblEventStatus setText:[CommonMethod displayOpenTime:[[[parsedObject objectForKey:@"event"] objectForKey:@"event_start_datetime"] doubleValue]]];
                        [VWeventStatus setBackgroundColor:orangecolor];
                    }
                    
                }else
				{
                    [lblEventStatus setText:@""];
                    [VWeventStatus setBackgroundColor:[UIColor clearColor]];
                }
                
                eventView.hidden = true;
	
                vwVenueDetail.hidden = true;
                
                vwVenueTimeHieghtCons.constant = 0;
				
				venueRate.hidden = true;
				
				
			   //font change paresh
				lblAddress.font = [UIFont fontWithName:@"Helvetica" size:15];
				
			//	NSString *Starttime = [[parsedObject objectForKey:@"event"] objectForKey:@"event_start_datetime"]];
				
				NSString *StrStartTime = [self displayStartTime:[[[parsedObject objectForKey:@"event"] objectForKey:@"event_start_datetime"] doubleValue]];
				NSString *StrEndTime = [self displayEndTime:[[[parsedObject objectForKey:@"event"] objectForKey:@"event_end_datetime"] doubleValue]];
				
				lblTimeEvent.text= [NSString stringWithFormat:@"%@ - %@",[StrStartTime lowercaseString],[StrEndTime lowercaseString]];
				
				//lblTimeEvent.text= @"8:30pm - 10:30pm";
                
            }else
			{
				
				lblTimeEvent.text= @"";
				lblAddress.font = [UIFont fontWithName:@"Helvetica-Bold" size:15];
				
				//[lblAddress.text ]
				//STAR:-
				
				venueRate.hidden = false;
                
                eventView.hidden = false;
                
                [lblVenueName setText:[NSString stringWithFormat:@"%@",[[parsedObject objectForKey:@"event"] objectForKey:@"event_name"]]];
                
                if([[[parsedObject objectForKey:@"event"] objectForKey:@"event_price"] isEqualToString:@"FREE"]){
                    [lblVenuePrice setText:[NSString stringWithFormat:@"%@",[[parsedObject objectForKey:@"event"] objectForKey:@"event_price"]]];
                }else{
					//presh $
                    [lblVenuePrice setText:[NSString stringWithFormat:@"%@",[[parsedObject objectForKey:@"event"] objectForKey:@"event_price"]]];
                }
				
				//presh
              //  venueRate.starSize = 12.0;
               // venueRate.rating = [[dict objectForKey:@"event_rating"] floatValue];
               // venueRate.starFillColor = [UIColor colorWithRed:235.0/255.0f green:129.0/255.0f
                                                              // blue:10.0/255.0 alpha:1.0];
                
                
                if(![[[parsedObject objectForKey:@"event"] objectForKey:@"event_start_datetime"] isEqual: [NSNull null]] || ![[[parsedObject objectForKey:@"event"] objectForKey:@"event_end_datetime"] isEqual:[NSNull null]]){
                    
                    BOOL is24Hours;
                    
                    NSMutableDictionary *dictHours = [[[[parsedObject objectForKey:@"event"] objectForKey:@"more_details"] objectForKey:@"operating_hours"] mutableCopy];
                    
                    NSString *dayName = [CommonMethod getCurrentWeekDay];
                    
                    if([CommonMethod isTimeZoneIn24Hours]){
                        is24Hours = true;
                    }else{
                        is24Hours = false;
                    }
                    
                    if(dictHours[dayName]){
                        
                        if([[[dictHours objectForKey:dayName] objectForKey:@"start"]isEqualToString:@""]){
                            //[lblDateNTime setText:@""];
                            [lblEventStatus setText:@""];
                            [VWeventStatus setBackgroundColor:[UIColor clearColor]];
                        }else{
                            
                            if([[[dictHours objectForKey:dayName] objectForKey:@"start"] isEqualToString:@"Open 24 hours"]){
                                
                                //[lblDateNTime setText:[[dictHours objectForKey:dayName] objectForKey:@"start"]];
                                
                                [lblEventStatus setText:@"Open\nNow"];
                                [VWeventStatus setBackgroundColor:[UIColor colorWithRed:(4.0/255.0) green:(149.0/255.0) blue:(149.0/255.0) alpha:1.0]];
								
                                
                            }else if([[[dictHours objectForKey:dayName] objectForKey:@"start"] isEqualToString:@"Closed"]){
                                
                                //If Start time is close thn display time and status as close.
                                //[lblDateNTime setText:[[dictHours objectForKey:dayName] objectForKey:@"start"]];
                                
                                //[cell.lblStats setText:@"Closed Now"];
                                //[cell.lblStats setBackgroundColor:[UIColor redColor]];
                                
                                [lblEventStatus setText:@""];
                                [VWeventStatus setBackgroundColor:[UIColor clearColor]];
                                
                            }else{
                                
                                NSInteger startTime = 0,endTime = 0;
                                NSString *startTimeAMPM,*endTimeAMPM;
                                
                                if([[[dictHours objectForKey:dayName] objectForKey:@"end"] rangeOfString:@","].location != NSNotFound){
                                    
                                    NSString *strendTime = [[[[dictHours objectForKey:dayName] objectForKey:@"end"]componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@","]]objectAtIndex:0];
                                    
                                    NSArray *ary = [strendTime componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
                                    
                                    endTime = [[ary objectAtIndex:0] integerValue];
									
									
									if (ary.count>1)
									{
										endTimeAMPM = [ary objectAtIndex:1];
										
									}else
									{
										startTimeAMPM =@"PM";
									}
									
								
                                    
                                    ary = [[[dictHours objectForKey:dayName] objectForKey:@"start"]componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
                                    
                                    startTime = [[ary objectAtIndex:0] integerValue];
									
									
			
									if (ary.count>1)
									{
										startTimeAMPM = [ary objectAtIndex:1];
										
									}else
									{
										startTimeAMPM =@"AM";
									}
                                }else{
                                    
                                    NSArray *ary = [[[dictHours objectForKey:dayName] objectForKey:@"start"]componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
                                    
                                    startTime = [[ary objectAtIndex:0] integerValue];
									
									
									if (ary.count>1)
									{
										startTimeAMPM = [ary objectAtIndex:1];
										
									}else
									{
										startTimeAMPM =@"AM";
									}


                                    ary = [[[dictHours objectForKey:dayName] objectForKey:@"end"]componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
                                    
                                    endTime = [[ary objectAtIndex:0] integerValue];
                                    
                                    //endTimeAMPM = [ary objectAtIndex:1];
									
									if (ary.count>1)
									{
										endTimeAMPM = [ary objectAtIndex:1];
										
									}else
									{
										endTimeAMPM =@"PM";
									}
                                    //[lblDateNTime setText:[NSString stringWithFormat:@"%@ - %@",[[dictHours objectForKey:dayName] objectForKey:@"start"],[[dictHours objectForKey:dayName] objectForKey:@"end"]]];
                                }
						
                                
                                if(is24Hours){
                                    
                                    if([startTimeAMPM isEqualToString:@"pm"] || [startTimeAMPM isEqualToString:@"PM"]){
                                        startTime += 12;
                                    }else{
                                        startTime = startTime;
                                    }
                                    
                                    if([endTimeAMPM isEqualToString:@"pm"] || [endTimeAMPM isEqualToString:@"PM"]){
                                        endTime += 12;
                                    }else{
                                        endTime = endTime;
                                    }
                                    
                                    if([CommonMethod getCurrentHours] >= startTime && [CommonMethod getCurrentHours] <= endTime){
                                        [lblEventStatus setText:OPENNOW];
                                        [VWeventStatus setBackgroundColor:greencolor];
                                        //[self.lblStats setBackgroundColor:[UIColor colorWithRed:(0.0/255.0) green:(145.0/255.0) blue:(71.0/255.0) alpha:1.0]];
                                    }else{
										
										NSString *strStarTimeFinal = [[[dictHours objectForKey:dayName] objectForKey:@"start"] uppercaseString];
										
										[lblEventStatus setText:[NSString stringWithFormat:@"OPEN\n%@",strStarTimeFinal]];
										
										
                                      //  [lblEventStatus setText:[NSString stringWithFormat:@"OPEN\n%ld%@",(long)startTime,[startTimeAMPM uppercaseString]]];
                                        //[self.lblStats setBackgroundColor:[UIColor colorWithRed:(249.0/255.0) green:(120.0/255.0) blue:(6.0/255.0) alpha:1.0]];
                                        [VWeventStatus setBackgroundColor:orangecolor];
                                    }
                                    
                                }else{
                                    
                                    if([CommonMethod getCurrentHours] >= startTime && [CommonMethod getCurrentHours] <= endTime){
                                        
                                        [lblEventStatus setText:OPENNOW];
                                        [VWeventStatus setBackgroundColor:greencolor];
                                    }else{
										
										
										NSString *strStarTimeFinal = [[[dictHours objectForKey:dayName] objectForKey:@"start"] uppercaseString];
										
										[lblEventStatus setText:[NSString stringWithFormat:@"OPEN\n%@",strStarTimeFinal]];
										
                                      //  [lblEventStatus setText:[NSString stringWithFormat:@"OPEN\n%ld%@",(long)startTime,[startTimeAMPM uppercaseString]]];
                                        
                                        [VWeventStatus setBackgroundColor:orangecolor];
                                    }
                                    
                                }
                            }
                        }
                    }else{
                        
                        [lblEventStatus setText:@""];
                        [VWeventStatus setBackgroundColor:[UIColor clearColor]];
                        //[self.lblStats setHidden:true];
                    }
                    
                }else{
                    [lblEventStatus setText:@""];
                    [VWeventStatus setBackgroundColor:[UIColor clearColor]];
                }
                
                lblVenueCatName.hidden = false;
                [lblVenueCatName setText:[NSString stringWithFormat:@"%@",[[parsedObject objectForKey:@"event"] objectForKey:@"event_categories"]]];
                
                strEventName = [NSString stringWithFormat:@"%@",[[parsedObject objectForKey:@"event"] objectForKey:@"event_name"]];
                
                [lblVenueName setText:[NSString stringWithFormat:@"%@",[[parsedObject objectForKey:@"event"] objectForKey:@"event_name"]]];
                
                [lblVenueCategoryName setText:[NSString stringWithFormat:@"%@",[[parsedObject objectForKey:@"event"] objectForKey:@"event_categories"]]];
				
				
                if([[[parsedObject objectForKey:@"event"] objectForKey:@"event_price"] isEqualToString:@"FREE"]){
                    [lblPrice setText:[NSString stringWithFormat:@"%@",[[parsedObject objectForKey:@"event"] objectForKey:@"event_price"]]];
                }else{
					
					//paresh change remove :$
                    [lblPrice setText:[NSString stringWithFormat:@"%@",[[parsedObject objectForKey:@"event"] objectForKey:@"event_price"]]];
                }
				
				
				//For star change colre create second method:- paresh hirpara
                venueRate.rating = [[[parsedObject objectForKey:@"event"] objectForKey:@"event_rating"] floatValue];
				venueRate.starFillColorFlag = FALSE;
				venueRate.starFillColor = [UIColor redColor];
				
                venueRate.starSize = 10.0;
                
                vwVenueTimeHieghtCons.constant = 120;
                //HIRPARA
				
				NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
				[dateFormatter setDateFormat:@"EEEE"];
				NSDate *now = [NSDate date];
				for (int i = 0; i < 7; i++)
				{
					NSDate *date = [NSDate dateWithTimeInterval:+(i * (60 * 60 * 24)) sinceDate:now];
					[arrykeyweakfromday addObject:[dateFormatter stringFromDate:date]];
				}
				
                arryOperatingHours = [[[parsedObject objectForKey:@"event"] objectForKey:@"more_details"] objectForKey:@"operating_hours"];
				arrKeys = [[[[[parsedObject objectForKey:@"event"] objectForKey:@"more_details"] objectForKey:@"operating_hours"] allKeys] mutableCopy];
				
				
                if (arryOperatingHours.count > 0){
                    vwVenueDetail.hidden = false;
                }else{
                    vwVenueDetail.hidden = true;
                }
                
                [tblVenueTime reloadData];
                
            }
            
            [lblAddress setText:[NSString stringWithFormat:@"%@",[[parsedObject objectForKey:@"event"] objectForKey:@"event_address"]]];
            
            [lblWebsite setText:[NSString stringWithFormat:@"%@",[[parsedObject objectForKey:@"event"] objectForKey:@"event_website"]]];
            
            strWebsite = lblWebsite.text;
            
            if([[[parsedObject objectForKey:@"event"]objectForKey:@"is_favorite"] boolValue]){
                [btnFav setSelected:YES];
            }else{
                [btnFav setSelected:NO];
            }
            
            tvDesc.text = [NSString stringWithFormat:@"%@",[[parsedObject objectForKey:@"event"] objectForKey:@"event_description"]];
            
            CGSize sizeThatFitsTextView = [tvDesc sizeThatFits:tvDesc.frame.size];
            
            if (sizeThatFitsTextView.height < 80)
			{
                
                tvDescHieghtCons.constant = sizeThatFitsTextView.height;
                
                btnDescHieghtCons.constant = 0;
				
				vwDetailHeightCons.constant = vwEventDetail.frame.size.height - 45;
                
            }else
			{
	
                tvDescHieghtCons.constant = 80;
                btnDescHieghtCons.constant = 30;
				
            }
            
            if([[[parsedObject objectForKey:@"event"] objectForKey:@"event_latitude"] isEqual:[NSNull null]] && [[[parsedObject objectForKey:@"event"] objectForKey:@"event_longitude"] isEqual:[NSNull null]]){
                
                coord = CLLocationCoordinate2DMake(0.0, 0.0);
                
            }else{
                
                coord = CLLocationCoordinate2DMake([[[parsedObject objectForKey:@"event"] objectForKey:@"event_latitude"] floatValue], [[[parsedObject objectForKey:@"event"] objectForKey:@"event_longitude"] floatValue]);
            }
            
            MKCoordinateSpan span = MKCoordinateSpanMake(20, 20);
            MKCoordinateRegion region = {coord, span};
            
            MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
            [annotation setCoordinate:coord];
            
            
            annotation.title = [[parsedObject objectForKey:@"event"] objectForKey:@"event_address"];
            
            [eventMap setRegion:region];
            [eventMap addAnnotation:annotation];
            
            [eventMap selectAnnotation:annotation animated:true];
            
            if([[[parsedObject objectForKey:@"event"] objectForKey:@"event_latitude"] isEqual:[NSNull null]] || [[[parsedObject objectForKey:@"event"] objectForKey:@"event_longitude"] isEqual:[NSNull null]]){
                
                [lblMiles setText:@""];
                
            }else{
                
                if(AppDel.crntLat == 0.0 && AppDel.crntLat == 0.0){
                    
                    [lblMiles setText:@"No mi"];
                    
                }else{
                    
                    CLLocation *currentLocation = [[CLLocation alloc]initWithLatitude:AppDel.crntLat longitude:AppDel.crntLon];
                    
                    CLLocation *serverLocation = [[CLLocation alloc]initWithLatitude:[[[parsedObject objectForKey:@"event"] objectForKey:@"event_latitude"] floatValue] longitude:[[[parsedObject objectForKey:@"event"] objectForKey:@"event_longitude"] floatValue]];
                    
                    CLLocationDistance meters = [currentLocation distanceFromLocation:serverLocation];
                    
                    [lblMiles setText:[NSString stringWithFormat:@"%.1f mi",(meters * 0.000621371)]];
                    
                }
            }
            
            [eventMap setUserInteractionEnabled:false];
            strDateOfEvent = [NSString stringWithFormat:@"%@ %@",lblDate.text,[lblMonth.text uppercaseString]];
            strAddress = [NSString stringWithFormat:@"%@",[[parsedObject objectForKey:@"event"] objectForKey:@"event_address"]];
            strDescription = [NSString stringWithFormat:@"%@",[[parsedObject objectForKey:@"event"] objectForKey:@"event_description"]];
            
        }else{
            DisplayAlert([parsedObject objectForKey:res_Message]);
        }
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error){
        [CommonMethod hideLoader];
        DisplayAlert(@"Error From Web Service");
    }];
    
}

#pragma mark - MFMailComposeViewController Delegate
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    {
        switch (result) {
            case MFMailComposeResultSent:
                DisplayAlert(@"You sent the email.");
                break;
            case MFMailComposeResultSaved:
                DisplayAlert(@"You saved a draft of this email");
                break;
            case MFMailComposeResultCancelled:
                DisplayAlert(@"You cancelled sending this email.");
                break;
            case MFMailComposeResultFailed:
                DisplayAlert(@"Mail failed:  An error occurred when trying to compose this email");
                break;
            default:
                DisplayAlert(@"An error occurred when trying to compose this email");
                break;
        }
        
        [self dismissViewControllerAnimated:YES completion:NULL];
    }
}

#pragma mark - UICollectionview delegate and datasource

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return counter;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    DetailImageCell *cell = (DetailImageCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"DetailImageIdentifier" forIndexPath:indexPath];
    
    
    
    pageControlVW.currentPage = indexPath.row;
    
    [cell.detailImageCell sd_setImageWithURL:[NSURL URLWithString:[[arrImages objectAtIndex:indexPath.row] objectForKey:@"url"]] placeholderImage:[UIImage imageNamed:@"Placeholder"] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
     
     } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
     
     }];
    
    
    return cell;
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(self.view.frame.size.width, collectionView.frame.size.height);
}


#pragma mark - UITableView Delegate and DataSouce
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arryOperatingHours.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    VenueDetailCell *cell = (VenueDetailCell *)[tableView dequeueReusableCellWithIdentifier:@"SimpleTableItem"];
    
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
	cell.lblDayName.text = [NSString stringWithFormat:@"%@",[arrykeyweakfromday objectAtIndex:indexPath.row]];
    if ([[[arryOperatingHours valueForKey:[arrKeys objectAtIndex:indexPath.row]] objectForKey:@"start"] isEqualToString:@"Closed"]){
        
        cell.lblDayTime.text = [NSString stringWithFormat:@"Closed"];
        
    }else{
		
	cell.lblDayTime.text = [NSString stringWithFormat:@"%@ - %@ ",[[arryOperatingHours valueForKey:[arrykeyweakfromday objectAtIndex:indexPath.row]] objectForKey:@"start"]  , [[arryOperatingHours valueForKey:[arrykeyweakfromday objectAtIndex:indexPath.row]] objectForKey:@"end"] ] ;
        
    }
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 22;
    
}

#pragma mark - Memory warning
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Actions


- (IBAction)btnCallPressed:(id)sender {
	


	NSArray* words = [chekphoneis componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	NSString* nospacestring = [words componentsJoinedByString:@""];
	
    if([[UIApplication sharedApplication]canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@",nospacestring]]])
	{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@",nospacestring]]];
    }else
	{
        DisplayAlert(@"Not support");
    }
    
}

- (IBAction)downBtnPressed:(id)sender {
    
    CGSize sizeThatFitsTextView = [tvDesc sizeThatFits:tvDesc.frame.size];
    
    if (btnDescDown.isSelected){
        [btnDescDown setSelected:NO];
        
        if (sizeThatFitsTextView.height < 80){
            
            tvDescHieghtCons.constant = sizeThatFitsTextView.height;
            
            btnDescHieghtCons.constant = 0;
            
        }else{
            tvDescHieghtCons.constant = 80;
            
            btnDescHieghtCons.constant = 30;
        }
        
        vwDetailHeightCons.constant = 530;
        
    }else{
        
        [UIView animateWithDuration:5.0 animations:^{
            
            tvDescHieghtCons.constant = sizeThatFitsTextView.height;
            
            vwDetailHeightCons.constant = vwEventDetail.frame.size.height + tvDescHieghtCons.constant - 35;
            
        } completion:^(BOOL finished) {
            [btnDescDown setSelected:YES];
        }];
        
    }
    
    [self.view layoutIfNeeded];
    
}

-(IBAction)venueTimeBtnPressed:(id)sender{
    
    CGSize sizeOfVenueTime = [tblVenueTime sizeThatFits:tblVenueTime.frame.size];
    
    if(btnVenueDown.isSelected){
        
        [UIView animateWithDuration:3.0 animations:^{
            vwVenueTimeHieghtCons.constant = 120;
        } completion:^(BOOL finished) {
            [btnVenueDown setSelected:false];
        }];
        
    }else{
        
        [UIView animateWithDuration:3.0 animations:^{
            vwVenueTimeHieghtCons.constant = vwVenueDetail.frame.size.height + (sizeOfVenueTime.height + 50);
        } completion:^(BOOL finished) {
            [btnVenueDown setSelected:true];
        }];
        
        //btnVenueTimeDownHeightCons.constant = 0;
    }
    
}

-(IBAction)webSiteBtnPressed:(id)sender{
    
    if([[UIApplication sharedApplication]canOpenURL:[NSURL URLWithString:strWebsite]]){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strWebsite]];
    }else{
        DisplayAlert(@"Page not found")
    }
    
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result{
	
	///your stuff here
	[self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)btnTextFrndPressed:(id)sender {
	
	//Text Message :-
	
	if ([MFMessageComposeViewController canSendText])
	{
		
		NSMutableString *body = [NSMutableString string];
		[body appendString:@"Event Name:"];
		[body appendString:[NSString stringWithFormat:@"%@",strEventName]];

		[body appendString:@" Link:"];
		[body appendString:@"https://itunes.apple.com/us/app/buzzlynk/id1081824971?ls=1&mt=8"];
		
		MFMessageComposeViewController *messageComposer =
		[[MFMessageComposeViewController alloc] init];
		[messageComposer setBody:body];
		messageComposer.messageComposeDelegate = self;
		[self presentViewController:messageComposer animated:YES completion:nil];
	}

}

- (IBAction)btnEmailFrndPressed:(id)sender {
	//Email:-
    if ([MFMailComposeViewController canSendMail])
    {
        NSMutableString *body = [NSMutableString string];
        [body appendString:@"Event Name:"];
        [body appendString:[NSString stringWithFormat:@"%@",strEventName]];
		
       [body appendString:@"<br><br><a href='https://itunes.apple.com/us/app/buzzlynk/id1081824971?ls=1&mt=8'>Download Buzzlynk App now to see other great events happening in your area!</a>"];
		
        MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
        mail.mailComposeDelegate = self;
        [mail setSubject:[NSString stringWithFormat:@"Checkout %@",strEventName]];
        [mail setMessageBody:body isHTML:YES];
        [self presentViewController:mail animated:YES completion:NULL];
    }
    else
    {
        DisplayAlert(@"This device cannot send email");
    }
    
}
- (IBAction)btnBackPresed:(id)sender {
    
    [AppDel.slideMenuVC.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)btnFavPressed:(id)sender {
    if([CommonMethod isNetworkAvailable]){
            
        NSString *str = [[NSUserDefaults standardUserDefaults]objectForKey:@"user_id"] ;
            
        if(str.length != 0){
                
            [CommonMethod showLoadingIndicator];
                
            [manager POST:[NSString stringWithFormat:@"%@events/%@/favorite_unfavorite",baseUrlWebService,strEventId] parameters:[NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"],@"user_id", nil]
                success:^(AFHTTPRequestOperation *operation, id responseObject){
                          
                    NSError *error;
                    NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
                    
                    [CommonMethod hideLoader];
                          
                    if([[parsedObject objectForKey:res_Code] integerValue] == 200){
                              
                        if([btnFav isSelected]){
                            [btnFav setSelected:NO];
                        }else{
                            [btnFav setSelected:YES];
                        }
                    }else{
                        DisplayAlert(@"Some error in webservice");
                    }
                          
                    
                          
            }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    [CommonMethod hideLoader];
                    DisplayAlert(error.localizedFailureReason);
            }];
        }else{
            DisplayAlert(@"You are not login. Please login first");
        }
    }else{
        DisplayAlert(NET_MSG);
    }
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end