//
//  CallOutVCViewController.m
//  BuzzLynkRedesign
//
//  Created by sotsys036 on 21/04/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import "CallOutVCViewController.h"

@interface CallOutVCViewController ()

@end

@implementation CallOutVCViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.callOutView.layer.cornerRadius = 5.0;
    self.callOutView.clipsToBounds = true;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
