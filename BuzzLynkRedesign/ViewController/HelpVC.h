//
//  HelpVC.h
//  BuzzLynkRedesign
//
//  Created by sotsys036 on 07/03/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@interface HelpVC : UIViewController<UIWebViewDelegate>{
    
    __weak IBOutlet UIWebView *webViewHelp;
    
}

@end
