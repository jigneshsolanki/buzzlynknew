
#import "UpComingViewController.h"
#import "UpcomingCustomCell.h"
#import "EventListingVC.h"

@interface UpComingViewController (){
	NSArray *arrImgType ;
	
	NSInteger indx;
}

@end

@implementation UpComingViewController

#pragma mark - View Controller Methods
- (void)viewDidLoad {
	[super viewDidLoad];
	
}

-(void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
	
	arrImgType = [NSArray arrayWithObjects:@"glob",@"food",@"hotel",@"mask",@"calender",@"music", nil];
	
	manager  = [AFHTTPRequestOperationManager manager];
	manager.responseSerializer = [AFHTTPResponseSerializer serializer];
	
	arrUpComing = [NSMutableArray array];
	
	[self callUpcomingWebservice];
	
}

#pragma mark - CollectionView Delegate and datasouce
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
	return arrUpComing.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
	UpcomingCustomCell *cell = (UpcomingCustomCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"UpcomingIdentifier" forIndexPath:indexPath];
	
	indx = indexPath.row % arrImgType.count;
	
	NSString *strchek = [NSString stringWithFormat:@"%@",[[arrUpComing objectAtIndex:indexPath.row] objectForKey:@"upcoming_vector"]];
	
	if ([strchek isEqualToString:@"glob"])
	{
		cell.EventType.image = [UIImage imageNamed:[arrImgType objectAtIndex:0]];
		
	}else if ([strchek isEqualToString:@"food"])
	{
		cell.EventType.image = [UIImage imageNamed:[arrImgType objectAtIndex:1]];
	}
	else if ([strchek isEqualToString:@"hotel"])
	{
		cell.EventType.image = [UIImage imageNamed:[arrImgType objectAtIndex:2]];
	}
	else if ([strchek isEqualToString:@"mask"])
	{
		cell.EventType.image = [UIImage imageNamed:[arrImgType objectAtIndex:3]];
	}
	else if ([strchek isEqualToString:@"calender"])
	{
		cell.EventType.image = [UIImage imageNamed:[arrImgType objectAtIndex:4]];
	}
	else if ([strchek isEqualToString:@"music"])
	{
		cell.EventType.image = [UIImage imageNamed:[arrImgType objectAtIndex:5]];
	}
	
	
	cell.lblEventName.text = [NSString stringWithFormat:@"%@",[[arrUpComing objectAtIndex:indexPath.row] objectForKey:@"upcoming_name"]];
	
	cell.lblCity.text = [NSString stringWithFormat:@"%@",[[arrUpComing objectAtIndex:indexPath.row] objectForKey:@"upcoming_city"]];
	
	NSArray *ary_components = [[CommonMethod getUpcomingDate:[[[arrUpComing objectAtIndex:indexPath.row] objectForKey:@"upcoming_startdate"] doubleValue] endDate:[[[arrUpComing objectAtIndex:indexPath.row] objectForKey:@"upcoming_enddate"] doubleValue]] componentsSeparatedByString:@"-"];
	
	
	[self dateDisplay:cell startDate:[ary_components objectAtIndex:0] endDate:[ary_components objectAtIndex:1]];
	
	[cell.imgEvent sd_setImageWithURL:[NSURL URLWithString:[[arrUpComing objectAtIndex:indexPath.row] objectForKey:@"upcoming_image"]] placeholderImage:[UIImage imageNamed:@"Placeholder"] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
		
	} completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
		
	}];
	
	
	return cell;
}

-(void)dateDisplay:(UpcomingCustomCell *)cell startDate:(NSString *)startDate endDate:(NSString *)endDate
{
	
	
	//NSTimeZone *TimeZoneUTC = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
	
	NSDateFormatter *dff = [[NSDateFormatter alloc] init];
	[dff setDateFormat:@"MMM,dd yyyy"];
	//[dff setTimeZone:TimeZoneUTC];
	[dff setTimeZone:[NSTimeZone localTimeZone]];
	
	NSDate * newStartDate = [dff dateFromString:startDate];
	NSDate * newEndDate = [dff dateFromString:endDate];
	
	
	NSDateFormatter *df = [[NSDateFormatter alloc] init];
	[df setDateFormat:@"dd"];
	
	NSString * startDayString = [df stringFromDate:newStartDate];
	NSString * endDayString = [df stringFromDate:newEndDate];
	
	[df setDateFormat:@"MMM"];
	NSString * startMonthString = [[df stringFromDate:newStartDate] uppercaseString];
	NSString * endMonthString = [[df stringFromDate:newEndDate] uppercaseString];
	
	[df setDateFormat:@"yyyy"];
	
	NSString *tempStr = @"";
	// Both month and day are same
	
	if([startMonthString isEqualToString: endMonthString]  && [startDayString isEqualToString: endDayString])
	{
		tempStr = [NSString stringWithFormat:@"%@\n%@",startDayString,startMonthString];
	}
	else if ([startMonthString isEqualToString: endMonthString] && ![startDayString isEqualToString: endDayString])
	{
		tempStr = [NSString stringWithFormat:@"%@-%@\n%@",startDayString,endDayString,startMonthString];
	}
	else if (startMonthString != endMonthString && ![startDayString isEqualToString: endDayString])
	{
		tempStr = [NSString stringWithFormat:@"%@ - %@\n%@ %@",startDayString,endDayString,startMonthString,endMonthString];
	}else
	{
		tempStr = [NSString stringWithFormat:@"%@\n%@ - %@",startDayString,startMonthString,endMonthString];
	}
	
	cell.lblDate.edgeInsets = UIEdgeInsetsMake(1, 5, 1, 5);
	cell.lblDate.textAlignment = NSTextAlignmentCenter;
	cell.lblDate.text = tempStr;
	cell.lblDate.backgroundColor = bluecolor;
	cell.lblDate.layer.cornerRadius = 4.0;
	cell.lblDate.clipsToBounds = YES;
	
}

-(NSString *)convertNumberToMonthName:(NSInteger)month {
	
	NSString * dateString = [NSString stringWithFormat: @"%ld", (long)month];
	
	NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"MM"];
	NSDate* myDate = [dateFormatter dateFromString:dateString];
	
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"MMM"];
	NSString *stringFromDate = [formatter stringFromDate:myDate];
	//NSLog(@"%@", stringFromDate);
	
	return [stringFromDate uppercaseString];
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
	
	EventListingVC *eventVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"EventListingVC"];
	
	eventVC.strCategoryName = [NSString stringWithFormat:@"%@",[[arrUpComing objectAtIndex:indexPath.row] objectForKey:@"upcoming_name"]];
	eventVC.categoryId = [[[arrUpComing objectAtIndex:indexPath.row] objectForKey:@"upcoming_id"] intValue];
	eventVC.isFromCategory = false;
	
	[AppDel.slideMenuVC.mainViewController.navigationController pushViewController:eventVC animated:YES];
	
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
	
	return CGSizeMake((self.view.frame.size.width) ,230);
}


#pragma mark - Private Methods
-(void)callUpcomingWebservice{
	
	[CommonMethod showLoadingIndicator];
	
	[manager GET:[NSString stringWithFormat:@"%@%@",baseUrlWebService,upcomingCategory] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject){
		NSError *error;
		NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
		
		[CommonMethod hideLoader];
		
		if([[parsedObject objectForKey:res_Code] integerValue] == 200){
			
			arrUpComing = [[parsedObject objectForKey:@"upcoming"] mutableCopy];
			
			if(arrUpComing.count == 0)
			{
				DisplayAlert(@"No Data found");
			}
			
			[clgUpcoming reloadData];
			
		}else{
			DisplayAlert([parsedObject objectForKey:res_Message]);
		}
	}failure:^(AFHTTPRequestOperation *operation, NSError *error){
		[CommonMethod hideLoader];
		DisplayAlert(@"Error From Web Service");
		
	}];
	
}


#pragma mark - Memory Warning
- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
