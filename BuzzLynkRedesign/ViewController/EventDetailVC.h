//
//  EventDetailVC.h
//  BuzzLynkRedesign
//
//  Created by sotsys036 on 09/03/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import <MapKit/MapKit.h>
#import <MessageUI/MessageUI.h>

#import "RateView.h"
#import "SMPageControl.h"

//hardik 20-04-2016
#import "MapRequestModel.h"
#import "OpenInGoogleMapsController.h"

@interface EventDetailVC : UIViewController<MFMailComposeViewControllerDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,MFMessageComposeViewControllerDelegate>{
    

	
	IBOutlet UILabel *lblTimeEvent;
	IBOutlet UIButton *btncall;
    __weak IBOutlet UILabel *lblEventVenueName;
    __weak IBOutlet UILabel *lblPrice;
    __weak IBOutlet UILabel *lblCategory;
    __weak IBOutlet UILabel *lblMiles;
    __weak IBOutlet UILabel *lblAddress;
    __weak IBOutlet UILabel *lblWebsite;
    __weak IBOutlet UILabel *lblDate;
    __weak IBOutlet UILabel *lblMonth;
    
    __weak IBOutlet UILabel *lblVenueCategoryName;
    __weak IBOutlet UILabel *lblVenueName;
    __weak IBOutlet UILabel *lblVenuePrice;
    
    __weak IBOutlet UILabel *lblEventStatus;
    __weak IBOutlet UILabel *lblVenueCatName;
    __weak IBOutlet UILabel *lblVenueStatus;
    
    __weak IBOutlet RateView *venueRate;
    __weak IBOutlet SMPageControl *pageControlVW;
    
    __weak IBOutlet UITextView *tvDesc;
    
    __weak IBOutlet UIButton *btnDescDown;
    __weak IBOutlet UIButton *btnWebsite;
    
    __weak IBOutlet UIButton *btnFav;
    
    __weak IBOutlet MKMapView *eventMap;
    
    __weak IBOutlet UITableView *tblVenueTime;
    
    __weak IBOutlet UIButton *btnVenueDown;
    
    __weak IBOutlet UIView *vwVenueDetail;
    __weak IBOutlet UIView *vwEventDetail;
    __weak IBOutlet UIView *vwDate;
    __weak IBOutlet UIView *VWeventStatus;
    __weak IBOutlet UIView *vWVenueStatis;
    
    __weak IBOutlet UIView *eventView;
    
    
    __weak IBOutlet UICollectionView *clgImages;
    
    __weak IBOutlet NSLayoutConstraint *vwVenueHeightCons;
    
    __weak IBOutlet NSLayoutConstraint *tvDescHieghtCons;
    
    __weak IBOutlet NSLayoutConstraint *vwDetailHeightCons;
    
    __weak IBOutlet NSLayoutConstraint *vwVenueTimeHieghtCons;
    
    __weak IBOutlet NSLayoutConstraint *btnVenueTimeDownHeightCons;
    
    __weak IBOutlet NSLayoutConstraint *btnDescHieghtCons;
    
    
    
    NSMutableArray *arryOperatingHours,*arrKeys,*arrImages,*arrykeyweakfromday;
    
    AFHTTPRequestOperationManager *manager;
	
	NSString * chekphoneis;
}

@property(nonatomic,retain)NSString *strEventId;

@end
