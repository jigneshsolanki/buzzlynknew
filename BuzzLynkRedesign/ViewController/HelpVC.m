//
//  HelpVC.m
//  BuzzLynkRedesign
//
//  Created by sotsys036 on 07/03/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import "HelpVC.h"
#import "HelpCustomcell.h"
#import "PrivacyPolicyNTermsVC.h"

@interface HelpVC ()
@end
@implementation HelpVC

#pragma mark - Viewcontroller methods
- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",baseURL_Page,page_faq];
    NSURL *url = [NSURL URLWithString:strUrl];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [webViewHelp loadRequest:urlRequest];
	
}

#pragma mark - UIWebView Delegate
-(void)webViewDidStartLoad:(UIWebView *)webView{
    [CommonMethod showLoadingIndicator];
}
-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [CommonMethod hideLoader];
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [CommonMethod hideLoader];
    DisplayAlert(error.description);
}


#pragma mark - Button Actions
- (IBAction)menuBtnPressed:(id)sender {
    [AppDel.slideMenuVC toggleMenu];
}
- (IBAction)closeBntPressed:(id)sender {
    
    //[AppDel.slideMenuVC.navigationController popViewControllerAnimated:YES];
    ViewController *mainVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [AppDel.slideMenuVC setMainViewController:mainVC];
    
}

//#pragma mark - TableViewDelegate
//
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//	return UITableViewAutomaticDimension;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
//	
//	return UITableViewAutomaticDimension;
//}
//
//-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    return 2;
//}
//-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//    
//    HelpCustomcell *cell = (HelpCustomcell *)[tableView dequeueReusableCellWithIdentifier:@"HelpIdentifier" forIndexPath:indexPath];
//    
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    
//    switch (indexPath.row) {
//        case 0:{
//			
//			
//			cell.lblMenuTitle.text = @"Customer Care";
//			cell.lblMenuTitle.textColor=[UIColor colorWithRed:(25/255.f) green:(107/255.f) blue:(184/255.f) alpha:1.0f];
//			
//			// cell.lblSubTitle.text = @"1-800-333-3432";
//			cell.lblSubTitle.text = @"1-888-234-3433";
//			cell.lblSubTitle.textColor=[UIColor colorWithRed:(25/255.f) green:(107/255.f) blue:(184/255.f) alpha:1.0f];
//			[cell.lblSubTitle setHidden:NO];
//			[cell.imgArrow setHidden:YES];
//			
////            cell.lblMenuTitle.text = @"FAQ";
////			cell.lblMenuTitle.textColor=[UIColor colorWithRed:(25/255.f) green:(107/255.f) blue:(184/255.f) alpha:1.0f];
////            [cell.lblSubTitle setHidden:YES];
////             [cell.imgArrow setHidden:NO];
//        }
//            
//            break;
//             
//        case 1:{
//			cell.lblMenuTitle.text = @"Email Address";
//			cell.lblSubTitle.text = @"support@buzzlynk.com";
//			
//			cell.lblMenuTitle.textColor=[UIColor colorWithRed:(25/255.f) green:(107/255.f) blue:(184/255.f) alpha:1.0f];
//			cell.lblSubTitle.textColor=[UIColor colorWithRed:(25/255.f) green:(107/255.f) blue:(184/255.f) alpha:1.0f];
//			
//			[cell.lblSubTitle setHidden:NO];
//			[cell.imgArrow setHidden:YES];
//			
//        }
//
//             break;
//			
//            
//        default:
//            break;
//    }
//    
//    
//    return cell;
//    
//}
//
//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
////    if (indexPath.row == 0){
////        PrivacyPolicyNTermsVC *privacyPolicyVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PrivacyPolicyNTermsVC"];
////        privacyPolicyVC.strPageName = @"FAQ";
////        [AppDel.slideMenuVC.mainViewController.navigationController pushViewController:privacyPolicyVC animated:YES];
////    }
//}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
