//
//  FavoriteVC.h
//  BuzzLynkRedesign
//
//  Created by sotsys036 on 04/03/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@interface FavoriteVC : UIViewController<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>{

    __weak IBOutlet UICollectionView *clgFav;
    
    NSMutableArray *arrFav;
    
    AFHTTPRequestOperationManager *afmanager;
    
}

@end
