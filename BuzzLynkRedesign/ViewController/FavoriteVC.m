//
//  FavoriteVC.m
//  BuzzLynkRedesign
//
//  Created by sotsys036 on 04/03/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import "FavoriteVC.h"
#import "EventCustomCell.h"
#import "EventDetailVC.h"
#import "SearchVC.h"

@interface FavoriteVC ()

@end

@implementation FavoriteVC

#pragma mark - ViewController Methods
- (void)viewDidLoad {
    [super viewDidLoad];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    arrFav = [NSMutableArray array];
    
    afmanager  = [AFHTTPRequestOperationManager manager];
    afmanager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [self callFavListWebService];
}

#pragma mark - Button Actions

- (IBAction)menuBtnPressed:(id)sender {
    [CommonMethod openSideMenu];
}

- (IBAction)closeBtnPressed:(id)sender {

    ViewController *mainVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [AppDel.slideMenuVC setMainViewController:mainVC];
    
}

#pragma mark - CollectionView Delegate and datasouce
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return arrFav.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    EventCustomCell *cell = (EventCustomCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"EventListCell" forIndexPath:indexPath];
    
    cell.layer.cornerRadius = 5.0f;
    cell.clipsToBounds = YES;
    
    [cell setListCellEventDetail:[arrFav objectAtIndex:indexPath.row]];
    
    [cell.btnFav addTarget:self action:@selector(cellFavPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake((self.view.frame.size.width-8) ,200);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    EventDetailVC *eventVC = [self.storyboard instantiateViewControllerWithIdentifier:@"EventDetailVCID"];
    eventVC.strEventId = [[[arrFav objectAtIndex:indexPath.row] objectForKey:@"event_id"] stringValue];
    [AppDel.slideMenuVC.mainViewController.navigationController pushViewController:eventVC animated:YES];
    
}

#pragma mark - Private Methods
-(void)callFavListWebService{
    
    if([CommonMethod isNetworkAvailable]){
        [CommonMethod showLoadingIndicator];
        
        [afmanager GET:[NSString stringWithFormat:@"%@users/%@/favorites",baseUrlWebService,[[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"]] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject){
            NSError *error;
            NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
            
            [CommonMethod hideLoader];
            
            if([[parsedObject objectForKey:res_Code] integerValue] == 200){
                
                arrFav = [[parsedObject objectForKey:@"favourites"] mutableCopy];
                
                if(arrFav.count == 0){
                    DisplayAlert(@"No data found");
                }
                
                [clgFav reloadData];
                
            }else{
                DisplayAlert([parsedObject objectForKey:res_Message]);
            }
            
        }failure:^(AFHTTPRequestOperation *operation, NSError *error){
            [CommonMethod hideLoader];
            DisplayAlert(error.localizedDescription);
            
        }];
        
    }else{
        DisplayAlert(NET_MSG);
    }
    
}

-(void)calculateMileofEventVenue:(EventCustomCell *)cell dictionary:(NSDictionary *)dict{
    
    if([[dict objectForKey:@"event_latitude"] isEqual:[NSNull null]] || [[dict objectForKey:@"event_longitude"] isEqual:[NSNull null]]){
        
        [cell.lblDistance setText:@""];
        
    }else{
        
        if(AppDel.crntLat == 0.0 && AppDel.crntLat == 0.0){
            
            [cell.lblDistance setText:@"No mi"];
            
        }else{
            
            CLLocation *currentLocation = [[CLLocation alloc]initWithLatitude:AppDel.crntLat longitude:AppDel.crntLon];
            
            CLLocation *serverLocation = [[CLLocation alloc]initWithLatitude:[[dict objectForKey:@"event_latitude"] floatValue] longitude:[[dict objectForKey:@"event_longitude"] floatValue]];
            
            CLLocationDistance meters = [currentLocation distanceFromLocation:serverLocation];
            
            [cell.lblDistance setText:[NSString stringWithFormat:@"%.1f mi",(meters * 0.000621371)]];
            
        }
    }
    
}

/*
-(void)setStatusOfEventVenue:(EventCustomCell *)cell dictEvent:(NSDictionary *)dict{
    
    if(![[dict objectForKey:@"event_start_datetime"] isEqual: [NSNull null]] || ![[dict objectForKey:@"event_end_datetime"] isEqual:[NSNull null]]){
        
        if([[dict objectForKey:@"event_type"] isEqualToString:@"event"]){
			
            if([CommonMethod isBetweenDate:[[dict objectForKey:@"event_start_datetime"] doubleValue] andDate:[[dict objectForKey:@"event_end_datetime"] doubleValue]]){
                
                [cell.lblStats setBackgroundColor:[UIColor colorWithRed:(0.0/255.0) green:(145.0/255.0) blue:(71.0/255.0) alpha:1.0]];
                [cell.lblStats setText:@"Open Now"];
            }else{
                //[cell.lblStats setText:@"Close Now"];
                //[cell.lblStats setBackgroundColor:[UIColor redColor]];
            }
            
        }else{
            
            BOOL is24Hours;
            
            NSMutableDictionary *dictHours = [[dict objectForKey:@"operating_hours"] mutableCopy];
            
            NSString *dayName = [CommonMethod getCurrentWeekDay];
            
            if([CommonMethod isTimeZoneIn24Hours]){
                is24Hours = true;
            }else{
                is24Hours = false;
            }
            
            if(dictHours[dayName]){
                
                if([[[dictHours objectForKey:dayName] objectForKey:@"start"]isEqualToString:@""]){
                    //[lblDateNTime setText:@""];
                    [cell.lblStats setHidden:YES];
                    
                }else{
                    
                    if([[[dictHours objectForKey:dayName] objectForKey:@"start"] isEqualToString:@"Open 24 hours"]){
                        
                        //[lblDateNTime setText:[[dictHours objectForKey:dayName] objectForKey:@"start"]];
                        
                        [cell.lblStats setText:@"Open Now"];
                        [cell.lblStats setBackgroundColor:[UIColor colorWithRed:(0.0/255.0) green:(145.0/255.0) blue:(71.0/255.0) alpha:1.0]];
                        
                    }else if([[[dictHours objectForKey:dayName] objectForKey:@"start"] isEqualToString:@"Closed"]){
                        
                        //If Start time is close thn display time and status as close.
                        //[lblDateNTime setText:[[dictHours objectForKey:dayName] objectForKey:@"start"]];
                        
                        //[cell.lblStats setText:@"Closed Now"];
                        //[cell.lblStats setBackgroundColor:[UIColor redColor]];
                        
                        
                    }else{
                        
                        NSInteger startTime = 0,endTime = 0;
                        NSString *startTimeAMPM,*endTimeAMPM;
                        
                        if([[[dictHours objectForKey:dayName] objectForKey:@"end"] rangeOfString:@","].location != NSNotFound){
                            
                            NSString *strendTime = [[[[dictHours objectForKey:dayName] objectForKey:@"end"]componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@","]]objectAtIndex:0];
                            
                            NSArray *ary = [strendTime componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
                            
                            endTime = [[ary objectAtIndex:0] integerValue];
                            
							
							
							if (ary.count>1)
							{
								endTimeAMPM = [ary objectAtIndex:1];;
								
							}else
							{
								endTimeAMPM =@"AM";
							}

							
                            
                            ary = [[[dictHours objectForKey:dayName] objectForKey:@"start"]componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
                            
                            startTime = [[ary objectAtIndex:0] integerValue];
                            
							
							
							if (ary.count>1)
							{
								startTimeAMPM = [ary objectAtIndex:1];
								
							}else
							{
								startTimeAMPM =@"AM";
							}
							
							
                            
                            
                        }else{
                            
                            NSArray *ary = [[[dictHours objectForKey:dayName] objectForKey:@"start"]componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
                            
                            startTime = [[ary objectAtIndex:0] integerValue];
                            
							
							
							if (ary.count>1)
							{
								startTimeAMPM = [ary objectAtIndex:1];
								
							}else
							{
								startTimeAMPM =@"AM";
							}

                            
                            ary = [[[dictHours objectForKey:dayName] objectForKey:@"end"]componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
                            
                            endTime = [[ary objectAtIndex:0] integerValue];
                            
							
							if (ary.count>1)
							{
								   endTimeAMPM = [ary objectAtIndex:1];
								
							}else
							{
								endTimeAMPM =@"AM";
							}
                            
                            //[lblDateNTime setText:[NSString stringWithFormat:@"%@ - %@",[[dictHours objectForKey:dayName] objectForKey:@"start"],[[dictHours objectForKey:dayName] objectForKey:@"end"]]];
                        }
                        
                        /*if(is24Hours){
                         
                         
                         
                         if([CommonMethod getCurrentHours] >= startTime && [CommonMethod getCurrentHours] <= endTime){
                         [lblStatus setText:@"Open Now"];
                         }else{
                         [lblStatus setText:@"Closed Now"];
                         [lblStatus setTextColor:[UIColor redColor]];
                         }
                         
                         }else{
                             
                             if([startTimeAMPM isEqualToString:@"pm"] || [startTimeAMPM isEqualToString:@"PM"]){
                                 startTime += 12;
                             }
                             
                             if([endTimeAMPM isEqualToString:@"pm"] || [endTimeAMPM isEqualToString:@"PM"]){
                                 endTime += 12;
                             }
                             
                             if([CommonMethod getCurrentHours] >= startTime && [CommonMethod getCurrentHours] <= endTime){
                                 [cell.lblStats setText:@"Open Now"];
                                 [cell.lblStats setBackgroundColor:[UIColor colorWithRed:(0.0/255.0) green:(145.0/255.0) blue:(71.0/255.0) alpha:1.0]];
                             }else{
                                 //[cell.lblStats setText:@"Closed Now"];
                                 //[cell.lblStats setBackgroundColor:[UIColor redColor]];
                                 
                             }
                         }
                    }
                }
            }else{
                
                //[lblDateNTime setText:@""];
                [cell.lblStats setHidden:true];
            }
        }
    }
}
 */

#pragma mark - cell Button Pressed
-(IBAction)cellFavPressed:(id)sender{
    UIButton *btn = (UIButton *)sender;
    
    NSString *str =[[NSUserDefaults standardUserDefaults]objectForKey:@"user_id"];
    
    if(str.length == 0){
        DisplayAlert(@"You are not logged in");
    }else{
        
        if([CommonMethod isNetworkAvailable]){
            
            [CommonMethod showLoadingIndicator];
            
            [afmanager POST:[NSString stringWithFormat:@"%@events/%ld/favorite_unfavorite",baseUrlWebService,(long)btn.tag] parameters:[NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"],@"user_id", nil]
                  success:^(AFHTTPRequestOperation *operation, id responseObject){
                      
                      NSError *error;
                      NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
                      
                      if([[parsedObject objectForKey:res_Code] integerValue] == 200){
                          [self callFavListWebService];
                      }else{
                          DisplayAlert(@"Some error in webservice");
                      }
                      
                      [CommonMethod hideLoader];
                      
                  }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                      [CommonMethod hideLoader];
                      DisplayAlert(error.localizedFailureReason);
                  }];
            
        }else{
            DisplayAlert(NET_MSG);
        }
        
    }
}

#pragma mark - Memory Warning.
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
