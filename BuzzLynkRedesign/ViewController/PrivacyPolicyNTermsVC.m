//
//  PrivacyPolicyNTermsVC.m
//  BuzzLynkRedesign
//
//  Created by sotsys036 on 15/04/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import "PrivacyPolicyNTermsVC.h"

@interface PrivacyPolicyNTermsVC ()

@end

@implementation PrivacyPolicyNTermsVC

#pragma mark - View controller methods
- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *strUrl = @"";
    
    if([self.strPageName isEqualToString:@"FAQ"]){
        
        lblTitle.text = @"FAQ";
        strUrl = [NSString stringWithFormat:@"%@%@",baseURL_Page,page_faq];
        
    }else if ([self.strPageName isEqualToString:@"Privacy Policy"]){
        
        lblTitle.text = @"Privacy Policy";
        strUrl = [NSString stringWithFormat:@"%@%@",baseURL_Page,page_Privacy];
        
    }else{
        
        lblTitle.text = @"Terms of User";
        strUrl = [NSString stringWithFormat:@"%@%@",baseURL_Page,page_Term];
        
    }
    
    NSURL *url = [NSURL URLWithString:strUrl];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    
    [webViewPrivacy loadRequest:urlRequest];
}

#pragma mark - Button actions
- (IBAction)backButtonPressed:(id)sender {
    
    [AppDel.slideMenuVC.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - Web view delegate
-(void)webViewDidStartLoad:(UIWebView *)webView{
    [CommonMethod showLoadingIndicator];
}
-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [CommonMethod hideLoader];
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [CommonMethod hideLoader];
    DisplayAlert(error.description);
}


#pragma mark - Memory Warning
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
