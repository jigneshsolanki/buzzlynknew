//
//  PrivacyPolicyNTermsVC.h
//  BuzzLynkRedesign
//
//  Created by sotsys036 on 15/04/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@interface PrivacyPolicyNTermsVC : UIViewController<UIWebViewDelegate>{
    
    __weak IBOutlet UILabel *lblTitle;
    
    __weak IBOutlet UIWebView *webViewPrivacy;
    
}

@property(nonatomic,retain) NSString *strPageName;

@end
