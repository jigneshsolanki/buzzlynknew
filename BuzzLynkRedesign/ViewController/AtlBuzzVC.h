//
//  AtlBuzzVC.h
//  BuzzLynkRedesign
//
//  Created by sotsys036 on 02/03/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import <MapKit/MapKit.h>
#import "EventMapCustomCell.h"
#import "MyAnnotation.h"


@interface AtlBuzzVC : UIViewController<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,MKMapViewDelegate>{
	
	UIImageView *imageView;
	
    AFHTTPRequestOperationManager *manager;
    
    NSMutableArray *arrAtlBuzz;
    
    NSInteger selectedCell;
	EventMapCustomCell *customCellMap;
	
    __weak IBOutlet UICollectionView *clgAtlBuzz;
    
    __weak IBOutlet MKMapView *eventMap;
    
    __weak IBOutlet UIButton *btnShowMap;
    __weak IBOutlet UIButton *btnCurrentLocation;
    
    __weak IBOutlet NSLayoutConstraint *mapHeight;
	
	int onlyonetime;
	

	MyAnnotation *annotObj;
	
}
@property (nonatomic, retain) CLLocation* initialLocation;

@end
