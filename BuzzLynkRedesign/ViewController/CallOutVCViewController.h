//
//  CallOutVCViewController.h
//  BuzzLynkRedesign
//
//  Created by sotsys036 on 21/04/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyAnnotation.h"

@interface CallOutVCViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *callOutView;
@property (weak, nonatomic) IBOutlet UILabel *lblMiles;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
@property (weak, nonatomic) IBOutlet UIButton *btnArrow;

@property (retain,nonatomic) MyAnnotation *ann;

@end
