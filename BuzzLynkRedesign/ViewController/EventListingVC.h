//
//  EventListingVC.h
//  BuzzLynkRedesign
//
//  Created by sotsys036 on 05/03/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "Constants.h"
#import "FXBlurView.h"
#import <QuartzCore/QuartzCore.h>



@interface EventListingVC : UIViewController<UITableViewDataSource,UITableViewDelegate,MKMapViewDelegate,UIAlertViewDelegate>{
	
	UIImageView *imageView;
	
	
    __weak IBOutlet UILabel *lblTitle;
    
    __weak IBOutlet UIButton *btnFilter;
    __weak IBOutlet UIButton *btnShowMap;
    __weak IBOutlet UIButton *btnCrntLocation;
    
    __weak IBOutlet FXBlurView *filterView;
    
    __weak IBOutlet NSLayoutConstraint *tableHeightCons;
    
    __weak IBOutlet UITableView *tblSubCategories;
    
    __weak IBOutlet UICollectionView *clgEvent;
    
    __weak IBOutlet MKMapView *eventMap;
    
    __weak IBOutlet NSLayoutConstraint *mapHeight;
	
	int selectedindex;
    
    NSInteger selectedCell;
    
    AFHTTPRequestOperationManager *manager;
	UITableViewCell *cell;
    
    NSMutableArray *arryEventVenue,*filterArry;
	NSString *strCategoriesCount;

}

@property (nonatomic, retain) CLLocation* initialLocation;
@property(nonatomic,retain)NSString *strCategoryName;
@property(nonatomic)int categoryId;
@property(nonatomic)BOOL isFromCategory;


@end
