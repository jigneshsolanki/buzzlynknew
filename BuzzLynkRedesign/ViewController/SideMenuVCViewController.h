//
//  SideMenuVCViewController.h
//  BuzzLynkRedesign
//
//  Created by sotsys036 on 02/03/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "CommonMethod.h"

@interface SideMenuVCViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate,MFMailComposeViewControllerDelegate>{
    __weak IBOutlet UILabel *lblSignIn;
    
    __weak IBOutlet UIImageView *imgSignIn;
    __weak IBOutlet UIImageView *imgLogo;
    
    
    NSArray *arryMenu;
}

@property(nonatomic,strong)NSString *selectedIndex;

@end
