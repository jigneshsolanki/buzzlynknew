
//
//  EventListingVC.m
//  BuzzLynkRedesign
//
//  Created by sotsys036 on 05/03/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import "EventListingVC.h"
#import "EventCustomCell.h"
#import "EventDetailVC.h"
#import "EventMapCustomCell.h"
#import "UpComingEventMapCustomCell.h"
#import "MyAnnotation.h"

#import "FXBlurView.h"
#import <QuartzCore/QuartzCore.h>

#import "YHAnimatedCircleView.h"

#define RADIUS 500

#define MAX_RATIO 1.2
#define MIN_RATIO 0.6
#define STEP_RATIO 0.05

#define ANIMATION_DURATION 0.8

//repeat forever
#define ANIMATION_REPEAT HUGE_VALF

@interface EventListingVC ()
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tblwidth;
@end

@implementation EventListingVC

@synthesize categoryId,strCategoryName,isFromCategory;

#pragma ViewController Methods
- (void)viewDidLoad {
	[super viewDidLoad];
	
	selectedCell = -1;
	mapHeight.constant = 0.0;
	
	[lblTitle setText:strCategoryName];
	
	[btnCrntLocation setHidden:YES];
	
	tblSubCategories.layer.cornerRadius = 5.0;
	
	
	self->tblSubCategories.separatorInset = UIEdgeInsetsZero;
	self->tblSubCategories.layoutMargins = UIEdgeInsetsZero;
	self->tblSubCategories.tableFooterView = [[UIView alloc] init];
	
	_tblwidth.constant = self.view.frame.size.width/2 + 40;
	
	manager  = [AFHTTPRequestOperationManager manager];
	manager.responseSerializer = [AFHTTPResponseSerializer serializer];
	
	arryEventVenue = [NSMutableArray array];
	filterArry = [NSMutableArray array];
	
	if (isFromCategory)
	{
		[self getEventList];
	}else
	{
		[self getUpcomingEventList];
		
		btnFilter.hidden = true;
	}
}

-(void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
	
}

#pragma mark - Private Methods
- (void)moveCenterByOffset:(CGPoint)offset from:(CLLocationCoordinate2D)coordinate
{
	CGPoint point = [eventMap convertCoordinate:coordinate toPointToView:eventMap];
	point.x += offset.x;
	point.y += offset.y;
	CLLocationCoordinate2D center = [eventMap convertPoint:point toCoordinateFromView:eventMap];
	[eventMap setCenterCoordinate:center animated:YES];
}

-(void)calculateMileofEventVenueForMap:(EventMapCustomCell *)cellmap dictionary:(NSDictionary *)dict{
	
	if([[dict objectForKey:@"event_latitude"] isEqual:[NSNull null]] || [[dict objectForKey:@"event_longitude"] isEqual:[NSNull null]]){
		
		[cellmap.lblMiles setText:@""];
		
	}else{
		
		if(AppDel.crntLat == 0.0 && AppDel.crntLat == 0.0){
			
			[cellmap.lblMiles setText:@"No mi"];
			
		}else{
			
			CLLocation *currentLocation = [[CLLocation alloc]initWithLatitude:AppDel.crntLat longitude:AppDel.crntLon];
			
			CLLocation *serverLocation = [[CLLocation alloc]initWithLatitude:[[dict objectForKey:@"event_latitude"] floatValue] longitude:[[dict objectForKey:@"event_longitude"] floatValue]];
			
			CLLocationDistance meters = [currentLocation distanceFromLocation:serverLocation];
			
			[cellmap.lblMiles setText:[NSString stringWithFormat:@"%.1f mi",(meters * 0.000621371)]];
			
		}
	}
	
}

-(void)calculateMileofEventVenue:(EventCustomCell *)cellCustom dictionary:(NSDictionary *)dict{
	
	if([[dict objectForKey:@"event_latitude"] isEqual:[NSNull null]] || [[dict objectForKey:@"event_longitude"] isEqual:[NSNull null]]){
		
		[cellCustom.lblDistance setText:@""];
		
	}else{
		
		if(AppDel.crntLat == 0.0 && AppDel.crntLat == 0.0){
			
			[cellCustom.lblDistance setText:@"No mi"];
			
		}else{
			
			CLLocation *currentLocation = [[CLLocation alloc]initWithLatitude:AppDel.crntLat longitude:AppDel.crntLon];
			
			CLLocation *serverLocation = [[CLLocation alloc]initWithLatitude:[[dict objectForKey:@"event_latitude"] floatValue] longitude:[[dict objectForKey:@"event_longitude"] floatValue]];
			
			CLLocationDistance meters = [currentLocation distanceFromLocation:serverLocation];
			
			[cellCustom.lblDistance setText:[NSString stringWithFormat:@"%.1f mi",(meters * 0.000621371)]];
			
		}
	}
}
/*
-(void)setStatusOfEventVenueMapView:(EventMapCustomCell *)cellMap dictEvent:(NSDictionary *)dict{
	
	if(![[dict objectForKey:@"event_start_datetime"] isEqual: [NSNull null]] || ![[dict objectForKey:@"event_end_datetime"] isEqual:[NSNull null]]){
		
		if([[dict objectForKey:@"event_type"] isEqualToString:@"event"]){
			
			//If event thn display date
			
			if([CommonMethod isBetweenDate:[[dict objectForKey:@"event_start_datetime"] doubleValue] andDate:[[dict objectForKey:@"event_end_datetime"] doubleValue]]){
				
				[cellMap.lblStatus setBackgroundColor:[UIColor colorWithRed:(0.0/255.0) green:(145.0/255.0) blue:(71.0/255.0) alpha:1.0]];
				[cellMap.lblStatus setText:@"Open Now"];
			}else{
				[cellMap.lblStatus setText:[CommonMethod displayOpenTime:[[dict objectForKey:@"event_start_datetime"] doubleValue]]];
				[cellMap.lblStatus setBackgroundColor:[UIColor colorWithRed:(249.0/255.0) green:(120.0/255.0) blue:(6.0/255.0) alpha:1.0]];
			}
			
		}else{
			
			BOOL is24Hours;
			
			NSMutableDictionary *dictHours = [[dict objectForKey:@"operating_hours"] mutableCopy];
			
			NSString *dayName = [CommonMethod getCurrentWeekDay];
			
			if([CommonMethod isTimeZoneIn24Hours]){
				is24Hours = true;
			}else{
				is24Hours = false;
			}
			
			if(dictHours[dayName]){
				
				if([[[dictHours objectForKey:dayName] objectForKey:@"start"]isEqualToString:@""]){
					//[lblDateNTime setText:@""];
					[cellMap.lblStatus setHidden:YES];
					
				}else{
					
					if([[[dictHours objectForKey:dayName] objectForKey:@"start"] isEqualToString:@"Open 24 hours"]){
						
						//[lblDateNTime setText:[[dictHours objectForKey:dayName] objectForKey:@"start"]];
						
						[cellMap.lblStatus setText:@"Open Now"];
						[cellMap.lblStatus setBackgroundColor:[UIColor colorWithRed:(0.0/255.0) green:(145.0/255.0) blue:(71.0/255.0) alpha:1.0]];
						
					}else if([[[dictHours objectForKey:dayName] objectForKey:@"start"] isEqualToString:@"Closed"]){
						
						
						
					}else{
						
						NSInteger startTime = 0,endTime = 0;
						NSString *startTimeAMPM,*endTimeAMPM;
						
						if([[[dictHours objectForKey:dayName] objectForKey:@"end"] rangeOfString:@","].location != NSNotFound){
							
							NSString *strendTime = [[[[dictHours objectForKey:dayName] objectForKey:@"end"]componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@","]]objectAtIndex:0];
							
							NSArray *ary = [strendTime componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
							
							endTime = [[ary objectAtIndex:0] integerValue];
							
							
							if (ary.count>1)
							{
								endTimeAMPM = [ary objectAtIndex:1];
								
							}else
							{
								endTimeAMPM =@"AM";
							}
							
							
							
							ary = [[[dictHours objectForKey:dayName] objectForKey:@"start"]componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
							
							startTime = [[ary objectAtIndex:0] integerValue];
							
							if (ary.count>1)
							{
								startTimeAMPM = [ary objectAtIndex:1];
								
							}else
							{
								startTimeAMPM =@"AM";
							}
							//startTimeAMPM = [ary objectAtIndex:1];
							
							
						}else{
							
							NSArray *ary = [[[dictHours objectForKey:dayName] objectForKey:@"start"]componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
							
							startTime = [[ary objectAtIndex:0] integerValue];
							
							
							if (ary.count>1)
							{
								startTimeAMPM = [ary objectAtIndex:1];
								
							}else
							{
								startTimeAMPM =@"AM";
							}
							
							
							
							ary = [[[dictHours objectForKey:dayName] objectForKey:@"end"]componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
							
							endTime = [[ary objectAtIndex:0] integerValue];
							
							
							if (ary.count>1)
							{
								endTimeAMPM = [ary objectAtIndex:1];
								
							}else
							{
								endTimeAMPM =@"PM";
							}
							
							
							
							
							//[lblDateNTime setText:[NSString stringWithFormat:@"%@ - %@",[[dictHours objectForKey:dayName] objectForKey:@"start"],[[dictHours objectForKey:dayName] objectForKey:@"end"]]];
						}
						
						/*if(is24Hours){
						 
						 
						 
						 if([CommonMethod getCurrentHours] >= startTime && [CommonMethod getCurrentHours] <= endTime){
						 [lblStatus setText:@"Open Now"];
						 }else{
						 [lblStatus setText:@"Closed Now"];
						 [lblStatus setTextColor:[UIColor redColor]];
						 }
						 
						 }else{
							 
							 if([startTimeAMPM isEqualToString:@"pm"] || [startTimeAMPM isEqualToString:@"PM"]){
								 startTime += 12;
							 }
							 
							 if([endTimeAMPM isEqualToString:@"pm"] || [endTimeAMPM isEqualToString:@"PM"]){
								 endTime += 12;
							 }
							 
							 if([CommonMethod getCurrentHours] >= startTime && [CommonMethod getCurrentHours] <= endTime){
								 [cellMap.lblStatus setText:@"Open\nNow"];
								 [cellMap.lblStatus setBackgroundColor:[UIColor colorWithRed:(0.0/255.0) green:(145.0/255.0) blue:(71.0/255.0) alpha:1.0]];
							 }else{
								 //[cell.lblStats setText:@"Closed Now"];
								 //[cell.lblStats setBackgroundColor:[UIColor redColor]];
								 [cellMap.lblStatus setText:[NSString stringWithFormat:@"Open\n%ld",(long)startTime]];
								 [cellMap.lblStatus setBackgroundColor:[UIColor colorWithRed:(249.0/255.0) green:(120.0/255.0) blue:(6.0/255.0) alpha:1.0]];
							 }
						 }
					}
				}
			}else{
				
				//[lblDateNTime setText:@""];
				[cellMap.lblStatus setHidden:true];
			}
		}
	}
	
}
*/


-(void)setEventNameForVenue:(EventCustomCell *)cellcustom dictEvent:(NSDictionary *)dict{
	NSMutableDictionary *dictDays = [[dict objectForKey:@"day_events"] mutableCopy];
	
	NSString *dayName = [CommonMethod getCurrentWeekDay];
	
	if(dictDays.count > 0){
		
		if(dictDays[dayName])
		{
			cellcustom.lblNameVenue.text = dictDays[dayName][@"event_name"];
		}
		
	}else{
		cellcustom.lblNameVenue.text = @"";
	}
}

-(void)setStatusOfEventVenue:(EventCustomCell *)cellEvent dictEvent:(NSDictionary *)dict{
	
	if(![[dict objectForKey:@"event_start_datetime"] isEqual: [NSNull null]] || ![[dict objectForKey:@"event_end_datetime"] isEqual:[NSNull null]]){
		
		if([[dict objectForKey:@"event_type"] isEqualToString:@"event"]){
			
			if([CommonMethod isBetweenDate:[[dict objectForKey:@"event_start_datetime"] doubleValue] andDate:[[dict objectForKey:@"event_end_datetime"] doubleValue]]){
				
				[cellEvent.lblStats setBackgroundColor:[UIColor colorWithRed:(0.0/255.0) green:(145.0/255.0) blue:(71.0/255.0) alpha:1.0]];
				[cellEvent.lblStats setText:@"Open Now"];
			}else{
				[cellEvent.lblStats setText:[CommonMethod displayOpenTime:[[dict objectForKey:@"event_start_datetime"] doubleValue]]];
				[cellEvent.lblStats setBackgroundColor:[UIColor colorWithRed:(249.0/255.0) green:(120.0/255.0) blue:(6.0/255.0) alpha:1.0]];
			}
			
		}else{
			
			BOOL is24Hours;
			
			NSMutableDictionary *dictHours = [[dict objectForKey:@"operating_hours"] mutableCopy];
			
			NSString *dayName = [CommonMethod getCurrentWeekDay];
			
			if([CommonMethod isTimeZoneIn24Hours]){
				is24Hours = true;
			}else{
				is24Hours = false;
			}
			
			if(dictHours[dayName]){
				
				if([[[dictHours objectForKey:dayName] objectForKey:@"start"]isEqualToString:@""]){
					//[lblDateNTime setText:@""];
					[cellEvent.lblStats setHidden:YES];
					
				}else{
					
					if([[[dictHours objectForKey:dayName] objectForKey:@"start"] isEqualToString:@"Open 24 hours"]){
						
						//[lblDateNTime setText:[[dictHours objectForKey:dayName] objectForKey:@"start"]];
						
						[cellEvent.lblStats setText:@"Open Now"];
						[cellEvent.lblStats setBackgroundColor:[UIColor colorWithRed:(0.0/255.0) green:(145.0/255.0) blue:(71.0/255.0) alpha:1.0]];
						
					}else if([[[dictHours objectForKey:dayName] objectForKey:@"start"] isEqualToString:@"Closed"]){
						
						//If Start time is close thn display time and status as close.
						//[lblDateNTime setText:[[dictHours objectForKey:dayName] objectForKey:@"start"]];
						
						//[cell.lblStats setText:@"Closed Now"];
						//[cell.lblStats setBackgroundColor:[UIColor redColor]];
						
						
					}else{
						
						NSInteger startTime = 0,endTime = 0;
						NSString *startTimeAMPM,*endTimeAMPM;
						
						if([[[dictHours objectForKey:dayName] objectForKey:@"end"] rangeOfString:@","].location != NSNotFound){
							
							NSString *strendTime = [[[[dictHours objectForKey:dayName] objectForKey:@"end"]componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@","]]objectAtIndex:0];
							
							NSArray *ary = [strendTime componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
							
							endTime = [[ary objectAtIndex:0] integerValue];
							
							
							if (ary.count>1)
							{
								endTimeAMPM = [ary objectAtIndex:1];
								
							}else
							{
								endTimeAMPM =@"PM";
							}
							
							
							ary = [[[dictHours objectForKey:dayName] objectForKey:@"start"]componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
							
							startTime = [[ary objectAtIndex:0] integerValue];
							
							
							
							if (ary.count>1)
							{
								startTimeAMPM = [ary objectAtIndex:1];
								
							}else
							{
								startTimeAMPM =@"AM";
							}
							
							
						}else{
							
							NSArray *ary = [[[dictHours objectForKey:dayName] objectForKey:@"start"]componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
							
							startTime = [[ary objectAtIndex:0] integerValue];
							
							
							
							if (ary.count>1)
							{
								startTimeAMPM = [ary objectAtIndex:1];
								
							}else
							{
								startTimeAMPM =@"AM";
							}
							
							
							ary = [[[dictHours objectForKey:dayName] objectForKey:@"end"]componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
							
							endTime = [[ary objectAtIndex:0] integerValue];
							
							
							if (ary.count>1)
							{
								endTimeAMPM = [ary objectAtIndex:1];
								
							}else
							{
								endTimeAMPM =@"AM";
							}
							
							
							//[lblDateNTime setText:[NSString stringWithFormat:@"%@ - %@",[[dictHours objectForKey:dayName] objectForKey:@"start"],[[dictHours objectForKey:dayName] objectForKey:@"end"]]];
						}
						
						/*if(is24Hours){
						 
						 
						 
						 if([CommonMethod getCurrentHours] >= startTime && [CommonMethod getCurrentHours] <= endTime){
						 [lblStatus setText:@"Open Now"];
						 }else{
						 [lblStatus setText:@"Closed Now"];
						 [lblStatus setTextColor:[UIColor redColor]];
						 }
						 
						 }else*/{
							 
							 if([startTimeAMPM isEqualToString:@"pm"] || [startTimeAMPM isEqualToString:@"PM"]){
								 startTime += 12;
							 }
							 
							 if([endTimeAMPM isEqualToString:@"pm"] || [endTimeAMPM isEqualToString:@"PM"]){
								 endTime += 12;
							 }
							 
							 if([CommonMethod getCurrentHours] >= startTime && [CommonMethod getCurrentHours] <= endTime){
								 [cellEvent.lblStats setText:OPENNOW];
								 [cellEvent.lblStats setBackgroundColor:greencolor];
							 }else{
								 //[cell.lblStats setText:@"Closed Now"];
								 //[cell.lblStats setBackgroundColor:[UIColor redColor]];
								 [cellEvent.lblStats setText:[NSString stringWithFormat:@"Open\n%ld",(long)startTime]];
								 [cellEvent.lblStats setBackgroundColor:[UIColor colorWithRed:(254.0/255.0) green:(88.0/255.0) blue:(6.0/255.0) alpha:1.0]];
								 
							 }
						 }
					}
				}
			}else{
				
				//[lblDateNTime setText:@""];
				[cellEvent.lblStats setHidden:true];
			}
		}
	}
	
}

-(void)getUpcomingEventList{
	
	if ([CommonMethod isNetworkAvailable]) {
		
		[CommonMethod showLoadingIndicator];
		
		NSString *str =[[NSUserDefaults standardUserDefaults]objectForKey:@"user_id"] ;
		
		NSMutableDictionary *dictPara = [NSMutableDictionary dictionary];
		
		if(str.length == 0){
			[dictPara setObject:@"0" forKey:@"user_id"];
		}else{
			[dictPara setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"] forKey:@"user_id"];
		}
		[dictPara setObject:[NSString stringWithFormat:@"%d",categoryId] forKey:@"upcoming_id"];
		
		[manager GET:[NSString stringWithFormat:@"%@%@",baseUrlWebService,upcomingEvent] parameters:dictPara success:^(AFHTTPRequestOperation *operation, id responseObject){
			
			NSError *error;
			NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
			
			[CommonMethod hideLoader];
			
			if([[parsedObject objectForKey:res_Code] integerValue] == 200){
				
				arryEventVenue = [[parsedObject objectForKey:@"upcoming_events"] mutableCopy];
				
				[self getLocationForMap];
				
				if(arryEventVenue.count == 0)
				{
					DisplayAlert(@"No data found");
					
				}
				
				[clgEvent reloadData];
				
			}else
			{
				DisplayAlert([parsedObject objectForKey:res_Message]);
			}
			
			
		}failure:^(AFHTTPRequestOperation *operation, NSError *error){
			[CommonMethod hideLoader];
			DisplayAlert(@"Error From Web Service");
		}];
		
	}else{
		DisplayAlert(NET_MSG);
	}
}

-(void)getEventList{
	
	if([CommonMethod isNetworkAvailable]){
		
		[CommonMethod showLoadingIndicator];
		
		NSString *str =[[NSUserDefaults standardUserDefaults]objectForKey:@"user_id"];
		
		NSMutableDictionary *dictPara = [NSMutableDictionary dictionary];
		
		NSString *lat = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:@"coordinates"] valueForKey:@"lat"]];
		NSString *longi = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:@"coordinates"] valueForKey:@"lon"]];
		
		
		if(str.length == 0){
			[dictPara setObject:@"0" forKey:@"user_id"];
			
			[dictPara setObject:lat forKey:@"latitude"];
			[dictPara setObject:longi forKey:@"longitude"];
			
		}else{
			[dictPara setObject:str forKey:@"user_id"];
			
			[dictPara setObject:lat forKey:@"latitude"];
			[dictPara setObject:longi forKey:@"longitude"];;
		}
		[manager GET:[NSString stringWithFormat:@"%@%@",baseUrlWebService,[NSString stringWithFormat:@"categories/%d",categoryId]] parameters:dictPara success:^(AFHTTPRequestOperation *operation, id responseObject){
			
			NSError *error;
			NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
			
			[CommonMethod hideLoader];
			
			if([[parsedObject objectForKey:res_Code] integerValue] == 200){
				
				arryEventVenue = [[parsedObject objectForKey:event_events] mutableCopy];
				if(filterArry.count > 0)
				{
					[filterArry removeAllObjects];
				}
				
				
				[self getLocationForMap];
				
				strCategoriesCount = [NSString stringWithFormat:@"(%lu)",(unsigned long)arryEventVenue.count];
				
				[filterArry addObject:[NSString stringWithFormat:@"All %@",strCategoriesCount]];
				[filterArry addObject:[NSString stringWithFormat:@"Sort A to Z %@",strCategoriesCount]];
				
				
				NSMutableArray *arryCatCount = [[NSMutableArray alloc]init];
				arryCatCount = [parsedObject objectForKey:@"sub_categories"];
				
				
				if (arryCatCount.count > 0)
				{
					for (int i=0; i< arryCatCount.count; i++)
					{
						if ([[[arryCatCount objectAtIndex:i] objectForKey:@"count"] integerValue] == 0)
						{
							
							//NSLog(@"Index found 0");
							
						}else
						{
							//NSLog(@"Add");
							
							[filterArry addObject:[arryCatCount objectAtIndex:i]];
							
						}
						
					}
					
				}
				
				tableHeightCons.constant = filterArry.count *51;
				
				if(arryEventVenue.count == 0){
					UIAlertView *alert = [[UIAlertView alloc]initWithTitle:APP_NAME message:@"No data found" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
					[alert show];
				}
				
				[clgEvent reloadData];
				
			}else
			{
				DisplayAlert([parsedObject objectForKey:res_Message]);
			}
			
		}failure:^(AFHTTPRequestOperation *operation, NSError *error){
			[CommonMethod hideLoader];
			DisplayAlert(@"Error From Web Service");
		}];
		
	}else{
		DisplayAlert(NET_MSG);
	}
}

-(void)getLocationForMap{
	
	[CommonMethod showLoadingIndicator];
	
	CLLocationCoordinate2D coord;
	
	CLLocationCoordinate2D coordCurnt = CLLocationCoordinate2DMake(AppDel.crntLat, AppDel.crntLon);
	
	MKCoordinateSpan span = MKCoordinateSpanMake(20, 20);
	MKCoordinateRegion region = {coordCurnt, span};
	for (id <MKAnnotation> annotation in self->eventMap.annotations)
	{
		if (![annotation isKindOfClass:[MKUserLocation class]])
		{
			[self->eventMap removeAnnotation:annotation];
		}
		
	}
	
	for (int i=0; i<[arryEventVenue count]; i++)
	{
		
		if(![[[arryEventVenue objectAtIndex:i] objectForKey:@"event_latitude"] isEqual:[NSNull null]] && ![[[arryEventVenue objectAtIndex:i] objectForKey:@"event_longitude"] isEqual:[NSNull null]]){
			
			coord.latitude = [[[arryEventVenue objectAtIndex:i] objectForKey:@"event_latitude"] floatValue];
			coord.longitude = [[[arryEventVenue objectAtIndex:i] objectForKey:@"event_longitude"] floatValue];
			
			//event_address==>	event_name
			MyAnnotation *annotObj =[[MyAnnotation alloc]initWithCoordinate:coord title:[[arryEventVenue objectAtIndex:i] objectForKey:@"event_name"] isHot:[[[arryEventVenue objectAtIndex:i] objectForKey:@"event_is_hot"] boolValue] tag:i lat:[[[arryEventVenue objectAtIndex:i] objectForKey:@"event_latitude"] floatValue] long:[[[arryEventVenue objectAtIndex:i] objectForKey:@"event_longitude"] floatValue] evtID:[[[arryEventVenue objectAtIndex:i] objectForKey:@"event_id"] stringValue]];
			
			[eventMap addAnnotation:annotObj];
		}
		
	}
	
	[eventMap setRegion:region animated:YES];
	
	[CommonMethod hideLoader];
	
}

-(void)getEventListFromSubCategory:(NSString *)strCatId{
	
	if([CommonMethod isNetworkAvailable]){
		
		
		[CommonMethod showLoadingIndicator];
		NSString *str =[[NSUserDefaults standardUserDefaults]objectForKey:@"user_id"];
		NSMutableDictionary *dictPara = [NSMutableDictionary dictionary];
		if(str.length == 0)
		{
			[dictPara setObject:@"0" forKey:@"user_id"];
		}else{
			[dictPara setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"] forKey:@"user_id"];
		}
		
		[manager GET:[NSString stringWithFormat:@"%@%@",baseUrlWebService,[NSString stringWithFormat:@"categories/%d",[strCatId intValue]]] parameters:dictPara success:^(AFHTTPRequestOperation *operation, id responseObject){
			
			NSError *error;
			NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
			
			[CommonMethod hideLoader];
			
			if([[parsedObject objectForKey:res_Code] integerValue] == 200){
				
				arryEventVenue = [[parsedObject objectForKey:event_events] mutableCopy];
				
				[self getLocationForMap];
				if(arryEventVenue.count == 0)
				{
					DisplayAlert(@"No data found");
				}
				
				[clgEvent reloadData];
				
			}else{
				DisplayAlert([parsedObject objectForKey:res_Message]);
			}
			
		}failure:^(AFHTTPRequestOperation *operation, NSError *error){
			[CommonMethod hideLoader];
			DisplayAlert(@"Error From Web Service");
		}];
		
	}else{
		DisplayAlert(NET_MSG);
	}
	
}

#pragma mark - UIAlertView Delegate and Datasource
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
	
	[AppDel.slideMenuVC.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableView Delegate and DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	
	return filterArry.count;
	
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	
	static NSString *simpleTableIdentifier = @"SimpleTableCell";
	
	cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
	
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
	}
	cell.backgroundColor = [UIColor clearColor];
	
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	
	cell.textLabel.textAlignment = NSTextAlignmentRight;
	
	cell.textLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:17.0];
	
	
	NSInteger variable = indexPath.row;
	int myInt = (int) variable;
	if (selectedindex==myInt)
	{
		cell.textLabel.font =[UIFont boldSystemFontOfSize:17];
		
	}else
	{
		cell.textLabel.font =[UIFont systemFontOfSize:17];
		
	}
	
	if (indexPath.row==0)
	{
		cell.textLabel.text = [filterArry objectAtIndex:indexPath.row];
		
		
	}else if(indexPath.row == 1)
	{
		cell.textLabel.text = [filterArry objectAtIndex:indexPath.row];
		//cell.textLabel.font =[UIFont boldSystemFontOfSize:14];
	}
	else
	{
		
	 NSString *strCatCount = [NSString stringWithFormat:@"(%@)",[[filterArry objectAtIndex:indexPath.row] objectForKey:@"count"]];
		
		cell.textLabel.text = [NSString stringWithFormat:@"%@ %@",[[filterArry objectAtIndex:indexPath.row] objectForKey:@"category_name"],strCatCount];
		
	}
	
	cell.textLabel.textColor = [UIColor colorWithRed:(54.0/255.0) green:(105.0/255.0) blue:(189.0/255.0) alpha:1.0];
	
	//[cell setSelectionStyle:UITableViewCellSelectionStyleDefault];
	
	return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPat{
	
	return 50.0;
	
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	
	NSInteger variable = indexPath.row;
	int myInt = (int) variable;
	
	selectedindex = myInt;
	[tblSubCategories reloadData];
	
	
	dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
		
		if (indexPath.row == 0){
			
			[self getEventList];
			[lblTitle setText:strCategoryName];
			
		}else if (indexPath.row == 1)
		{
			
			NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"event_name" ascending:YES];
			NSArray *sortedArray=[arryEventVenue sortedArrayUsingDescriptors:@[sort]];
			
			arryEventVenue = Nil;
			arryEventVenue = [sortedArray mutableCopy];
			[clgEvent reloadData];
			
			[lblTitle setText:strCategoryName];
			
		}
		else
		{
			
		 [self getEventListFromSubCategory:[[[filterArry objectAtIndex:indexPath.row] objectForKey:@"category_id"] stringValue]];
			
			[lblTitle setText:[[filterArry objectAtIndex:indexPath.row] objectForKey:@"category_name"]];
		}
		
		dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
			
			btnFilter.backgroundColor = [UIColor clearColor];
			btnFilter.selected = NO;
			[filterView setHidden:YES];
		});
		
	});
}


#pragma mark - CollectionView Delegate and datasouce
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
	return arryEventVenue.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
	
	if(btnShowMap.selected)
	{
		
		if (isFromCategory)
		{
			
			//From Categorites :->Deatail
			EventMapCustomCell *customCell;
			customCell = (EventMapCustomCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"EventMapCellIdentifier1" forIndexPath:indexPath];
			[customCell.btnImagepressed addTarget:self action:@selector(btnPresedIages:) forControlEvents:UIControlEventTouchUpInside];
			
			customCell.btnImagepressed.tag = indexPath.row;
			
			[customCell setDetailOfMapCell:[arryEventVenue objectAtIndex:indexPath.row]];
			
			if (selectedCell == indexPath.row)
			{
				customCell.selectedView.hidden = false;
			}else{
				customCell.selectedView.hidden = true;
			}
			
			return customCell;
		}
		else
		{
			
			//From upcoming event :-detail
			UpComingEventMapCustomCell *customCell;
			
			customCell = (UpComingEventMapCustomCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"EventMapCellIdentifier" forIndexPath:indexPath];
			[customCell.btnimagepressed addTarget:self action:@selector(btnPresedIages:) forControlEvents:UIControlEventTouchUpInside];
			
			customCell.btnimagepressed.tag = indexPath.row;
			
			[customCell setDetailOfMapCell:[arryEventVenue objectAtIndex:indexPath.row]];
			
			if (selectedCell == indexPath.row)
			{
				customCell.selectedView.hidden = false;
			}else{
				customCell.selectedView.hidden = true;
			}
			
			return customCell;
			
		}
		
	}else{
		
		//UPcoming...detatil
		EventCustomCell *cell_event = (EventCustomCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"EventListCell" forIndexPath:indexPath];
		
		cell_event.layer.cornerRadius = 5.0f;
		cell_event.clipsToBounds = YES;
		AppDel.isFromUpcoming = YES;
		
		[cell_event setListCellEventDetail:[arryEventVenue objectAtIndex:indexPath.row]];
		[cell_event.btnFav addTarget:self action:@selector(FavBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
		
		return cell_event;
		
	}
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
	
	if(btnShowMap.isSelected){
		return CGSizeMake(self.view.frame.size.width, 80);
	}else{
		return CGSizeMake((self.view.frame.size.width-8) ,200);
	}
}

- (UIEdgeInsets)collectionView:(UICollectionView *) collectionView
						layout:(UICollectionViewLayout *) collectionViewLayout
		insetForSectionAtIndex:(NSInteger) section {
	
	
	if (btnShowMap.isSelected){
		return UIEdgeInsetsMake(0, 0, 0, 0); // top, left, bottom, right
	}else{
		return UIEdgeInsetsMake(8, 0, 0, 0); // top, left, bottom, right
	}
	
	
}
- (CGFloat)collectionView:(UICollectionView *) collectionView
				   layout:(UICollectionViewLayout *) collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger) section {
	return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
	if(btnShowMap.isSelected){
		return 0.5;
	}else{
		return 8.0;
	}
	
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
	
	[self removePin];
	
	if(btnShowMap.isSelected)
	{
		
		selectedCell = indexPath.row;
		[clgEvent reloadData];
		//selectedCell = -1;
		CLLocation *loc = [[CLLocation alloc]initWithLatitude:[[[arryEventVenue objectAtIndex:indexPath.row]valueForKey:@"event_latitude"] doubleValue] longitude:[[[arryEventVenue objectAtIndex:indexPath.row]valueForKey:@"event_longitude"] doubleValue]];
		
		CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(loc.coordinate.latitude, loc.coordinate.longitude);
		[self zoomInto:coord distance:(RADIUS * 200.0) animated:YES];
		
		
	}else
	{
		EventDetailVC *eventVC = [self.storyboard instantiateViewControllerWithIdentifier:@"EventDetailVCID"];
		eventVC.strEventId = [[[arryEventVenue objectAtIndex:indexPath.row] objectForKey:@"event_id"] stringValue];
		[AppDel.slideMenuVC.mainViewController.navigationController pushViewController:eventVC animated:YES];
		
	}
	
}

-(IBAction)btnPresedIages2:(id)sender{
	
	[self removePin];
	
	UIButton *btn = (UIButton *)sender;
	EventDetailVC *eventVC = [self.storyboard instantiateViewControllerWithIdentifier:@"EventDetailVCID"];
	eventVC.strEventId = [[[arryEventVenue objectAtIndex:btn.tag] objectForKey:@"event_id"] stringValue];
	[AppDel.slideMenuVC.mainViewController.navigationController pushViewController:eventVC animated:YES];
	
	
}


-(IBAction)btnPresedIages:(id)sender{
	
	[self removePin];
	
	UIButton *btn = (UIButton *)sender;
	EventDetailVC *eventVC = [self.storyboard instantiateViewControllerWithIdentifier:@"EventDetailVCID"];
	eventVC.strEventId = [[[arryEventVenue objectAtIndex:btn.tag] objectForKey:@"event_id"] stringValue];
	[AppDel.slideMenuVC.mainViewController.navigationController pushViewController:eventVC animated:YES];
	
	
}

#pragma mark - MKMapView Delegate;
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation: (id<MKAnnotation>)annotation {
	
	if ([annotation isKindOfClass:[MyAnnotation class]]){
		MyAnnotation *myAnnotation = (MyAnnotation *)annotation;
		
		MKAnnotationView *annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:@"PinAnnotation"];
		
		if (annotationView == nil)
		{
			annotationView = myAnnotation.annotationview;
		}else{
			annotationView.annotation = annotation;
		}
		
		
		//left View
		UIButton *btnred = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50,50)];
		btnred.titleLabel.font = [UIFont systemFontOfSize:10.0];
		[btnred setTitle:myAnnotation.strDistance forState:UIControlStateNormal];
		[btnred setTitleEdgeInsets:UIEdgeInsetsMake(0,0,-20,0)]; //For bootm display labeltile
		
		if (myAnnotation.isHot){
			CLLocationCoordinate2D coord;
			coord.latitude = myAnnotation.lat;
			coord.longitude = myAnnotation.longt;
			[mapView addOverlay:[MKCircle circleWithCenterCoordinate:coord radius:RADIUS]];
			
			MKPinAnnotationView *pinAnnotation = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"PinAnnotation"];
			if ( pinAnnotation == nil )
				pinAnnotation = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"PinAnnotation"];
			
			pinAnnotation.canShowCallout = YES;
			pinAnnotation.enabled = YES;
			annotationView.image = [UIImage imageNamed:@"hotPin"];
			annotationView.centerOffset = CGPointMake(0, -annotationView.image.size.height / 2);
			btnred.backgroundColor = [UIColor redColor];
			
		}else{
			
			CLLocationCoordinate2D coord;
			coord.latitude = myAnnotation.lat;
			coord.longitude = myAnnotation.longt;
			[mapView addOverlay:[MKCircle circleWithCenterCoordinate:coord radius:RADIUS]];
			
			MKPinAnnotationView *pinAnnotation = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"PinAnnotation"];
			if ( pinAnnotation == nil )
				pinAnnotation = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"PinAnnotation"];
			
			NSLog(@"pav.frame.size = %@, pav.image.size = %@",
				  NSStringFromCGSize(pinAnnotation.frame.size),
				  NSStringFromCGSize(pinAnnotation.image.size));
			
			
			pinAnnotation.canShowCallout = YES;
			pinAnnotation.enabled = YES;
			annotationView.image = [UIImage imageNamed:@"defaultPin"];
			annotationView.centerOffset = CGPointMake(0, -annotationView.image.size.height / 2);
			btnred.backgroundColor = [UIColor colorWithRed:0 green:0.5 blue:1 alpha:1];
		}
		
		UIImageView *imgcar = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"callOutCar"]];
		imgcar.frame = CGRectMake(16, 15, imgcar.image.size.width, imgcar.image.size.height);
		[btnred addSubview:imgcar];
		
		annotationView.leftCalloutAccessoryView = btnred;
		
		//blueView name of button
		
		// Right View
		UIButton *btnDetail = [[UIButton alloc]initWithFrame:CGRectMake(0, 0,15, 15)];
		[btnDetail setImage:[UIImage imageNamed:@"websiteArrow"] forState:UIControlStateNormal];
		
		annotationView.rightCalloutAccessoryView = btnDetail;
		
		return annotationView;
		
		
	}else{
		return nil;
	}
	
}

- (void)zoomInto:(CLLocationCoordinate2D)zoomLocation distance:(CGFloat)distance animated:(BOOL)animated{
	
	MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, distance, distance);
	MKCoordinateRegion adjustedRegion = [eventMap regionThatFits:viewRegion];
	[eventMap setRegion:adjustedRegion animated:animated];
}
-(void)func_addAnimateImageView:(CGPoint)point InView:(MKAnnotationView*)view{
	UIImage* img = [UIImage imageNamed:@"redCircle.png"];
	
	if(imageView == nil){
		imageView = [[UIImageView alloc] initWithImage:img];
		imageView.frame = CGRectMake(-18, 11, 50, 50);
		imageView.image=[UIImage imageNamed:@"redCircle.png"];
		imageView.alpha = 0.3;
		[view addSubview:imageView];
	}
	
	
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view{
	[self removePin];
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
	
	[self removePin];
	selectedCell = -1;
	[clgEvent reloadData];
	
	if([eventMap overlays].count >0)
	{
		[eventMap removeOverlays:[eventMap overlays]];
		
	}
	
	NSArray *selectedAnnotations = mapView.selectedAnnotations;
	for(id annotation in selectedAnnotations) {
		[mapView deselectAnnotation:annotation animated:NO];
	}
	
	MyAnnotation *ann = (MyAnnotation *)view.annotation;
	
	EventDetailVC *eventVC = [self.storyboard instantiateViewControllerWithIdentifier:@"EventDetailVCID"];
	eventVC.strEventId = [NSString stringWithFormat:@"%@",ann.eventId];
	[AppDel.slideMenuVC.mainViewController.navigationController pushViewController:eventVC animated:YES];
	
}
-(void)removePin
{
	[imageView removeFromSuperview];
	imageView = nil;
	
}

-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
	
	[mapView removeOverlays:[mapView overlays]];
	
	CLLocationCoordinate2D coord;
	
	if (![view.annotation isKindOfClass:[MKUserLocation class]]){
		
		
		MyAnnotation *ann  = (MyAnnotation *)view.annotation;
		coord.latitude = ann.lat;
		coord.longitude = ann.longt;
		
		CGPoint point = [mapView convertCoordinate:coord toPointToView:self.view];
		[imageView removeFromSuperview];
		imageView = nil;
		[self func_addAnimateImageView:point InView:view];
		[self.view bringSubviewToFront:imageView];
		
		[mapView addOverlay:[MKCircle circleWithCenterCoordinate:coord radius:RADIUS]];
		[self zoomInto:coord distance:(RADIUS * 200.0) animated:YES];
		
		[clgEvent scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:(ann.pintag) inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredVertically animated:true];
		
		
		if(btnShowMap.isSelected){
			selectedCell = ann.pintag;
		}else{
			selectedCell = -1;
		}
		
	}else{
		selectedCell = -1;
	}
	
	[clgEvent reloadData];
	
}

#pragma mark - Custom Cell Button Pressed
-(IBAction)FavBtnPressed:(id)sender{
	UIButton *btn = (UIButton *)sender;
	
	NSString *str =[[NSUserDefaults standardUserDefaults]objectForKey:@"user_id"];
	
	if(str.length == 0){
		DisplayAlert(@"You are not logged in");
	}else{
		
		if([CommonMethod isNetworkAvailable]){
			
			[CommonMethod showLoadingIndicator];
			
			[manager POST:[NSString stringWithFormat:@"%@events/%ld/favorite_unfavorite",baseUrlWebService,(long)btn.tag] parameters:[NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"],@"user_id", nil]
				  success:^(AFHTTPRequestOperation *operation, id responseObject){
					  
					  NSError *error;
					  NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
					  
					  [CommonMethod hideLoader];
					  
					  if([[parsedObject objectForKey:res_Code] integerValue] == 200){
						  [self getEventList];
					  }else{
						  DisplayAlert(@"Some error in webservice");
					  }
					  
				  }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
					  [CommonMethod hideLoader];
					  DisplayAlert(error.localizedFailureReason);
				  }];
			
		}else{
			DisplayAlert(NET_MSG);
		}
		
	}
}

#pragma mark - Button Actions
- (IBAction)backBtnPressed:(id)sender {
	
	AppDel.isFromUpcoming = FALSE;
	[AppDel.slideMenuVC.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnShowMapPressed:(id)sender {
	
	if([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
	{
		//NSLog(@"Dont allow");
		[self setAtlantaLocation2];
	}else
	{
		//NSLog(@" allow");
	}
	
	eventMap.showsUserLocation = YES;
	if(btnShowMap.isSelected)
	{
		[btnShowMap setSelected:NO];
		[btnCrntLocation setHidden:YES];
		mapHeight.constant = 0.0;
		
	}else{
		
		[btnShowMap setSelected:YES];
		mapHeight.constant = 185;
		[btnCrntLocation setHidden:NO];
		
	}
	
	[clgEvent reloadData];
	
}
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
	if ( !_initialLocation )
	{
		self.initialLocation = userLocation.location;
		
		MKCoordinateRegion region;
		region.center = mapView.userLocation.coordinate;
		region.span = MKCoordinateSpanMake(20, 20);
		
		region = [mapView regionThatFits:region];
		[mapView setRegion:region animated:YES];
	}
}
-(void)setAtlantaLocation2
{
	CLLocationCoordinate2D coord;
	coord.latitude = 33.749065;
	coord.longitude = -84.3888742;
	MKCoordinateRegion region1;
	region1.center=coord;
	region1.span.longitudeDelta=6;
	region1.span.latitudeDelta=6;
	[eventMap setRegion:region1 animated:YES];
}

- (IBAction)btnCrntLocationPressed:(id)sender {
	
	[self setAtlantaLocation2];
	
}
- (IBAction)filterBtnPressed:(id)sender {
	
	selectedindex = 0;
	
	if(btnFilter.isSelected)
	{
		
		btnFilter.backgroundColor = [UIColor clearColor];
		btnFilter.selected = NO;
		[filterView setHidden:YES];
		
	}else
	{
		
		if(filterArry.count == 0)
		{
			DisplayAlert(@"No data found");
			return;
		}else
		{
			[tblSubCategories reloadData];
			
		}
		btnFilter.backgroundColor = [UIColor whiteColor];
		btnFilter.selected = YES;
		self->filterView.blurRadius = 10.0;
		[filterView setHidden:NO];
		
	}
	
}

#pragma mark - Memory warning
- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}
/*
	#pragma mark - Navigation
 
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	// Get the new view controller using [segue destinationViewController].
	// Pass the selected object to the new view controller.
	}
	*/

@end
