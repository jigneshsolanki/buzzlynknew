//
//  LegalVC.m
//  BuzzLynkRedesign
//
//  Created by sotsys036 on 08/04/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import "LegalVC.h"
#import "Constants.h"
#import "PrivacyPolicyNTermsVC.h"

@interface LegalVC (){
    
    NSArray *arrData;
}
@property (strong, nonatomic) IBOutlet UITableView *tbllegal;

@end

@implementation LegalVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    arrData = [NSArray arrayWithObjects:@"Privacy Policy",@"Terms of User", nil];
	//arrData = [NSArray arrayWithObjects:@"Private Policy",@"Terms of Use", nil];
    
	self.tbllegal.separatorInset = UIEdgeInsetsZero;
	self.tbllegal.layoutMargins = UIEdgeInsetsZero;
	self.tbllegal.tableFooterView = [[UIView alloc] init];
	
	
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
	// Remove seperator inset
	if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
		[cell setSeparatorInset:UIEdgeInsetsZero];
	}
	
	// Prevent the cell from inheriting the Table View's margin settings
	if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
		[cell setPreservesSuperviewLayoutMargins:NO];
	}
	
	// Explictly set your cell's layout margins
	if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
		[cell setLayoutMargins:UIEdgeInsetsZero];
	}
}


#pragma mark - UITableview delegate and datasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
	
	return UITableViewAutomaticDimension;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *simpleTableIdentifier = @"SimpleTableCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.textLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:17.0];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@",arrData[indexPath.row]];
	
	cell.textLabel.textColor=[UIColor colorWithRed:(25/255.f) green:(107/255.f) blue:(184/255.f) alpha:1.0f];
	
	// or other instantiation...
	
	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PrivacyPolicyNTermsVC *privacyPolicyVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PrivacyPolicyNTermsVC"];
	
	
	
    if(indexPath.row == 0){
        privacyPolicyVC.strPageName = @"Privacy Policy";
    }else{
        privacyPolicyVC.strPageName = @"Terms of User";
    }
    
    [AppDel.slideMenuVC.mainViewController.navigationController pushViewController:privacyPolicyVC animated:YES];
    
}

#pragma mark - Button Actions
- (IBAction)menuBtnPressed:(id)sender {
    [AppDel.slideMenuVC toggleMenu];
}

- (IBAction)closeBtnPressed:(id)sender {
    
    ViewController *mainVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [AppDel.slideMenuVC setMainViewController:mainVC];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
