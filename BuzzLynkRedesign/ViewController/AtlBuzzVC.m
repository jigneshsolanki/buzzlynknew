//
//  AtlBuzzVC.m
//  BuzzLynkRedesign
//
//  Created by sotsys036 on 02/03/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import "AtlBuzzVC.h"
#import "EventCustomCell.h"
#import "EventDetailVC.h"
#import "EventMapCustomCell.h"
#import "MyAnnotation.h"
#import "CallOutVCViewController.h"
#import "YHAnimatedCircleView.h"

#define RADIUS 500

#define MAX_RATIO 1.2
#define MIN_RATIO 0.6
#define STEP_RATIO 0.05

#define ANIMATION_DURATION 0.8

//repeat forever
#define ANIMATION_REPEAT HUGE_VALF

//static CGFloat kMyCalloutOffset = 80.0;

@interface AtlBuzzVC ()

@end

@implementation AtlBuzzVC

#pragma mark:- Viewcontroller Methods

- (void)viewDidLoad {
	
	[super viewDidLoad];
	
	
	//New commit :-
	
	
	selectedCell = -1;
	mapHeight.constant = 0;
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appToBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appReturnsActive) name:UIApplicationDidBecomeActiveNotification object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(dayFilterWebService:)
												 name:@"dayFilterationOFAtl"
											   object:nil];
	
	}

-(NSString *)displayTime:(double)unixMilliseconds {
	
	NSDate *date = [NSDate dateWithTimeIntervalSince1970:unixMilliseconds / 1000.];
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
	[dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
	//[dateFormatter setLocale:[NSLocale currentLocale]];
	[dateFormatter setDateFormat:@"hh:mm a"];
	return [dateFormatter stringFromDate:date];
}

- (void)appReturnsActive{
	
	[eventMap setShowsUserLocation:YES];
}
- (void)appToBackground{
	
	[eventMap setShowsUserLocation:NO];
}
-(void)viewWillAppear:(BOOL)animated{
	
	[super viewWillAppear:animated];
	
	[UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
	
	arrAtlBuzz = [[NSMutableArray alloc]init];
	manager  = [AFHTTPRequestOperationManager manager];
	manager.responseSerializer = [AFHTTPResponseSerializer serializer];
	
	[self callAtlBuzzWebService];
	
	
}
#pragma mark - Notification Center
-(void)dayFilterWebService:(NSNotification *)notification{
	NSDictionary *dict = notification.userInfo;
	
	[self callAtlBuzzWebServiceWithFilter:dict];
}

#pragma mark - CollectionView Delegate and datasouce
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
	return arrAtlBuzz.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
	
	if(btnShowMap.isSelected){
		
		customCellMap = (EventMapCustomCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"EventcellIdentifier" forIndexPath:indexPath];
		
		if (selectedCell == indexPath.row)
		{
			customCellMap.selectedView.hidden = false;
		}else{
			customCellMap.selectedView.hidden = true;
		}
		
		[customCellMap setDetailOfMapCell:[arrAtlBuzz objectAtIndex:indexPath.row]];
		
		[customCellMap.btnImagepressed addTarget:self action:@selector(btnImagePressed:) forControlEvents:UIControlEventTouchUpInside];
		
		customCellMap.btnImagepressed.tag = indexPath.row;
		
		return customCellMap;
		
	}else{
		
		EventCustomCell *cell = (EventCustomCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"EventListCell" forIndexPath:indexPath];
		
		cell.layer.cornerRadius = 5.0f;
		cell.clipsToBounds = YES;
		
		[cell setListCellEventDetail:[arrAtlBuzz objectAtIndex:indexPath.row]];
		
		[cell.btnFav addTarget:self action:@selector(cellFavBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
		
		return cell;
		
	}
	
}
-(void)removePin
{
	[imageView removeFromSuperview];
	imageView = nil;
	
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
	
	[self removePin];
	
	if(btnShowMap.isSelected)
	{
		
		selectedCell = indexPath.row;
		
		[clgAtlBuzz reloadData];
		
		CLLocation *loc = [[CLLocation alloc]initWithLatitude:[[[arrAtlBuzz objectAtIndex:indexPath.row]valueForKey:@"event_latitude"] doubleValue] longitude:[[[arrAtlBuzz objectAtIndex:indexPath.row]valueForKey:@"event_longitude"] doubleValue]];
		
		CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(loc.coordinate.latitude, loc.coordinate.longitude);
		[self zoomInto:coord distance:(RADIUS * 200.0) animated:YES];
		
		
	}else
	{
		EventDetailVC *eventVC = [self.storyboard instantiateViewControllerWithIdentifier:@"EventDetailVCID"];
		eventVC.strEventId = [[[arrAtlBuzz objectAtIndex:indexPath.row] objectForKey:@"event_id"] stringValue];
		[AppDel.slideMenuVC.mainViewController.navigationController pushViewController:eventVC animated:YES];
		
	}
	
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
	
	if(btnShowMap.isSelected){
		
		return CGSizeMake(self.view.frame.size.width, 80);
		
	}else{
		
		return CGSizeMake((self.view.frame.size.width-8) ,200);
	}
	
}

- (UIEdgeInsets)collectionView:(UICollectionView *) collectionView
						layout:(UICollectionViewLayout *) collectionViewLayout
		insetForSectionAtIndex:(NSInteger) section {
	
	
	if (btnShowMap.isSelected){
		return UIEdgeInsetsMake(0, 0, 0, 0); // top, left, bottom, right
	}else{
		return UIEdgeInsetsMake(8, 0, 0, 0); // top, left, bottom, right
	}
	
	
}

- (CGFloat)collectionView:(UICollectionView *) collectionView
				   layout:(UICollectionViewLayout *) collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger) section {
	return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
	if(btnShowMap.isSelected){
		return 0.5;
	}else{
		return 8.0;
	}
}
#pragma mark - MKMapView Delegate;

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation: (id<MKAnnotation>)annotation {
	
	if ([annotation isKindOfClass:[MyAnnotation class]])
	{
		
		MyAnnotation *myAnnotation = (MyAnnotation *)annotation;
		
		MKAnnotationView *annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:@"PinAnnotation"];
		
		if (annotationView == nil){
			annotationView = myAnnotation.annotationview;
		}else{
			annotationView.annotation = annotation;
		}
		
		//left View
		UIButton *btnred = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50,50)];
		btnred.titleLabel.font = [UIFont systemFontOfSize:10.0];
		[btnred setTitle:myAnnotation.strDistance forState:UIControlStateNormal];
		[btnred setTitleEdgeInsets:UIEdgeInsetsMake(0,0,-20,0)]; //For bootm display labeltile
		if (myAnnotation.isHot){
			CLLocationCoordinate2D coord;
			coord.latitude = myAnnotation.lat;
			coord.longitude = myAnnotation.longt;
			
			MKPinAnnotationView *pinAnnotation = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"PinAnnotation"];
			if ( pinAnnotation == nil )
				pinAnnotation = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"PinAnnotation"];
			
			pinAnnotation.canShowCallout = YES;
			pinAnnotation.enabled = YES;
			annotationView.image = [UIImage imageNamed:@"hotPin"];
			annotationView.centerOffset = CGPointMake(0, -annotationView.image.size.height / 2);
			btnred.backgroundColor = [UIColor redColor];
			
		}else{
			
			CLLocationCoordinate2D coord;
			coord.latitude = myAnnotation.lat;
			coord.longitude = myAnnotation.longt;
			
			MKPinAnnotationView *pinAnnotation = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"PinAnnotation"];
			if ( pinAnnotation == nil )
				pinAnnotation = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"PinAnnotation"];
			
			pinAnnotation.canShowCallout = YES;
			pinAnnotation.enabled = YES;
			annotationView.image = [UIImage imageNamed:@"defaultPin"];
			annotationView.centerOffset = CGPointMake(0, -annotationView.image.size.height / 2);
			btnred.backgroundColor = [UIColor colorWithRed:0 green:0.5 blue:1 alpha:1];
		}
		
		
		UIImageView *imgcar = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"callOutCar"]];
		imgcar.frame = CGRectMake(16, 15, imgcar.image.size.width, imgcar.image.size.height);
		[btnred addSubview:imgcar];
		
		annotationView.leftCalloutAccessoryView = btnred;
		
		// Right View
		UIButton *btnDetail = [[UIButton alloc]initWithFrame:CGRectMake(0, 0,15, 15)];
		[btnDetail setImage:[UIImage imageNamed:@"websiteArrow"] forState:UIControlStateNormal];
		annotationView.rightCalloutAccessoryView = btnDetail;
		
		return annotationView;
		
	}else{
		
		return nil;
	}
	
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view{
	
	[self removePin];
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
	[self removePin];
	selectedCell = -1;
	[clgAtlBuzz reloadData];
	
	if([eventMap overlays].count >0)
	{
		[eventMap removeOverlays:[eventMap overlays]];
		
	}
	
	NSArray *selectedAnnotations = mapView.selectedAnnotations;
	for(id annotation in selectedAnnotations) {
		[mapView deselectAnnotation:annotation animated:NO];
	}
	
	MyAnnotation *ann = (MyAnnotation *)view.annotation;
	EventDetailVC *eventVC = [self.storyboard instantiateViewControllerWithIdentifier:@"EventDetailVCID"];
	
	eventVC.strEventId = [NSString stringWithFormat:@"%@",ann.eventId];
	[AppDel.slideMenuVC.mainViewController.navigationController pushViewController:eventVC animated:YES];
	
}

#pragma mark - Helper
- (void)zoomInto:(CLLocationCoordinate2D)zoomLocation distance:(CGFloat)distance animated:(BOOL)animated{
	
	MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, distance, distance);
	MKCoordinateRegion adjustedRegion = [eventMap regionThatFits:viewRegion];
	[eventMap setRegion:adjustedRegion animated:animated];
}

-(void)func_addAnimateImageView:(CGPoint)point InView:(MKAnnotationView*)view{
	UIImage* img = [UIImage imageNamed:@"redCircle.png"];
	
	if(imageView == nil){
		
		imageView = [[UIImageView alloc] initWithImage:img];
		imageView.frame = CGRectMake(-16, 16, 45, 45);
		//imageView.center = view.center;
		imageView.image=[UIImage imageNamed:@"redCircle.png"];
		imageView.alpha = 0.3;
		[view addSubview:imageView];
		
	}
}

-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
	
	[mapView removeOverlays:[mapView overlays]];
	CLLocationCoordinate2D coord;
	
	if (![view.annotation isKindOfClass:[MKUserLocation class]]){
		
		MyAnnotation *ann  = (MyAnnotation *)view.annotation;
		coord.latitude = ann.lat;
		coord.longitude = ann.longt;
		
		CGPoint point = [mapView convertCoordinate:coord toPointToView:self.view];
		[imageView removeFromSuperview];
		imageView = nil;
		[self func_addAnimateImageView:point InView:view];
		[self.view bringSubviewToFront:imageView];
		
		[mapView addOverlay:[MKCircle circleWithCenterCoordinate:coord radius:RADIUS]];
		[self zoomInto:coord distance:(RADIUS * 200.0) animated:YES];
		
		[clgAtlBuzz scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:(ann.pintag) inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredVertically animated:true];
		
		if(btnShowMap.isSelected)
		{
			selectedCell = ann.pintag;
		}else
		{
			selectedCell = -1;
		}
	}else
	{
		selectedCell = -1;
	}
	
	[clgAtlBuzz reloadData];
	
}

#pragma mark:- Private Methods
- (void)moveCenterByOffset:(CGPoint)offset from:(CLLocationCoordinate2D)coordinate
{
	CGPoint point = [eventMap convertCoordinate:coordinate toPointToView:eventMap];
	point.x += offset.x;
	point.y += offset.y;
	CLLocationCoordinate2D center = [eventMap convertPoint:point toCoordinateFromView:eventMap];
	[eventMap setCenterCoordinate:center animated:YES];
}

-(void)callAtlBuzzWebServiceWithFilter: (NSDictionary *)dict{
	if([CommonMethod isNetworkAvailable]){
		
		[CommonMethod showLoadingIndicator];
		
		NSString *lat = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:@"coordinates"] valueForKey:@"lat"]];
		NSString *longi = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:@"coordinates"] valueForKey:@"lon"]];
		
		[manager GET:[NSString stringWithFormat:@"%@%@",baseUrlWebService,featureEvent] parameters:[NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"] ,@"user_id",[dict objectForKey:@"week_day"],@"week_day",[dict objectForKey:@"day_night"],@"day_night",lat,@"latitude",longi,@"longitude",nil] success:^(AFHTTPRequestOperation *operation, id responseObject){
			NSError *error;
			NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
			
			[CommonMethod hideLoader];
			
			if([[parsedObject objectForKey:res_Code] integerValue] == 200){
				
				arrAtlBuzz = [CommonMethod sortAnArrayByHotevent:[[parsedObject objectForKey:@"featured_events"] mutableCopy]];
				
				//NSLog(@"Total event is :->> %lu",(unsigned long)arrAtlBuzz.count);
				
				if(arrAtlBuzz.count > 0){
					[clgAtlBuzz reloadData];
				}else{
					DisplayAlert(@"No Data found");
				}
				
			}else{
				DisplayAlert([parsedObject objectForKey:res_Message]);
			}
			
		}failure:^(AFHTTPRequestOperation *operation, NSError *error){
			[CommonMethod hideLoader];
			DisplayAlert(@"Error From Web Service");
			
		}];
		
	}else{
		DisplayAlert(NET_MSG);
	}
}

/*
-(void)setStatusOfEventVenueMapView:(EventMapCustomCell *)cell dictEvent:(NSDictionary *)dict{
	
	if(![[dict objectForKey:@"event_start_datetime"] isEqual: [NSNull null]] || ![[dict objectForKey:@"event_end_datetime"] isEqual:[NSNull null]]){
		
		if([[dict objectForKey:@"event_type"] isEqualToString:@"event"]){
			
			//If event thn display date
			
			if([CommonMethod isBetweenDate:[[dict objectForKey:@"event_start_datetime"] doubleValue] andDate:[[dict objectForKey:@"event_end_datetime"] doubleValue]]){
				
				[cell.lblStatus setBackgroundColor:[UIColor colorWithRed:(0.0/255.0) green:(145.0/255.0) blue:(71.0/255.0) alpha:1.0]];
				[cell.lblStatus setText:@"Open Now"];
			}else{
				[cell.lblStatus setText:[CommonMethod displayOpenTime:[[dict objectForKey:@"event_start_datetime"] doubleValue]]];
				[cell.lblStatus setBackgroundColor:[UIColor colorWithRed:(249.0/255.0) green:(120.0/255.0) blue:(6.0/255.0) alpha:1.0]];
			}
			
		}else{
			
			BOOL is24Hours;
			
			NSMutableDictionary *dictHours = [[dict objectForKey:@"operating_hours"] mutableCopy];
			
			NSString *dayName = [CommonMethod getCurrentWeekDay];
			
			if([CommonMethod isTimeZoneIn24Hours]){
				is24Hours = true;
			}else{
				is24Hours = false;
			}
			
			if(dictHours[dayName]){
				
				if([[[dictHours objectForKey:dayName] objectForKey:@"start"]isEqualToString:@""]){
					//[lblDateNTime setText:@""];
					[cell.lblStatus setHidden:YES];
					
				}else{
					
					if([[[dictHours objectForKey:dayName] objectForKey:@"start"] isEqualToString:@"Open 24 hours"]){
						
						//[lblDateNTime setText:[[dictHours objectForKey:dayName] objectForKey:@"start"]];
						
						[cell.lblStatus setText:@"Open Now"];
						[cell.lblStatus setBackgroundColor:[UIColor colorWithRed:(0.0/255.0) green:(145.0/255.0) blue:(71.0/255.0) alpha:1.0]];
						
					}else if([[[dictHours objectForKey:dayName] objectForKey:@"start"] isEqualToString:@"Closed"]){
						
						//If Start time is close thn display time and status as close.
						//[lblDateNTime setText:[[dictHours objectForKey:dayName] objectForKey:@"start"]];
						
						//[cell.lblStats setText:@"Closed Now"];
						//[cell.lblStats setBackgroundColor:[UIColor redColor]];
						
						
					}else{
						
						NSInteger startTime = 0,endTime = 0;
						NSString *startTimeAMPM,*endTimeAMPM;
						
						if([[[dictHours objectForKey:dayName] objectForKey:@"end"] rangeOfString:@","].location != NSNotFound){
							
							NSString *strendTime = [[[[dictHours objectForKey:dayName] objectForKey:@"end"]componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@","]]objectAtIndex:0];
							
							NSArray *ary = [strendTime componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
							
							endTime = [[ary objectAtIndex:0] integerValue];
							
							
							if (ary.count>1)
							{
								endTimeAMPM = [ary objectAtIndex:1];
								
							}else
							{
								endTimeAMPM =@"PM";
							}
							
							
							ary = [[[dictHours objectForKey:dayName] objectForKey:@"start"]componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
							
							startTime = [[ary objectAtIndex:0] integerValue];
							
							
							if (ary.count>1)
							{
								startTimeAMPM = [ary objectAtIndex:1];
								
							}else
							{
								startTimeAMPM =@"AM";
							}
							
							
						}else{
							
							NSArray *ary = [[[dictHours objectForKey:dayName] objectForKey:@"start"]componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
							
							startTime = [[ary objectAtIndex:0] integerValue];
							
							
							if (ary.count>1)
							{
								startTimeAMPM = [ary objectAtIndex:1];
							}else
							{
								startTimeAMPM =@"AM";
							}
							
							ary = [[[dictHours objectForKey:dayName] objectForKey:@"end"]componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
							
							endTime = [[ary objectAtIndex:0] integerValue];
							
							if (ary.count>1)
							{
								endTimeAMPM = [ary objectAtIndex:1];
								
							}else
							{
								endTimeAMPM =@"PM";
							}
							
							//[lblDateNTime setText:[NSString stringWithFormat:@"%@ - %@",[[dictHours objectForKey:dayName] objectForKey:@"start"],[[dictHours objectForKey:dayName] objectForKey:@"end"]]];
						}
						
						/*if(is24Hours){
						 
						 
						 
						 if([CommonMethod getCurrentHours] >= startTime && [CommonMethod getCurrentHours] <= endTime){
						 [lblStatus setText:@"Open Now"];
						 }else{
						 [lblStatus setText:@"Closed Now"];
						 [lblStatus setTextColor:[UIColor redColor]];
						 }
						 
						 }else{
							 
							 if([startTimeAMPM isEqualToString:@"pm"] || [startTimeAMPM isEqualToString:@"PM"]){
								 startTime += 12;
							 }
							 
							 if([endTimeAMPM isEqualToString:@"pm"] || [endTimeAMPM isEqualToString:@"PM"]){
								 endTime += 12;
							 }
							 
							 if([CommonMethod getCurrentHours] >= startTime && [CommonMethod getCurrentHours] <= endTime){
								 [cell.lblStatus setText:@"Open Now"];
								 [cell.lblStatus setBackgroundColor:[UIColor colorWithRed:(0.0/255.0) green:(145.0/255.0) blue:(71.0/255.0) alpha:1.0]];
							 }else{
								 //[cell.lblStats setText:@"Closed Now"];
								 //[cell.lblStats setBackgroundColor:[UIColor redColor]];
								 [cell.lblStatus setText:[NSString stringWithFormat:@"Opens at %ld",(long)startTime]];
								 [cell.lblStatus setBackgroundColor:[UIColor colorWithRed:(249.0/255.0) green:(120.0/255.0) blue:(6.0/255.0) alpha:1.0]];
							 }
						 }
					}
				}
			}else{
				
				//[lblDateNTime setText:@""];
				[cell.lblStatus setHidden:true];
			}
		}
	}
	
}


*/
-(void)getLocationForMap{
	
	[CommonMethod showLoadingIndicator];
	
	CLLocationCoordinate2D coord;
	
	CLLocationCoordinate2D coordCurnt = CLLocationCoordinate2DMake(AppDel.crntLat, AppDel.crntLon);
	
	MKCoordinateSpan span = MKCoordinateSpanMake(20, 20);
	MKCoordinateRegion regionnew = {coordCurnt, span};
	
	for (int i=0; i<[arrAtlBuzz count]; i++)
	{
		
		if(![[[arrAtlBuzz objectAtIndex:i] objectForKey:@"event_latitude"] isEqual:[NSNull null]] && ![[[arrAtlBuzz objectAtIndex:i] objectForKey:@"event_longitude"] isEqual:[NSNull null]]){
			
			coord.latitude = [[[arrAtlBuzz objectAtIndex:i] objectForKey:@"event_latitude"] floatValue];
			coord.longitude = [[[arrAtlBuzz objectAtIndex:i] objectForKey:@"event_longitude"] floatValue];
			
			
			MyAnnotation *annotObj3 =[[MyAnnotation alloc]initWithCoordinate:coord title:[[arrAtlBuzz objectAtIndex:i] objectForKey:@"event_name"] isHot:[[[arrAtlBuzz objectAtIndex:i] objectForKey:@"event_is_hot"] boolValue] tag:i lat:[[[arrAtlBuzz objectAtIndex:i] objectForKey:@"event_latitude"] floatValue] long:[[[arrAtlBuzz objectAtIndex:i] objectForKey:@"event_longitude"] floatValue] evtID:[[[arrAtlBuzz objectAtIndex:i] objectForKey:@"event_id"] stringValue]];
			
			
			[eventMap addAnnotation:annotObj3];
		}
	}
	[eventMap setRegion:regionnew animated:YES];
	[CommonMethod hideLoader];
	
}

-(void)callAtlBuzzWebService{
	
	if([CommonMethod isNetworkAvailable]){
		
		[CommonMethod showLoadingIndicator];
		
		NSString *lat = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:@"coordinates"] valueForKey:@"lat"]];
		NSString *longi = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:@"coordinates"] valueForKey:@"lon"]];
		
		[manager GET:[NSString stringWithFormat:@"%@%@",baseUrlWebService,featureEvent] parameters:[NSDictionary dictionaryWithObjectsAndKeys:lat,@"latitude",longi,@"longitude",[[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"],@"user_id", nil] success:^(AFHTTPRequestOperation *operation, id responseObject)
		 {
			 NSError *error;
			 
			 NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
			 
			 [CommonMethod hideLoader];
			 
			 if([[parsedObject objectForKey:res_Code] integerValue] == 200){
				 
				 arrAtlBuzz = [CommonMethod sortAnArrayByHotevent:[[parsedObject objectForKey:@"featured_events"] mutableCopy]];
				 //NSLog(@"arrAtlBuzz COUNT %lu",(unsigned long)arrAtlBuzz.count);
				 
				 if(arrAtlBuzz.count > 0){
					 [clgAtlBuzz reloadData];
					 
					 [self getLocationForMap];
					 
				 }else{
					 DisplayAlert(@"No Data found");
				 }
				 
			 }else{
				 DisplayAlert([parsedObject objectForKey:res_Message]);
			 }
			 
		 }failure:^(AFHTTPRequestOperation *operation, NSError *error){
			 [CommonMethod hideLoader];
			 DisplayAlert(@"Error From Web Service");
			 
		 }];
		
	}else{
		DisplayAlert(NET_MSG);
	}
	
}

#pragma mark - Button Actions
- (IBAction)showMapBtnPressed:(id)sender {
	
	if([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
	{
		//NSLog(@"Dont allow");
		[self setAtlantaLocation];
	}else
	{
		//eventMap.userTrackingMode=YES;
		//eventMap.userTrackingMode=NO;
		//[eventMap reloadInputViews];
		
	}
	eventMap.showsUserLocation = YES;
	
	if(btnShowMap.isSelected){
		
		[btnShowMap setSelected:NO];
		
		mapHeight.constant = 0;
		
		btnCurrentLocation.hidden = true;
		
	}else{
		
		[btnShowMap setSelected:YES];
		
		mapHeight.constant = 200;
		
		btnCurrentLocation.hidden = false;
	}
	
	[clgAtlBuzz reloadData];
}
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
	if ( !_initialLocation )
	{
		self.initialLocation = userLocation.location;
		
		MKCoordinateRegion region;
		region.center = mapView.userLocation.coordinate;
		region.span = MKCoordinateSpanMake(20, 20);
		
		region = [mapView regionThatFits:region];
		[mapView setRegion:region animated:YES];
	}
}

- (IBAction)crntLocationBtnPressed:(id)sender {
	
	[self setAtlantaLocation];
	
}
-(void)setAtlantaLocation
{
	CLLocationCoordinate2D coord;
	coord.latitude = 33.749065;
	coord.longitude = -84.3888742;
	MKCoordinateRegion region1;
	region1.center=coord;
	region1.span.longitudeDelta=6;
	region1.span.latitudeDelta=6;
	[eventMap setRegion:region1 animated:YES];
	
}

-(IBAction)btnImagePressed:(id)sender{
	UIButton *btn = (UIButton *)sender;
	
	//[eventMap removeOverlays:[eventMap overlays]];
	
	//PARESHHIRPARA
	[self removePin];
	EventDetailVC *eventVC = [self.storyboard instantiateViewControllerWithIdentifier:@"EventDetailVCID"];
	eventVC.strEventId = [[[arrAtlBuzz objectAtIndex:btn.tag] objectForKey:@"event_id"] stringValue];
	[AppDel.slideMenuVC.mainViewController.navigationController pushViewController:eventVC animated:YES];
	
}

#pragma mark - cell Button Pressed
-(IBAction)cellFavBtnPressed:(id)sender{
	UIButton *btn = (UIButton *)sender;
	
	NSString *str =[[NSUserDefaults standardUserDefaults]objectForKey:@"user_id"];
	
	if(str.length == 0){
		DisplayAlert(@"You are not logged in");
	}else{
		
		if([CommonMethod isNetworkAvailable]){
			
			[CommonMethod showLoadingIndicator];
			
			[manager POST:[NSString stringWithFormat:@"%@events/%ld/favorite_unfavorite",baseUrlWebService,(long)btn.tag] parameters:[NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"],@"user_id", nil]
				  success:^(AFHTTPRequestOperation *operation, id responseObject){
					  
					  NSError *error;
					  NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
					  
					  [CommonMethod hideLoader];
					  
					  if([[parsedObject objectForKey:res_Code] integerValue] == 200){
						  [self callAtlBuzzWebService];
					  }else{
						  DisplayAlert(@"Some error in webservice");
					  }
					  
				  }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
					  [CommonMethod hideLoader];
					  DisplayAlert(error.localizedFailureReason);
				  }];
			
		}else{
			DisplayAlert(NET_MSG);
		}
		
	}
}

#pragma mark :- Memory Warning
- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
