//
//  BuzzViewController.m
//  BuzzLynkRedesign
//
//  Created by sotsys036 on 02/03/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import "BuzzViewController.h"
#import "BuzzShowLessCell1.h"
#import "EventListingVC.h"
#import "EventCustomCell.h"

@interface BuzzViewController ()
{
    NSArray *arryColor;
    NSInteger indx;
}

@end

@implementation BuzzViewController

#pragma mark - Viewcontroller Method
- (void)viewDidLoad {
    [super viewDidLoad];
    
    arryColor = [NSArray arrayWithObjects:[UIColor colorWithRed:(232.0/255.0) green:(0.0/255.0) blue:(31.0/255.0) alpha:1.0],[UIColor colorWithRed:(210.0/255.0) green:(165.0/255.0) blue:(51.0/255.0) alpha:1.0],[UIColor colorWithRed:(0.0/255.0) green:(187.0/255.0) blue:(204.0/255.0) alpha:1.0],[UIColor colorWithRed:(0.0/255.0) green:(181.0/255.0) blue:(51.0/255.0) alpha:1.0],[UIColor colorWithRed:(202.0/255.0) green:(0.0/255.0) blue:(161.0/255.0) alpha:1.0],[UIColor colorWithRed:(255.0/255.0) green:(112.0/255.0) blue:(41.0/255.0) alpha:1.0], nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(dayNightFilterWebService:)
                                                 name:@"dayFilteration"
                                               object:nil];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    arryBuzzLess = [[NSMutableArray alloc]init];
    
    manager  = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [self callCategoryWebService];
}

#pragma mark - Notification Center
-(void)dayNightFilterWebService:(NSNotification *)notification{
    NSDictionary *dict = notification.userInfo;
    
    [self callCategoryWSwithFilter:dict];
}

#pragma mark - UICollectionViewDelegate and Datasource
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{

    return arryBuzzLess.count;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
        static NSString *gridCell = @"GridCell1";
            
        BuzzShowLessCell1 *cell = (BuzzShowLessCell1 *)[collectionView dequeueReusableCellWithReuseIdentifier:gridCell forIndexPath:indexPath];

        indx = indexPath.row % arryColor.count;
        [cell setBuzzCellDetail:[arryBuzzLess objectAtIndex:indexPath.row]counterColor:[arryColor objectAtIndex:indx]];
    
        cell.layer.cornerRadius = 5.0;
    
        return cell;
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    EventListingVC *eventVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"EventListingVC"];
    
    eventVC.categoryId = [[[arryBuzzLess objectAtIndex:indexPath.row] objectForKey:category_id] intValue];
    eventVC.strCategoryName = [NSString stringWithFormat:@"%@",[[arryBuzzLess objectAtIndex:indexPath.row] objectForKey:category_name]];
    eventVC.isFromCategory = true;
    
    [AppDel.slideMenuVC.mainViewController.navigationController pushViewController:eventVC animated:YES];
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(((self.view.frame.size.width-10-10-5-10)/2) ,[CommonMethod cellHeight]);
}

#pragma mark - Private Method
-(void)callCategoryWSwithFilter:(NSDictionary *)dict{
    
    if([CommonMethod isNetworkAvailable]){
        
        [CommonMethod showLoadingIndicator];

		
		NSString *lat = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:@"coordinates"] valueForKey:@"lat"]];
		NSString *longi = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:@"coordinates"] valueForKey:@"lon"]];
		
		NSDictionary *dictParam = [NSDictionary dictionaryWithObjectsAndKeys:[dict objectForKey:@"week_day"],@"week_day",[dict objectForKey:@"day_night"],@"day_night",lat,@"latitude",longi,@"longitude",nil];
        
        [manager GET:[NSString stringWithFormat:@"%@%@",baseUrlWebService,buzzCategory] parameters:dictParam success:^(AFHTTPRequestOperation *operation, id responseObject){
            NSError *error;
            NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
            
            [CommonMethod hideLoader];
            
            if([[parsedObject objectForKey:res_Code] integerValue] == 200){
                
                [self filterArray:[parsedObject objectForKey:res_Categories]];
                
            }else{
                
                DisplayAlert([parsedObject objectForKey:res_Message]);
            }
            
        }failure:^(AFHTTPRequestOperation *operation, NSError *error){
	
            [CommonMethod hideLoader];
            DisplayAlert(@"Error from Web service");
        }];
        
    }else{
        DisplayAlert(NET_MSG);
    }
    
}

-(void)callCategoryWebService{
    
    if([CommonMethod isNetworkAvailable]){
        
        [CommonMethod showLoadingIndicator];
        
        [manager GET:[NSString stringWithFormat:@"%@%@",baseUrlWebService,buzzCategory] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject){
            NSError *error;
            NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
            
            [CommonMethod hideLoader];
            
            if([[parsedObject objectForKey:res_Code] integerValue] == 200)
			{
                
                [self filterArray:[parsedObject objectForKey:res_Categories]];
                
            }else{
                
                DisplayAlert([parsedObject objectForKey:res_Message]);
            }
            
        }failure:^(AFHTTPRequestOperation *operation, NSError *error){

            [CommonMethod hideLoader];
            DisplayAlert(error.description);
        }];
        
    }else{
        DisplayAlert(NET_MSG);
    }
    
}

-(void)filterArray:(NSArray *)arrFil{
    
    if(arryBuzzLess.count > 0){
        [arryBuzzLess removeAllObjects];
    }
    
    for (NSDictionary *dict in arrFil) {
        
        if([[dict objectForKey:@"display_front"] boolValue]){
            [arryBuzzLess addObject:dict];
        }
    }
    
    if(arryBuzzLess.count > 0){
        [buzzclgVw reloadData];
    }else{
        DisplayAlert(@"No data found");
    }
}

#pragma mark - Receive memory warning
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
