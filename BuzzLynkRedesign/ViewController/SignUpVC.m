//
//  SignUpVC.m
//  BuzzLynkRedesign
//
//  Created by sotsys036 on 10/03/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import "SignUpVC.h"

@interface SignUpVC ()

@end

@implementation SignUpVC

#pragma mark - ViewController Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    
    manager  = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [txtEmail.layer setBorderWidth:1.0];
    [txtEmail.layer setBorderColor:[UIColor whiteColor].CGColor];
    
    [txtName.layer setBorderColor:[UIColor whiteColor].CGColor];
    [txtName.layer setBorderWidth:1.0];
    
    [txtUsername.layer setBorderColor:[UIColor whiteColor].CGColor];
    [txtUsername.layer setBorderWidth:1.0];
    
    [txtPassword.layer setBorderColor:[UIColor whiteColor].CGColor];
    [txtPassword.layer setBorderWidth:1.0];

    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(addPhotoBtnPressed:)];
    tapGesture.numberOfTouchesRequired = 1;
    tapGesture.numberOfTapsRequired = 1;
    [imgProfile addGestureRecognizer:tapGesture];
    
    [btnSignUp.layer setCornerRadius:5.0];
    btnSignUp.clipsToBounds = true;
    
    [txtName becomeFirstResponder];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:true];
}

#pragma mark - Private methods
-(BOOL)isTextFieldValidation{
    
    BOOL isValidate;
    
    NSString *strMsg = @"";
    if(txtName.text.length == 0){
        strMsg = @"Please enter name";
        [txtName becomeFirstResponder];
    }else if(![CommonMethod validateEmailWithString:txtEmail.text]){
        strMsg = @"Please enter valid email address";
        [txtEmail becomeFirstResponder];
    }else if(txtUsername.text.length == 0){
        strMsg = @"Please enter username";
        [txtUsername becomeFirstResponder];
    }else if(txtPassword.text.length < 6 ){
        strMsg = @"Password should be minimum 6 characters.";
        [txtPassword becomeFirstResponder];
    }
    
    if([strMsg isEqualToString:@""]){
        isValidate = true;
    }else{
        DisplayAlert(strMsg);
        isValidate = false;
    }
    
    return isValidate;
}

-(void)imagePickerSetting:(BOOL)isCamera{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = NO;
    
    if (isCamera) {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }else{
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
     
    [self presentViewController:picker animated:YES completion:NULL];
    
}

#pragma mark - UITextfield Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if([textField isEqual:txtName]){
        [txtEmail becomeFirstResponder];
    }else if([textField isEqual:txtEmail]){
        [txtUsername becomeFirstResponder];
    }else if([textField isEqual:txtUsername]){
        [txtPassword becomeFirstResponder];
    }else{
        [textField resignFirstResponder];
        [self signUpBtnPressed:nil];
    }
    return YES;
}

#pragma mark - Action Sheet delegate
- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    switch (buttonIndex) {
        case 0:
            [self imagePickerSetting:true];
            break;
            
        case 1:
            [self imagePickerSetting:false];
            break;
            
        default:
            break;
    }
    
}


#pragma mark - Button Actions
- (IBAction)addPhotoBtnPressed:(id)sender {
    
    
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Select Photo:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Capture from Camera",
                            @"Select from library",
                            nil];
    popup.tag = 1;
    [popup showInView:self.view];
    
}


- (IBAction)signUpBtnPressed:(id)sender {
    
    if([CommonMethod isNetworkAvailable]){
        
        if([self isTextFieldValidation]){
            
            [CommonMethod showLoadingIndicator];
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            [dict setObject:txtName.text forKey:reg_firstName];
            [dict setObject:txtUsername.text forKey:reg_lastName];
            [dict setObject:txtEmail.text forKey:reg_email];
            [dict setObject:txtPassword.text forKey:reg_password];
            
            if(![imgProfile.image isEqual:[UIImage imageNamed:@"profile"]]){
                
                //[dict setObject:photoDict forKey:reg_imgattribute];
                
                [manager POST:[NSString stringWithFormat:@"%@%@",baseUrlWebService,user_Registration] parameters:[NSDictionary dictionaryWithObjectsAndKeys:dict,@"user",nil] constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData)
                 {
                     [formData appendPartWithFileData:UIImageJPEGRepresentation(imgProfile.image, 0.8) name:@"photo" fileName:[NSString stringWithFormat:@"profile_picture.png"] mimeType:@"image/png"];
                     
                 } success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
                     
                     NSError *error;
                     NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
                     
                     [CommonMethod hideLoader];
                     
                     if([[parsedObject objectForKey:res_Code] integerValue] == 201){
                         
                         [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%@",[[[parsedObject objectForKey:@"user"]objectForKey:@"user_id"] stringValue]] forKey:@"user_id"];
                         
                         [[NSUserDefaults standardUserDefaults]synchronize];
                         
                         [[NSNotificationCenter defaultCenter]postNotificationName:@"changeStatus"object:self
                                                                          userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"Yes",@"SignInStatus", nil]];
                         
                         
                         [[NSNotificationCenter defaultCenter]postNotificationName:@"FromSign"object:self
                                                                          userInfo: nil];
                         
                        
                         
                     }else{
                         
                         DisplayAlert([parsedObject objectForKey:res_Message])
                     }
                     
                     
                 } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
                     [CommonMethod hideLoader];
                     DisplayAlert(@"Error from web service");
                 }];
                
            }else{
                
                [manager POST:[NSString stringWithFormat:@"%@%@",baseUrlWebService,user_Registration] parameters:[NSDictionary dictionaryWithObjectsAndKeys:dict,@"user", nil] success:^(AFHTTPRequestOperation *operation, id responseObject){
                    NSError *error;
                    NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
                    
                    [CommonMethod hideLoader];
                    
                    if([[parsedObject objectForKey:res_Code] integerValue] == 201){
                        
                        [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%@",[[[parsedObject objectForKey:@"user"]objectForKey:@"user_id"] stringValue]] forKey:@"user_id"];
                        
                        [[NSUserDefaults standardUserDefaults]synchronize];
                        
                        [[NSNotificationCenter defaultCenter]postNotificationName:@"changeStatus"object:self
                                                                         userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"Yes",@"SignInStatus", nil]];
                        
                        
                        [[NSNotificationCenter defaultCenter]postNotificationName:@"FromSign"object:self
                                                                         userInfo: nil];
                        
                    }else{
                        
                        DisplayAlert([parsedObject objectForKey:res_Message])
                    }
                    
                }failure:^(AFHTTPRequestOperation *operation, NSError *error){
                    [CommonMethod hideLoader];
                    DisplayAlert(@"Error From Web Service");
                }];
            }
            
        }
    }else{
        DisplayAlert(NET_MSG);
    }
    
    
}

- (IBAction)backBtnPressed:(id)sender {
    
    [AppDel.slideMenuVC.navigationController popViewControllerAnimated:YES];
}



#pragma mark - Image Picker delegate
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    imgProfile.image  = info[UIImagePickerControllerOriginalImage];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    
    imgProfile.image  = [UIImage imageNamed:@"profile"];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Memory warning
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
