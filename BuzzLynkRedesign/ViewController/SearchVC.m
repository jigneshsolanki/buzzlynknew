
#import "SearchVC.h"
#import "SearchCustomCell.h"
#import "EventCustomCell.h"
#import "EventDetailVC.h"

@interface SearchVC ()

@end

@implementation SearchVC

@synthesize strTitle;

#pragma mark - Viewcontroller Methods
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [vwSearch.layer setCornerRadius:5.0];
	//[vwSearch.layer setBackgroundColor:[UIColor grayColor].CGColor];
     vwSearch.layer.borderWidth = 1.0;
	vwSearch.layer.borderColor = [UIColor grayColor].CGColor;
	
    
    arrySearch = [NSMutableArray array];
    arryTopSearch = [NSMutableArray array];
    
    afmanager  = [AFHTTPRequestOperationManager manager];
    afmanager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [lblSearchTitle setText:[NSString stringWithFormat:@"Search %@",strTitle]];
    
    [self getHistoryOfSearch];
}

#pragma mark - Private Methods


-(void)getHistoryOfSearch{
    
    if([CommonMethod isNetworkAvailable]){
        
        [CommonMethod showLoadingIndicator];
        
        [afmanager GET:[NSString stringWithFormat:@"%@%@",baseUrlWebService,searchHistory] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject){
            
            NSError *error;
            NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
            
            [CommonMethod hideLoader];
            
            if([[parsedObject objectForKey:res_Code] integerValue] == 200){
                
			arryTopSearch = [[parsedObject objectForKey:@"top_events"] mutableCopy];
			
                if (arryTopSearch.count == 0) {
                    DisplayAlert(@"No data found")
                }
                
                [clgTopSearch reloadData];
                
            }else{
                DisplayAlert([parsedObject objectForKey:res_Message]);
            }
            
        }failure:^(AFHTTPRequestOperation *operation, NSError *error){
            [CommonMethod hideLoader];
            DisplayAlert(@"Error From Web Service");
        }];
        
    }else{
        DisplayAlert(NET_MSG);
    }
    
}

-(void)callSearchWebService:(NSString *)strText{
    if([CommonMethod isNetworkAvailable]){
        
        [CommonMethod showLoadingIndicator];
        
        NSString *str =[[NSUserDefaults standardUserDefaults]objectForKey:@"user_id"] ;
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        
        if(str.length == 0){
            [dict setObject:@"0" forKey:@"user_id"];
        }else{
            [dict setObject:str forKey:@"user_id"];
        }
        
        [dict setObject:strText forKey:@"query"];
        
        [afmanager GET:[NSString stringWithFormat:@"%@%@",baseUrlWebService,searchWS] parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject){
            
            NSError *error;
            NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
            
            [CommonMethod hideLoader];
            
            if([[parsedObject objectForKey:res_Code] integerValue] == 200){
                
                arrySearch = [[parsedObject objectForKey:event_events] mutableCopy];
                
                if (arrySearch.count == 0) {
                    DisplayAlert(@"No data found")
                }
                
                [clgSearchResult reloadData];
                
            }else{
                DisplayAlert([parsedObject objectForKey:res_Message]);
            }
            
        }failure:^(AFHTTPRequestOperation *operation, NSError *error){
            [CommonMethod hideLoader];
            DisplayAlert(@"Error From Web Service");
            
        }];
        
    }else{
        DisplayAlert(NET_MSG);
    }
}

#pragma  mark - TextField Delagate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [self.view endEditing:true];
        
    recentSearchHeightCons.constant = 0;
        
    lblRecentPopularSearch.text = @"";
        
    [self callSearchWebService:textField.text];
    
    return YES;
    
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    
    [self.view endEditing:true];
    
    recentSearchHeightCons.constant = 100;
    
    lblRecentPopularSearch.text = @"Recent Popular Searches";
    
    return true;
    
}

/*

#pragma mark - Private Methods
-(void)setStatusOfEventVenue:(EventCustomCell *)cell dictEvent:(NSDictionary *)dict{
    
    if(![[dict objectForKey:@"event_start_datetime"] isEqual: [NSNull null]] || ![[dict objectForKey:@"event_end_datetime"] isEqual:[NSNull null]]){
        
        if([[dict objectForKey:@"event_type"] isEqualToString:@"event"]){
            
            //If event thn display date
            
            //[lblDateNTime setText:[CommonMethod displayDateInformate:[[dict objectForKey:@"event_start_datetime"] doubleValue] endDate:[[dict objectForKey:@"event_end_datetime"] doubleValue]]];
            
            if([CommonMethod isBetweenDate:[[dict objectForKey:@"event_start_datetime"] doubleValue] andDate:[[dict objectForKey:@"event_end_datetime"] doubleValue]]){
                
                [cell.lblStats setBackgroundColor:[UIColor colorWithRed:(0.0/255.0) green:(145.0/255.0) blue:(71.0/255.0) alpha:1.0]];
                [cell.lblStats setText:@"Open Now"];
            }else{
                [cell.lblStats setText:[CommonMethod displayOpenTime:[[dict objectForKey:@"event_start_datetime"] doubleValue]]];
                [cell.lblStats setBackgroundColor:[UIColor colorWithRed:(249.0/255.0) green:(120.0/255.0) blue:(6.0/255.0) alpha:1.0]];
            }
            
        }else{
            
            BOOL is24Hours;
            
            NSMutableDictionary *dictHours = [[dict objectForKey:@"operating_hours"] mutableCopy];
            
            NSString *dayName = [CommonMethod getCurrentWeekDay];
            
            if([CommonMethod isTimeZoneIn24Hours]){
                is24Hours = true;
            }else{
                is24Hours = false;
            }
            
            if(dictHours[dayName]){
                
                if([[[dictHours objectForKey:dayName] objectForKey:@"start"]isEqualToString:@""]){
                    //[lblDateNTime setText:@""];
                    [cell.lblStats setHidden:YES];
                    
                }else{
                    
                    if([[[dictHours objectForKey:dayName] objectForKey:@"start"] isEqualToString:@"Open 24 hours"]){
                        
                        //[lblDateNTime setText:[[dictHours objectForKey:dayName] objectForKey:@"start"]];
                        
                        [cell.lblStats setText:@"Open Now"];
                        [cell.lblStats setBackgroundColor:[UIColor colorWithRed:(0.0/255.0) green:(145.0/255.0) blue:(71.0/255.0) alpha:1.0]];
                        
                    }else if([[[dictHours objectForKey:dayName] objectForKey:@"start"] isEqualToString:@"Closed"]){
                        
                        //If Start time is close thn display time and status as close.
                        //[lblDateNTime setText:[[dictHours objectForKey:dayName] objectForKey:@"start"]];
                        
                        //[cell.lblStats setText:@"Closed Now"];
                        //[cell.lblStats setBackgroundColor:[UIColor redColor]];
                        
                        
                    }else{
                        
                        NSInteger startTime = 0,endTime = 0;
                        NSString *startTimeAMPM,*endTimeAMPM;
                        
                        if([[[dictHours objectForKey:dayName] objectForKey:@"end"] rangeOfString:@","].location != NSNotFound){
                            
                            NSString *strendTime = [[[[dictHours objectForKey:dayName] objectForKey:@"end"]componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@","]]objectAtIndex:0];
                            
                            NSArray *ary = [strendTime componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
                            
                            endTime = [[ary objectAtIndex:0] integerValue];
                            
							
							
							if (ary.count>1)
							{
								   endTimeAMPM = [ary objectAtIndex:1];
								
							}else
							{
								   endTimeAMPM = @"AM";
							}
                            
                            ary = [[[dictHours objectForKey:dayName] objectForKey:@"start"]componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
                            
                            startTime = [[ary objectAtIndex:0] integerValue];
                            
							
							if (ary.count>1)
							{
								 startTimeAMPM = [ary objectAtIndex:1];
								
							}else
							{
								startTimeAMPM =@"PM";
							}
                            
                            
                        }else{
                            
                            NSArray *ary = [[[dictHours objectForKey:dayName] objectForKey:@"start"]componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
                            
                            startTime = [[ary objectAtIndex:0] integerValue];
                            
							
							
							if (ary.count>1)
							{
								startTimeAMPM = [ary objectAtIndex:1];
								
							}else
							{
								startTimeAMPM =@"PM";
							}
                            
                            ary = [[[dictHours objectForKey:dayName] objectForKey:@"end"]componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
                            
                            endTime = [[ary objectAtIndex:0] integerValue];
                            
							
							if (ary.count>1)
							{
								endTimeAMPM = [ary objectAtIndex:1];
								
							}else
							{
								endTimeAMPM =@"PM";
							}
                            
                            //[lblDateNTime setText:[NSString stringWithFormat:@"%@ - %@",[[dictHours objectForKey:dayName] objectForKey:@"start"],[[dictHours objectForKey:dayName] objectForKey:@"end"]]];
                        }
                        
                        if(is24Hours){
                         
                         
                         
                         if([CommonMethod getCurrentHours] >= startTime && [CommonMethod getCurrentHours] <= endTime){
                         [lblStatus setText:@"Open Now"];
                         }else{
                         [lblStatus setText:@"Closed Now"];
                         [lblStatus setTextColor:[UIColor redColor]];
                         }
                         
                         }else{
                             
                             if([startTimeAMPM isEqualToString:@"pm"] || [startTimeAMPM isEqualToString:@"PM"]){
                                 startTime += 12;
                             }
                             
                             if([endTimeAMPM isEqualToString:@"pm"] || [endTimeAMPM isEqualToString:@"PM"]){
                                 endTime += 12;
                             }
                             
                             if([CommonMethod getCurrentHours] >= startTime && [CommonMethod getCurrentHours] <= endTime){
                                 [cell.lblStats setText:@"Open Now"];
                                 [cell.lblStats setBackgroundColor:[UIColor colorWithRed:(0.0/255.0) green:(145.0/255.0) blue:(71.0/255.0) alpha:1.0]];
                             }else{
                                 //[cell.lblStats setText:@"Closed Now"];
                                 //[cell.lblStats setBackgroundColor:[UIColor redColor]];
                                 [cell.lblStats setText:[NSString stringWithFormat:@"Opens at %ld",(long)startTime]];
                                 [cell.lblStats setBackgroundColor:[UIColor colorWithRed:(249.0/255.0) green:(120.0/255.0) blue:(6.0/255.0) alpha:1.0]];
                             }
                         }
                    }
                }
            }else{
                
                //[lblDateNTime setText:@""];
                [cell.lblStats setHidden:true];
            }
        }
    }
    
}
*/

-(void)setEventNameForVenue:(EventCustomCell *)cell dictEvent:(NSDictionary *)dict{
    NSMutableDictionary *dictDays = [[dict objectForKey:@"day_events"] mutableCopy];
    
    NSString *dayName = [CommonMethod getCurrentWeekDay];
    
    if(dictDays.count > 0){
        
        if(dictDays[dayName]){
            
            cell.lblNameVenue.text = dictDays[dayName][@"event_name"];
        }
        
    }else{
        cell.lblNameVenue.text = @"";
    }
}

#pragma mark - UICollectionView Delegate and datasource

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
	
	float spacing;
	if(IS_IPHONE_4 )
	{
		spacing=3.0;
		
	}else if(IS_IPHONE_5)
	{
		spacing=3.0;
	}else
	{
		spacing=5.0;
	}
	return spacing;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
	
	if([collectionView isEqual:clgTopSearch]){
		
		CGSize calCulateSizze =[(NSString*)[arryTopSearch objectAtIndex:indexPath.row] sizeWithAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"Helvetica Neue" size:18.0]}]; //+3 before
		calCulateSizze.width = calCulateSizze.width;
		calCulateSizze.height = calCulateSizze.height +5;
		
		return calCulateSizze;
		
		
	}else{
		
		return CGSizeMake((self.view.frame.size.width-8) ,200);
		
	}
	
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if ([collectionView isEqual:clgTopSearch]){
        return arryTopSearch.count;
    }else{
        return arrySearch.count;
    }
    
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if([collectionView isEqual:clgTopSearch]){
        
        SearchCustomCell *cell = (SearchCustomCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"SearchCell" forIndexPath:indexPath];
        
        cell.layer.borderWidth = 0.5;
		cell.layer.borderColor = [UIColor whiteColor].CGColor;
        [cell.layer setCornerRadius:5.0];
		[cell.lblSearch setFont:[UIFont fontWithName:@"Helvetica Neue" size:16.0]]; //+3 before
        [cell.lblSearch setText:[NSString stringWithFormat:@"%@",[arryTopSearch objectAtIndex:indexPath.row]]];
		
        return cell;
        
    }else{
        
        EventCustomCell *cell = (EventCustomCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"EventSearchCell" forIndexPath:indexPath];
        
        cell.layer.cornerRadius = 5.0f;
        cell.clipsToBounds = YES;
        
        [cell setListCellEventDetail:[arrySearch objectAtIndex:indexPath.row]];
        
        [cell.btnFav addTarget:self action:@selector(cellFavBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if ([collectionView isEqual:clgTopSearch]){
        
        [self.view endEditing:true];
        
        recentSearchHeightCons.constant = 0;
        
        lblRecentPopularSearch.text = @"";
        
        [self callSearchWebService:[NSString stringWithFormat:@"%@",[arryTopSearch objectAtIndex:indexPath.row]]];
        
    }else{
        
        EventDetailVC *eventVC = [self.storyboard instantiateViewControllerWithIdentifier:@"EventDetailVCID"];
        eventVC.strEventId = [[[arrySearch objectAtIndex:indexPath.row] objectForKey:@"event_id"] stringValue];
        [AppDel.slideMenuVC.mainViewController.navigationController pushViewController:eventVC animated:YES];
        
    }
}



#pragma mark - cell Button Pressed
-(IBAction)cellFavBtnPressed:(id)sender{
    UIButton *btn = (UIButton *)sender;
    
    NSString *str =[[NSUserDefaults standardUserDefaults]objectForKey:@"user_id"];
    
    if(str.length == 0){
        DisplayAlert(@"You are not logged in");
    }else{
        
        if([CommonMethod isNetworkAvailable]){
            
            [CommonMethod showLoadingIndicator];
            
            [afmanager POST:[NSString stringWithFormat:@"%@events/%ld/favorite_unfavorite",baseUrlWebService,(long)btn.tag] parameters:[NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"],@"user_id", nil]
                  success:^(AFHTTPRequestOperation *operation, id responseObject){
                      
                      NSError *error;
                      NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
                      
                      [CommonMethod hideLoader];
                      
                      if([[parsedObject objectForKey:res_Code] integerValue] == 200){
                          [self callSearchWebService:txtSearch.text];
                      }else{
                          DisplayAlert(@"Some error in webservice");
                      }
                      
                  }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                      [CommonMethod hideLoader];
                      DisplayAlert(error.localizedFailureReason);
                  }];
            
        }else{
            DisplayAlert(NET_MSG);
        }
        
    }
}

#pragma mark - Memory Warning.
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Actions
- (IBAction)closeButtonPressed:(id)sender {
    [AppDel.slideMenuVC.navigationController popViewControllerAnimated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
