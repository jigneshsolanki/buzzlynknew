//
//  SignInVC.m
//  BuzzLynkRedesign
//
//  Created by sotsys036 on 04/03/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import "SignInVC.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "SignUpVC.h"

@interface SignInVC ()

@end

@implementation SignInVC

#pragma mark - ViewController Methods
- (void)viewDidLoad {
	[super viewDidLoad];
	
	
	vwSignIn.layer.cornerRadius = 5.0;
	vwEmail.layer.cornerRadius = 5.0;
	vwPassword.layer.cornerRadius = 5.0;
	btnLogin.layer.cornerRadius = 5.0;
	
	UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(fbLogin)];
	tapGesture.numberOfTapsRequired = 1;
	[tapGesture setNumberOfTouchesRequired:1];
	
	[imgFbLogin addGestureRecognizer:tapGesture];
	
	[FBSDKAccessToken setCurrentAccessToken:nil];
	
	manager  = [AFHTTPRequestOperationManager manager];
	manager.responseSerializer = [AFHTTPResponseSerializer serializer];
	
}

-(void)viewWillDisappear:(BOOL)animated{
	[super viewWillDisappear:animated];
	
	[self.view endEditing:true];
}

#pragma mark - Private Method
-(BOOL)isTextFieldValidation{
	
	BOOL isValidate;
	
	NSString *strMsg = @"";
	if(![CommonMethod validateEmailWithString:txtEmail.text]){
		strMsg = @"Please enter valid email address";
		[txtEmail becomeFirstResponder];
	}else if(txtPassword.text.length == 0){
		strMsg = @"Please enter password";
		[txtPassword becomeFirstResponder];
	}
	
	if([strMsg isEqualToString:@""]){
		isValidate = true;
	}else{
		DisplayAlert(strMsg);
		isValidate = false;
	}
	
	
	return isValidate;
}

-(void)callFrgtPwdWebService:(NSString *)str{
	if([CommonMethod isNetworkAvailable]){
		
		[CommonMethod showLoadingIndicator];
		
		[manager POST:[NSString stringWithFormat:@"%@%@",baseUrlWebService,forgotPwd] parameters:[NSDictionary dictionaryWithObjectsAndKeys:str,@"email", nil] success:^(AFHTTPRequestOperation *operation, id responseObject){
			NSError *error;
			NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
			
			if([[parsedObject objectForKey:res_Code] integerValue] == 200){
				
				DisplayAlert([parsedObject objectForKey:@"message"]);
				
			}else{
				
				DisplayAlert([parsedObject objectForKey:res_Message])
			}
			[CommonMethod hideLoader];
			
		}failure:^(AFHTTPRequestOperation *operation, NSError *error){
			
			DisplayAlert(@"Error From Web Service");
			[CommonMethod hideLoader];
		}];
	}else{
		DisplayAlert(NET_MSG);
	}
}

-(void)getUserProfileDetailofFacebook{
	
	[CommonMethod showLoadingIndicator];
	
	[[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{ @"fields" : @"id,name,picture.width(500).height(500),email"}]startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
		if (!error) {
			
			NSData *data = [[NSData alloc]initWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[[result valueForKey:@"picture"] valueForKey:@"data"] valueForKey:@"url"]]]];
			
			NSMutableDictionary *fbDict = [NSMutableDictionary dictionary];
			
			[fbDict setObject:[result valueForKey:@"email"] forKey:@"email"];
			[fbDict setObject:[result valueForKey:@"name"] forKey:@"firstname"];
			[fbDict setObject:@"-" forKey:@"lastname"];
			[fbDict setObject:[result valueForKey:@"id"] forKey:@"facebook_id"];
			
			[manager POST:[NSString stringWithFormat:@"%@%@",baseUrlWebService,user_login] parameters:[NSDictionary dictionaryWithObjectsAndKeys:fbDict,@"user",nil] constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData)
			 {
				 [formData appendPartWithFileData:data name:@"photo" fileName:[NSString stringWithFormat:@"profile_picture.png"] mimeType:@"image/png"];
				 
			 } success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
				 
				 NSError *error;
				 NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
				 
				 if([[parsedObject objectForKey:res_Code] integerValue] == 200){
					 
					 [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%@",[[parsedObject objectForKey:@"user"] objectForKey:@"user_id"]] forKey:@"user_id"];
					 
					 [[NSUserDefaults standardUserDefaults]synchronize];
					 
					 [[NSNotificationCenter defaultCenter]postNotificationName:@"changeStatus"object:self
																	  userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"Yes",@"SignInStatus", nil]];
					 
					 
					 
					 ViewController *mainVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
					 
					 [AppDel.slideMenuVC setMainViewController:mainVC];
					 
				 }else{
					 DisplayAlert([parsedObject objectForKey:res_Message])
				 }
				 [CommonMethod hideLoader];
			 } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
				 DisplayAlert(@"Facebook login failed");
				 [CommonMethod hideLoader];
			 }];
			
		}else{
			DisplayAlert(@"Error while getting data from facebook");
			[CommonMethod hideLoader];
		}
	}];
}



#pragma mark - Button Actions

- (IBAction)closeBtnPressed:(id)sender {
	
	ViewController *mainVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
	[AppDel.slideMenuVC setMainViewController:mainVC];
	
}

- (IBAction)btnLoginPressed:(id)sender {
	
	if([self isTextFieldValidation]){
		
		NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:txtEmail.text,@"email",txtPassword.text,@"password", nil];
		
		[manager POST:[NSString stringWithFormat:@"%@%@",baseUrlWebService,user_login] parameters:[NSDictionary dictionaryWithObjectsAndKeys:dict,@"user", nil] success:^(AFHTTPRequestOperation *operation, id responseObject){
			NSError *error;
			NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
			
			if([[parsedObject objectForKey:res_Code] integerValue] == 200){
				
				
				[[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%@",[[parsedObject objectForKey:@"user"]objectForKey:@"user_id"]] forKey:@"user_id"];
				
				
				[[NSUserDefaults standardUserDefaults]synchronize];
				
				
				[[NSNotificationCenter defaultCenter]postNotificationName:@"changeStatus"object:self
																 userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"Yes",@"SignInStatus", nil]];
				
				
				ViewController *mainVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
				//mainVC.isFromSignUp = true;
				[AppDel.slideMenuVC setMainViewController:mainVC];
				
			}else{
				DisplayAlert([parsedObject objectForKey:res_Message])
			}
			
		}failure:^(AFHTTPRequestOperation *operation, NSError *error){
			DisplayAlert(@"Error From Web Service");
		}];
		
	}
	
}

-(void)fbLogin{
	
	if([CommonMethod isNetworkAvailable]){
		FBSDKLoginManager *fbLogin = [[FBSDKLoginManager alloc]init];
		
		
		[fbLogin logInWithReadPermissions:@[@"email",@"public_profile"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
			if(error){
				DisplayAlert(@"Facebook Login failed");
			}else if(result.isCancelled){
				DisplayAlert(@"Facebook Login cancel");
			}else{
				
				[self getUserProfileDetailofFacebook];
			}
		}];
		
	}else{
		DisplayAlert(NET_MSG);
	}
	
}

- (IBAction)btnFrgtPwdPressed:(id)sender {
	
	[self.view endEditing:true];
	
	UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:APP_NAME message:@"Please enter email id" delegate:self cancelButtonTitle:@"Submit" otherButtonTitles:@"Cancel", nil];
	alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
	[[alertView textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeEmailAddress];
	[[alertView textFieldAtIndex:0] becomeFirstResponder];
	[alertView show];
	
}

- (IBAction)btnSignUpPressed:(id)sender {
	
	[self.view endEditing:true];
	
	SignUpVC *signUp = [self.storyboard instantiateViewControllerWithIdentifier:@"SignUpVC"];
	[AppDel.slideMenuVC.mainViewController.navigationController pushViewController:signUp animated:YES];
	
}

#pragma mark - UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
	if(buttonIndex == 0){
		if(![CommonMethod validateEmailWithString:[alertView textFieldAtIndex:0].text]){
			DisplayAlert(@"Please enter valid email address");
		}else{
			[self callFrgtPwdWebService:[alertView textFieldAtIndex:0].text];
		}
	}
}

#pragma mark - TextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
	if (theTextField == txtEmail) {
		[txtPassword resignFirstResponder];
	} else if (theTextField == txtPassword) {
		[self btnLoginPressed:nil];
	}
	return YES;
}

#pragma mark - TextField Delegate
- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
