//
//  BuzzShowLessCell1.m
//  BuzzLink5
//
//  Created by sotsys036 on 15/12/15.
//  Copyright © 2015 sotsys036. All rights reserved.
//

#import "BuzzShowLessCell1.h"
#import "Constants.h"

@implementation BuzzShowLessCell1

- (void)awakeFromNib {
    
    //lblCounter.backgroundColor = [CommonMethod getRandomColor];
    _lblCounter.layer.cornerRadius = 10;
    _lblCounter.layer.masksToBounds = YES;
    
    
    //[_lblCategoryName setFont:[_lblCategoryName.font fontWithSize:[CommonMethod cellTextFontSize]]];
    //[_lblCounter setFont:[_lblCounter.font fontWithSize:[CommonMethod cellCounterFontSize]]];
    
}

-(void)setBuzzCellDetail:(NSDictionary *)dict counterColor:(UIColor *)color{
    
    [_lblCounter setBackgroundColor:color];
    
    [_lblCounter setText:[NSString stringWithFormat:@"%@",[dict objectForKey:category_total_event]]];
    
    [_lblCategoryName setText:[dict objectForKey:category_name]];
    
    
    [_imgView1 sd_setImageWithURL:[NSURL URLWithString:[dict objectForKey:category_pic]] placeholderImage:[UIImage imageNamed:@"Placeholder"] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
            
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
    }];
    
    
}
@end
