//
//  EventCustomCell.h
//  BuzzLynkRedesign
//
//  Created by sotsys036 on 03/03/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RateView.h"
#import "Constants.h"

#import "FXBlurView.h"
#import <QuartzCore/QuartzCore.h>


@interface EventCustomCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblEventName;
@property (weak, nonatomic) IBOutlet UILabel *lblCategoryName;

@property (weak, nonatomic) IBOutlet UIImageView *imgHot;
@property (weak,nonatomic) IBOutlet UIImageView *imgEvent;

@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UIButton *btnFav;

@property (weak,nonatomic) IBOutlet UIView *vwStatus;

@property (weak, nonatomic) IBOutlet UILabel *lblStats;
@property (weak, nonatomic) IBOutlet UILabel *lblRating;
@property (weak, nonatomic) IBOutlet UILabel *lblName;

@property (weak,nonatomic) IBOutlet UILabel *lblNameVenue;

@property (weak, nonatomic) IBOutlet RateView *rateView;

@property (weak, nonatomic) IBOutlet UILabel *lblCity;

@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
@property (strong, nonatomic) IBOutlet FXBlurView *blureView;
@property(nonatomic)BOOL isFromCategory;
-(void)setListCellEventDetail:(NSDictionary *)dict;



@end
