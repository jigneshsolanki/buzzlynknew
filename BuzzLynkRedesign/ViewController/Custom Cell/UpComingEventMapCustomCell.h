//
//  UpComingEventMapCustomCell.h
//  BuzzLynkRedesign
//
//  Created by SOTSYS0156 on 5/13/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RateView.h"


@interface UpComingEventMapCustomCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIButton *btnimagepressed;

@property (weak, nonatomic) IBOutlet UILabel *lblEventName;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblCategoryNMiles;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;

@property(weak,nonatomic) IBOutlet UILabel *lblMiles;

@property (weak,nonatomic) IBOutlet UIView *vwStatus;

@property (weak, nonatomic) IBOutlet RateView *rateview;

@property (weak, nonatomic) IBOutlet UIView *selectedView;


@property (weak, nonatomic) IBOutlet UIImageView *imgHot;
@property (weak, nonatomic) IBOutlet UIImageView *imgEventPic;

@property(nonatomic) BOOL strIdentifier;

-(void)setDetailOfMapCell:(NSDictionary *)dict;

@end
