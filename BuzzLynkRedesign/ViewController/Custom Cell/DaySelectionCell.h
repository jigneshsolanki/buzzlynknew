//
//  DaySelectionCell.h
//  BuzzLink5
//
//  Created by sotsys036 on 26/11/15.
//  Copyright © 2015 sotsys036. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DaySelectionCell : UICollectionViewCell

@property (nonatomic,strong) IBOutlet UILabel *lblDay;

@end
