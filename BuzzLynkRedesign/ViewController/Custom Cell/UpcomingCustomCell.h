//
//  UpcomingCustomCell.h
//  BuzzLynkRedesign
//
//  Created by sotsys036 on 09/03/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InsetLabel.h"

@interface UpcomingCustomCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgEvent;
@property (weak, nonatomic) IBOutlet UILabel *lblEventName;
@property (weak, nonatomic) IBOutlet UIImageView *EventType;
@property (weak, nonatomic) IBOutlet InsetLabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblCity;



@end
