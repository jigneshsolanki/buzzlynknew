//
//  HelpCustomcell.h
//  BuzzLynkRedesign
//
//  Created by sotsys036 on 07/03/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpCustomcell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblMenuTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblSubTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgArrow;

@end
