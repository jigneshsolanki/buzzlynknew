//
//  UpComingEventMapCustomCell.m
//  BuzzLynkRedesign
//
//  Created by SOTSYS0156 on 5/13/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import "UpComingEventMapCustomCell.h"
#import "Constants.h"




@implementation UpComingEventMapCustomCell


-(void)setDetailOfMapCell:(NSDictionary *)dict{
    
	
    self.vwStatus.layer.cornerRadius = 4;
    self.vwStatus.layer.masksToBounds = YES;
    //MANTHAN
    
    self.lblEventName.text = [NSString stringWithFormat:@"%@",[dict objectForKey:@"event_name"]];
    
    if([[dict objectForKey:@"event_is_hot"] boolValue]){
        self.imgHot.hidden = NO;
    }else{
        self.imgHot.hidden = YES;
    }
    
    if([[dict objectForKey:@"event_type"] isEqualToString:@"event"]){
        self.rateview.hidden = true;
        self.lblName.hidden = false;
        self.lblName.text = [NSString stringWithFormat:@"%@",[dict objectForKey:@"event_venue_name"]];
    }else{
        self.lblName.hidden = true;
        self.rateview.hidden = false;
        
        self.rateview.starSize = 10.0;
        self.rateview.rating = [[dict objectForKey:@"event_rating"] floatValue];
        self.rateview.starFillColor = [UIColor colorWithRed:236.0/255.0f green:133.0/255.0f
                                                       blue:46.0/255.0 alpha:1.0];
        
    }
    
    /*if([[dict objectForKey:@"event_price"]isEqualToString:@"FREE"]){
     [self.lblPrice setText:[NSString stringWithFormat:@"%@",[dict objectForKey:@"event_price"]]];
     }else{
     
     }*/
    
    [self.lblPrice setText:[NSString stringWithFormat:@"%@",[dict objectForKey:@"event_price"]]];
    
    [self.imgEventPic sd_setImageWithURL:[NSURL URLWithString:[dict objectForKey:@"event_image"]] placeholderImage:[UIImage imageNamed:@"Placeholder"] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    }];
    
    if([[dict objectForKey:@"event_categories"] length] != 0 && ((![[dict objectForKey:@"event_latitude"] isEqual:[NSNull null]] || ![[dict objectForKey:@"event_longitude"] isEqual:[NSNull null]]) || (AppDel.crntLat != 0.0 && AppDel.crntLat != 0.0))){
        
        CLLocation *currentLocation = [[CLLocation alloc]initWithLatitude:AppDel.crntLat longitude:AppDel.crntLon];
        
        CLLocation *serverLocation = [[CLLocation alloc]initWithLatitude:[[dict objectForKey:@"event_latitude"] floatValue] longitude:[[dict objectForKey:@"event_longitude"] floatValue]];
        
        CLLocationDistance meters = [currentLocation distanceFromLocation:serverLocation];
        
        [self.lblCategoryNMiles setText:[NSString stringWithFormat:@"%@ \u2022 %@",[dict objectForKey:@"event_categories"],[NSString stringWithFormat:@"%.1f mi",(meters * 0.000621371)]]];
        
    }else if([[dict objectForKey:@"event_categories"] length] == 0 && ((![[dict objectForKey:@"event_latitude"] isEqual:[NSNull null]] || ![[dict objectForKey:@"event_longitude"] isEqual:[NSNull null]]) || (AppDel.crntLat != 0.0 && AppDel.crntLat != 0.0))){
        
        
        CLLocation *currentLocation = [[CLLocation alloc]initWithLatitude:AppDel.crntLat longitude:AppDel.crntLon];
        
        CLLocation *serverLocation = [[CLLocation alloc]initWithLatitude:[[dict objectForKey:@"event_latitude"] floatValue] longitude:[[dict objectForKey:@"event_longitude"] floatValue]];
        
        CLLocationDistance meters = [currentLocation distanceFromLocation:serverLocation];
        
        [self.lblCategoryNMiles setText:[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%.1f mi",(meters * 0.000621371)]]];
        
    }else{
        
        [self.lblCategoryNMiles setText:[NSString stringWithFormat:@"%@",[dict objectForKey:@"event_categories"]]];
        
    }
    
    
    if([[dict objectForKey:@"event_latitude"] isEqual:[NSNull null]] || [[dict objectForKey:@"event_longitude"] isEqual:[NSNull null]]){
        
        [self.lblMiles setText:@""];
        
    }
    else
    {
        
        if(AppDel.crntLat == 0.0 && AppDel.crntLat == 0.0){
            
            [self.lblMiles setText:@"No mi"];
            
        }else{
            
            
            
        }
    }
    
    if(![[dict objectForKey:@"event_start_datetime"] isEqual: [NSNull null]] || ![[dict objectForKey:@"event_end_datetime"] isEqual:[NSNull null]])
    {
        
        if([[dict objectForKey:@"event_type"] isEqualToString:@"event"])
        {
            
            //If event thn display date
            
//            [lblDateNTime setText:[CommonMethod displayDateInformate:[[dict objectForKey:@"event_start_datetime"] doubleValue] endDate:[[dict objectForKey:@"event_end_datetime"] doubleValue]]];
			
            if([CommonMethod isBetweenDate:[[dict objectForKey:@"event_start_datetime"] doubleValue] andDate:[[dict objectForKey:@"event_end_datetime"] doubleValue]])
            {
                
                //                [self.vwStatus setBackgroundColor:[UIColor colorWithRed:(0.0/255.0) green:(145.0/255.0) blue:(71.0/255.0) alpha:1.0]];
                //                [self.lblStatus setText:@"Open Now"];
                
                [self.lblStatus setText:[CommonMethod displayOpenTimeMANTHAN:[[dict objectForKey:@"event_start_datetime"] doubleValue]]]; // MANTHAN
                [self.vwStatus setBackgroundColor:bluecolor]; // MANTHAN
                
            }
            else
            {
                //                [self.lblStatus setText:[CommonMethod displayOpenTime:[[dict objectForKey:@"event_start_datetime"] doubleValue]]];
                
                [self.lblStatus setText:[CommonMethod displayOpenTimeMANTHAN:[[dict objectForKey:@"event_start_datetime"] doubleValue]]]; // MANTHAN
                [self.vwStatus setBackgroundColor:bluecolor]; // MANTHAN
                
                //                [self.vwStatus setBackgroundColor:[UIColor colorWithRed:(249.0/255.0) green:(120.0/255.0) blue:(6.0/255.0) alpha:1.0]];
            }
            
            
            
            if (self.strIdentifier == TRUE )
            {
                
                //Logic of Upcoming
                
            }
            else
            {
                
                
                
            }
            
        }
        else
        {
            
            BOOL is24Hours;
            
            NSMutableDictionary *dictHours = [[dict objectForKey:@"operating_hours"] mutableCopy];
            
            NSString *dayName = [CommonMethod getCurrentWeekDay];
            
            if([CommonMethod isTimeZoneIn24Hours]){
                is24Hours = true;
            }else{
                is24Hours = false;
            }
            
            if(dictHours[dayName]){
                
                if([[[dictHours objectForKey:dayName] objectForKey:@"start"]isEqualToString:@""]){
                    //[lblDateNTime setText:@""];
                    [self.lblStatus setText:@""];
                    
                }else{
                    
                    if([[[dictHours objectForKey:dayName] objectForKey:@"start"] isEqualToString:@"Open 24 hours"]){
                        
                        //[lblDateNTime setText:[[dictHours objectForKey:dayName] objectForKey:@"start"]];
                        
                        //                        [self.lblStatus setText:@"Open Now"];
                        [self.lblStatus setText:[CommonMethod displayOpenTimeMANTHAN:[[dict objectForKey:@"event_start_datetime"] doubleValue]]]; // MANTHAN
                        [self.vwStatus setBackgroundColor:bluecolor]; // MANTHAN
                        
                        //                        [self.vwStatus setBackgroundColor:[UIColor colorWithRed:(0.0/255.0) green:(145.0/255.0) blue:(71.0/255.0) alpha:1.0]];
                        
                    }else if([[[dictHours objectForKey:dayName] objectForKey:@"start"] isEqualToString:@"Closed"]){
                        
                        //If Start time is close thn display time and status as close.
                        //[lblDateNTime setText:[[dictHours objectForKey:dayName] objectForKey:@"start"]];
                        
                        //[cell.lblStats setText:@"Closed Now"];
                        //[cell.lblStats setBackgroundColor:[UIColor redColor]];
                        
                        [self.lblStatus setText:@""];
                        [self.vwStatus setBackgroundColor:[UIColor clearColor]];
                        
                    }else{
                        
                        NSInteger startTime = 0,endTime = 0;
                        NSString *startTimeAMPM,*endTimeAMPM;
                        
                        if([[[dictHours objectForKey:dayName] objectForKey:@"end"] rangeOfString:@","].location != NSNotFound){
                            
                            NSString *strendTime = [[[[dictHours objectForKey:dayName] objectForKey:@"end"]componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@","]]objectAtIndex:0];
                            
                            NSArray *ary = [strendTime componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
                            
                            endTime = [[ary objectAtIndex:0] integerValue];
                            
							
							
							if (ary.count>1)
							{
								endTimeAMPM = [ary objectAtIndex:1];
								
							}else
							{
								endTimeAMPM =@"PM";
							}
                            
                            ary = [[[dictHours objectForKey:dayName] objectForKey:@"start"]componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
                            
                            startTime = [[ary objectAtIndex:0] integerValue];
                            
							
							if (ary.count>1)
							{
								startTimeAMPM = [ary objectAtIndex:1];
								
							}else
							{
								startTimeAMPM =@"PM";
							}
                            
                            
                        }else{
                            
                            NSArray *ary = [[[dictHours objectForKey:dayName] objectForKey:@"start"]componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
                            
                            startTime = [[ary objectAtIndex:0] integerValue];
                            
							
							if (ary.count>1)
							{
								startTimeAMPM = [ary objectAtIndex:1];
								
							}else
							{
								startTimeAMPM =@"PM";
							}
                            
                            ary = [[[dictHours objectForKey:dayName] objectForKey:@"end"]componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
                            
                            endTime = [[ary objectAtIndex:0] integerValue];
							
							if (ary.count>1)
							{
								 endTimeAMPM = [ary objectAtIndex:1];
								
							}else
							{
								endTimeAMPM =@"PM";
							}
							
                            
                            //[lblDateNTime setText:[NSString stringWithFormat:@"%@ - %@",[[dictHours objectForKey:dayName] objectForKey:@"start"],[[dictHours objectForKey:dayName] objectForKey:@"end"]]];
                        }
                        
                        /*if(is24Hours){
                         
                         
                         
                         if([CommonMethod getCurrentHours] >= startTime && [CommonMethod getCurrentHours] <= endTime){
                         [lblStatus setText:@"Open Now"];
                         }else{
                         [lblStatus setText:@"Closed Now"];
                         [lblStatus setTextColor:[UIColor redColor]];
                         }
                         
                         }else*//*{
                                 
                                 if([startTimeAMPM isEqualToString:@"pm"] || [startTimeAMPM isEqualToString:@"PM"]){
                                 startTime += 12;
                                 }
                                 
                                 if([endTimeAMPM isEqualToString:@"pm"] || [endTimeAMPM isEqualToString:@"PM"]){
                                 endTime += 12;
                                 }
                                 
                                 if([CommonMethod getCurrentHours] >= startTime && [CommonMethod getCurrentHours] <= endTime){
                                 [self.lblStats setText:@"Open Now"];
                                 [self.lblStats setBackgroundColor:[UIColor colorWithRed:(0.0/255.0) green:(145.0/255.0) blue:(71.0/255.0) alpha:1.0]];
                                 }else{
                                 //[cell.lblStats setText:@"Closed Now"];
                                 //[cell.lblStats setBackgroundColor:[UIColor redColor]];
                                 [self.lblStats setText:[NSString stringWithFormat:@"Opens at %ld",(long)startTime]];
                                 [self.lblStats setBackgroundColor:[UIColor colorWithRed:(249.0/255.0) green:(120.0/255.0) blue:(6.0/255.0) alpha:1.0]];
                                 }
                                 }*/
                        
                        if(is24Hours){
                            
                            if([startTimeAMPM isEqualToString:@"pm"] || [startTimeAMPM isEqualToString:@"PM"]){
                                startTime += 12;
                            }else{
                                startTime = startTime;
                            }
                            
                            if([endTimeAMPM isEqualToString:@"pm"] || [endTimeAMPM isEqualToString:@"PM"]){
                                endTime += 12;
                            }else{
                                endTime = endTime;
                            }
                            
                            if([CommonMethod getCurrentHours] >= startTime && [CommonMethod getCurrentHours] <= endTime){
                                //                                [self.lblStatus setText:@"Open Now"];
                                [self.lblStatus setText:[CommonMethod displayOpenTimeMANTHAN:[[dict objectForKey:@"event_start_datetime"] doubleValue]]]; // MANTHAN
                                
                                [self.vwStatus setBackgroundColor:bluecolor]; // MANTHAN
                                
                                //                                [self.vwStatus setBackgroundColor:[UIColor colorWithRed:(0.0/255.0) green:(145.0/255.0) blue:(71.0/255.0) alpha:1.0]];
                            }else{
                                
                                //                                [self.lblStatus setText:[NSString stringWithFormat:@"Opens at %ld%@",(long)startTime,[startTimeAMPM uppercaseString]]];
                                
                                
                                [self.lblStatus setText:[CommonMethod displayOpenTimeMANTHAN:[[dict objectForKey:@"event_start_datetime"] doubleValue]]]; // MANTHAN
                                [self.vwStatus setBackgroundColor:bluecolor]; // MANTHAN
                                
                                //                                [self.vwStatus setBackgroundColor:[UIColor colorWithRed:(249.0/255.0) green:(120.0/255.0) blue:(6.0/255.0) alpha:1.0]];
                            }
                            
                        }else{
                            
                            if([CommonMethod getCurrentHours] >= startTime && [CommonMethod getCurrentHours] <= endTime){
                                //                                [self.lblStatus setText:@"Open Now"];
                                [self.lblStatus setText:[CommonMethod displayOpenTimeMANTHAN:[[dict objectForKey:@"event_start_datetime"] doubleValue]]]; // MANTHAN
                                [self.vwStatus setBackgroundColor:bluecolor]; // MANTHAN
                                
                                //                                [self.vwStatus setBackgroundColor:[UIColor colorWithRed:(0.0/255.0) green:(145.0/255.0) blue:(71.0/255.0) alpha:1.0]];
                            }else{
                                
                                //                                [self.lblStatus setText:[NSString stringWithFormat:@"Opens at %ld%@",(long)startTime,[startTimeAMPM uppercaseString]]];
                                
                                
                                [self.lblStatus setText:[CommonMethod displayOpenTimeMANTHAN:[[dict objectForKey:@"event_start_datetime"] doubleValue]]]; // MANTHAN
                                [self.vwStatus setBackgroundColor:bluecolor]; // MANTHAN
                                
                                //                                [self.vwStatus setBackgroundColor:[UIColor colorWithRed:(249.0/255.0) green:(120.0/255.0) blue:(6.0/255.0) alpha:1.0]];
                                
                                
                            }
                            
                        }
                    }
                }
            }else{
                
                [self.lblStatus setText:@""];
                [self.vwStatus setBackgroundColor:[UIColor clearColor]];
                //[self.lblStats setHidden:true];
            }
        }
        
    }
    
    
    else
    {
        [self.lblStatus setText:@""];
        [self.vwStatus setBackgroundColor:[UIColor clearColor]];
    }
    
}




@end
