//
//  SearchCustomCell.h
//  BuzzLynkRedesign
//
//  Created by sotsys036 on 17/03/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchCustomCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblSearch;

@end
