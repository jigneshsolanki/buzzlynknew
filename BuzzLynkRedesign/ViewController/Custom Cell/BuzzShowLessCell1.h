//
//  BuzzShowLessCell1.h
//  BuzzLink5
//
//  Created by sotsys036 on 15/12/15.
//  Copyright © 2015 sotsys036. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BuzzShowLessCell1 : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView1;
@property (weak, nonatomic) IBOutlet UILabel *lblCounter;
@property (weak, nonatomic) IBOutlet UILabel *lblCategoryName;

-(void)setBuzzCellDetail:(NSDictionary *)dict counterColor:(UIColor *)color;
@end
