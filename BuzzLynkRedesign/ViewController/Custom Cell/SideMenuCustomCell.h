//
//  SideMenuCustomCell.h
//  BuzzLynkRedesign
//
//  Created by sotsys036 on 03/03/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideMenuCustomCell : UITableViewCell{

}

@property (weak, nonatomic) IBOutlet UIImageView *imgMenu;

@property (weak, nonatomic) IBOutlet UILabel *lblMenuname;

@end
