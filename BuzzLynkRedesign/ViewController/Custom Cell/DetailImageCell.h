//
//  DetailImageCell.h
//  BuzzLynkRedesign
//
//  Created by sotsys036 on 14/04/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailImageCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *detailImageCell;

@end
