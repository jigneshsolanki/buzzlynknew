//
//  EventCustomCell.m
//  BuzzLynkRedesign
//
//  Created by sotsys036 on 03/03/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import "EventCustomCell.h"
#import "FXBlurView.h"
#import <QuartzCore/QuartzCore.h>


@implementation EventCustomCell

@synthesize isFromCategory;

- (void)awakeFromNib {
	
    // Initialization code
}

-(void)setListCellEventDetail:(NSDictionary *)dict{
	
	//NSLog(isFromCategory ? @"Yes" : @"No");
	
    self.lblEventName.text = [NSString stringWithFormat:@"%@",[dict objectForKey:@"event_name"]];
    self.lblCity.text = [NSString stringWithFormat:@"%@",[dict objectForKey:@"event_city"]];
    
    if([[dict objectForKey:@"is_favorite"] boolValue]){
        self.btnFav.selected = YES;
    }else{
        self.btnFav.selected = NO;
    }
    
    self.lblCategoryName.text = [NSString stringWithFormat:@"%@",[dict objectForKey:@"event_categories"] ];
    
    [self.btnFav setTag:[[dict objectForKey:@"event_id"] integerValue]];
    
    if([[dict objectForKey:@"event_is_hot"] boolValue]){
        self.imgHot.hidden = NO;
    }else{
        self.imgHot.hidden = YES;
    }
    //[[dict objectForKey:@"event_type"] isEqualToString:@"event"]
    if([[dict objectForKey:@"event_type"] isEqualToString:@"event"]){
        self.rateView.hidden = true;
        self.lblRating.hidden = true;
        self.lblName.hidden = false;
        self.lblName.text = [NSString stringWithFormat:@"%@",[dict objectForKey:@"event_venue_name"]];
    }else{
        self.lblName.hidden = true;
        self.rateView.hidden = false;
        self.lblRating.hidden = false;
        self.rateView.starSize = 10.0;
        self.rateView.rating = [[dict objectForKey:@"event_rating"] floatValue];
		self.rateView.starFillColorFlag = TRUE;
        self.rateView.starFillColor = [UIColor colorWithRed:255/255.0f green:209/255.0f
                                                       blue:23/255.0 alpha:1.0];
        
    }
    
	
	//NSLog(@"%@",[NSString stringWithFormat:@"%@",[dict objectForKey:@"event_price"]]);
	
    [self.lblPrice setText:[NSString stringWithFormat:@"%@",[dict objectForKey:@"event_price"]]];
    //hirpara1
    [self.imgEvent sd_setImageWithURL:[NSURL URLWithString:[dict objectForKey:@"event_image"]] placeholderImage:[UIImage imageNamed:@"Placeholder"] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
		
    }];


    NSMutableDictionary *dictDays = [[dict objectForKey:@"day_events"] mutableCopy];
    
    NSString *dayName = [CommonMethod getCurrentWeekDay];
    
    if(dictDays.count > 0)
	{
        if(dictDays[dayName])
		{
            self.lblNameVenue.text = dictDays[dayName][@"event_name"];
        }
    }else{
        self.lblNameVenue.text = @"";
    }
    
    if([[dict objectForKey:@"event_latitude"] isEqual:[NSNull null]] || [[dict objectForKey:@"event_longitude"] isEqual:[NSNull null]]){
        
        [self.lblDistance setText:@""];
        
    }else{
        
        if(AppDel.crntLat == 0.0 && AppDel.crntLat == 0.0)
		{
            [self.lblDistance setText:@""];
            
        }else{
            
            CLLocation *currentLocation = [[CLLocation alloc]initWithLatitude:AppDel.crntLat longitude:AppDel.crntLon];
            
            CLLocation *serverLocation = [[CLLocation alloc]initWithLatitude:[[dict objectForKey:@"event_latitude"] floatValue] longitude:[[dict objectForKey:@"event_longitude"] floatValue]];
            
            CLLocationDistance meters = [currentLocation distanceFromLocation:serverLocation];
            
            [self.lblDistance setText:[NSString stringWithFormat:@"%.1f mi",(meters * 0.000621371)]];
            
        }
    }
    
    if(![[dict objectForKey:@"event_start_datetime"] isEqual: [NSNull null]] || ![[dict objectForKey:@"event_end_datetime"] isEqual:[NSNull null]]){
        
        if([[dict objectForKey:@"event_type"] isEqualToString:@"event"])
		{
            
            //If event thn display date OPE
            if([CommonMethod isBetweenDate:[[dict objectForKey:@"event_start_datetime"] doubleValue] andDate:[[dict objectForKey:@"event_end_datetime"] doubleValue]]){
                
				[self.vwStatus setBackgroundColor:greencolor]; //paresh
				[self.lblStats setText:OPENNOW];
				
            }else{
				
		
				if (AppDel.isFromUpcoming)
				{
					[self.lblStats setText:[CommonMethod displayOpenTimeMANTHAN:[[dict objectForKey:@"event_start_datetime"] doubleValue]]];
					[self.vwStatus setBackgroundColor:bluecolor];
					
				}else
				{
					 [self.lblStats setText:[CommonMethod displayOpenTime:[[dict objectForKey:@"event_start_datetime"] doubleValue]]];
					[self.vwStatus setBackgroundColor:[UIColor colorWithRed:(249.0/255.0) green:(120.0/255.0) blue:(6.0/255.0) alpha:1.0]];
				}
				//NSLog(@"%@",self.lblStats.text);
				
                //[self.lblStats setBackgroundColor:[UIColor colorWithRed:(249.0/255.0) green:(120.0/255.0) blue:(6.0/255.0) alpha:1.0]];
				
            }
            
        }else
		{
            
            BOOL is24Hours;
            
            NSMutableDictionary *dictHours = [[dict objectForKey:@"operating_hours"] mutableCopy];
            
            NSString *dayName = [CommonMethod getCurrentWeekDay];
            
            if([CommonMethod isTimeZoneIn24Hours])
			{
                is24Hours = true;
            }else{
                is24Hours = false;
            }
            
            if(dictHours[dayName])
			{
                
                if([[[dictHours objectForKey:dayName] objectForKey:@"start"]isEqualToString:@""])
				{
					
					//IF START TIME IS NILL:-
					
                    //[lblDateNTime setText:@""];
                    [self.lblStats setText:@""];
                    [self.vwStatus setBackgroundColor:[UIColor clearColor]];
                }else{
                    
                    if([[[dictHours objectForKey:dayName] objectForKey:@"start"] isEqualToString:@"Open 24 hours"])
					{
                        
                        //[lblDateNTime setText:[[dictHours objectForKey:dayName] objectForKey:@"start"]];
                        
                        [self.lblStats setText:OPENNOW];
						[self.vwStatus setBackgroundColor:greencolor]; //paresh
		
                        
                    }else if([[[dictHours objectForKey:dayName] objectForKey:@"start"] isEqualToString:@"Closed"]){
						
                        
                        [self.lblStats setText:@""];
                        [self.vwStatus setBackgroundColor:[UIColor clearColor]];
                        
                    }else{
                        
                        NSInteger startTime = 0,endTime = 0;
                        NSString *startTimeAMPM,*endTimeAMPM;
                        
                        if([[[dictHours objectForKey:dayName] objectForKey:@"end"] rangeOfString:@","].location != NSNotFound){
                            
                            NSString *strendTime = [[[[dictHours objectForKey:dayName] objectForKey:@"end"]componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@","]]objectAtIndex:0];
                            
                            NSArray *ary = [strendTime componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
                            
                            endTime = [[ary objectAtIndex:0] integerValue];
							
							if (ary.count>1)
							{
								 endTimeAMPM = [ary objectAtIndex:1];
								
							}else
							{
								endTimeAMPM =@"PM";
							}
							
                            
                            ary = [[[dictHours objectForKey:dayName] objectForKey:@"start"]componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
                            
                            startTime = [[ary objectAtIndex:0] integerValue];
							
							if (ary.count>1)
							{
								startTimeAMPM = [ary objectAtIndex:1];
								
							}else
							{
								startTimeAMPM =@"AM";
								
							}
							
                            
                        }else{
							
							
							//IF 24 AND GET START TIME & END TIME:
							
							
                            NSArray *ary = [[[dictHours objectForKey:dayName] objectForKey:@"start"]componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
                            
                            startTime = [[ary objectAtIndex:0] integerValue];
						
							
							///NSLog(@"START%ld",(long)startTime);
							
							
							if (ary.count>1)
							{
								startTimeAMPM = [ary objectAtIndex:1];
								
							}else
							{
								startTimeAMPM = @"AM";
							}
							
							//NSLog(@"AM PM IS%@",startTimeAMPM);
							
                            
                            ary = [[[dictHours objectForKey:dayName] objectForKey:@"end"]componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
                            
                            endTime = [[ary objectAtIndex:0] integerValue];
							
							if (ary.count>1)
							{
								endTimeAMPM = [ary objectAtIndex:1];
								
							}else
							{
								endTimeAMPM = @"PM";
								
							}

                        }
						
                        if(is24Hours)
						{
                            
                            if([startTimeAMPM isEqualToString:@"pm"] || [startTimeAMPM isEqualToString:@"PM"])
							{
                                startTime += 12;
                            }else{
                                startTime = startTime;
                            }
                            
                            if([endTimeAMPM isEqualToString:@"pm"] || [endTimeAMPM isEqualToString:@"PM"])
							{
                                endTime += 12;
                            }else{
                                endTime = endTime;
                            }
                            
                            if([CommonMethod getCurrentHours] >= startTime && [CommonMethod getCurrentHours] <= endTime){
                                [self.lblStats setText:OPENNOW];
								[self.vwStatus setBackgroundColor:greencolor]; //paresh
						
                            }else
							{
								
								NSString *strStarTimeFinal = [[[dictHours objectForKey:dayName] objectForKey:@"start"] uppercaseString];
								 [self.lblStats setText:[NSString stringWithFormat:@"OPEN\n%@",strStarTimeFinal]];
			
								[self.vwStatus setBackgroundColor:orangecolor]; //paresh for orange
                            }
                            
                        }else{
                            
                            if([CommonMethod getCurrentHours] >= startTime && [CommonMethod getCurrentHours] <= endTime){
                                [self.lblStats setText:OPENNOW];
								[self.vwStatus setBackgroundColor:greencolor]; // paresh
								
                            }else{
							
								NSString *strStarTimeFinal = [[[dictHours objectForKey:dayName] objectForKey:@"start"] uppercaseString];
								
								[self.lblStats setText:[NSString stringWithFormat:@"OPEN\n%@",strStarTimeFinal]];

								[self.vwStatus setBackgroundColor:orangecolor]; //paresh for orange
                            }
                            
                        }
                    }
                }
            }else{
                
                [self.lblStats setText:@""];
                [self.vwStatus setBackgroundColor:[UIColor clearColor]];
                //[self.lblStats setHidden:true];
            }
        }
    }else{
        [self.lblStats setText:@""];
        [self.vwStatus setBackgroundColor:[UIColor clearColor]];
    }
	self.vwStatus.layer.cornerRadius = 4;
	self.vwStatus.layer.masksToBounds = YES;
    
}



@end
