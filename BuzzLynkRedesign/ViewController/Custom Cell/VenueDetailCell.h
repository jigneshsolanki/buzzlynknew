//
//  VenueDetailCell.h
//  BuzzLynkRedesign
//
//  Created by sotsys036 on 19/04/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VenueDetailCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblDayName;
@property (weak, nonatomic) IBOutlet UILabel *lblDayTime;


@end
