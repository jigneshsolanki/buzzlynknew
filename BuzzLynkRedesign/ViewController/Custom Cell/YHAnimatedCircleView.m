/*
 Copyright 2012 Yick-Hong Lam
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License. 
*/

#import <QuartzCore/QuartzCore.h>
#import "YHAnimatedCircleView.h"



@implementation YHAnimatedCircleView

-(id)initWithCircle:(MKCircle *)circle{
	

//	[[NSNotificationCenter defaultCenter] addObserver:self
//											  selector:@selector(start)
//												  name:@"StartAnimation" object:nil];
	
	self = [super initWithCircle:circle];
	
	
//    if(self)
//	{
//        [self start];
//    }
	
    return self;
}



-(void)dealloc{
	
	/*[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(start)
												 name:@"StartAnimation" object:nil];*/
	
    //[self removeExistingAnimation];
    //[super dealloc];
}



-(void)stop{
    
    [self removeExistingAnimation];    
}

-(void)removeExistingAnimation{
	
	
    if(imageView){
        [imageView.layer removeAllAnimations];
        [imageView removeFromSuperview];
        imageView = nil;
    }
}

-(void)hideAllImageViewAnimation
{
	
}


- (void)drawMapRect:(MKMapRect)mapRect
          zoomScale:(MKZoomScale)zoomScale
          inContext:(CGContextRef)ctx
{
	//NSLog(@"paresh");
	
	

    //the circle center
    MKMapPoint mpoint = MKMapPointForCoordinate([[self overlay] coordinate]);
	

    //geting the radius in map point
    double radius = [(MKCircle*)[self overlay] radius];
	//double radius = 500;
	
    double mapRadius = radius * MKMapPointsPerMeterAtLatitude([[self overlay] coordinate].latitude);
    
    //calculate the rect in map coordination
    MKMapRect mrect = MKMapRectMake(mpoint.x - mapRadius, mpoint.y - mapRadius, mapRadius * 2, mapRadius * 2);
	
	
	//MKMapPoint annotationPoint = MKMapPointForCoordinate(mpoin.userLocation.coordinate);
	
	
	
	
    //get the rect in pixel coordination and set to the imageView
    CGRect rect = [self rectForMapRect:mrect];
    
    if(imageView){
        imageView.frame = rect;
    }
}

@end
