//
//  SearchVC.h
//  BuzzLynkRedesign
//
//  Created by sotsys036 on 08/03/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@interface SearchVC : UIViewController<UITextFieldDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>{
    
    __weak IBOutlet UILabel *lblSearchTitle;
    
    __weak IBOutlet UITextField *txtSearch;
    
    __weak IBOutlet UILabel *lblRecentPopularSearch;
    
    __weak IBOutlet UIView *vwSearch;
    
    __weak IBOutlet UICollectionView *clgSearchResult;
    
    __weak IBOutlet UICollectionView *clgTopSearch;
    
    __weak IBOutlet NSLayoutConstraint *recentSearchHeightCons;
    
    AFHTTPRequestOperationManager *afmanager;
    
    NSMutableArray *arrySearch,*arryTopSearch;
    
}

@property(nonatomic,retain)NSString *strTitle;

@end
