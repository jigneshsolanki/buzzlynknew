//
//  InsetLabel.h
//  BuzzLynkRedesign
//
//  Created by SOTSYS037 on 24/05/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InsetLabel : UILabel

@property (nonatomic, assign) UIEdgeInsets edgeInsets;

@end
