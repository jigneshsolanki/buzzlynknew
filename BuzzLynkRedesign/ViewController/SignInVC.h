//
//  SignInVC.h
//  BuzzLynkRedesign
//
//  Created by sotsys036 on 04/03/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@interface SignInVC : UIViewController<UITextFieldDelegate,UIAlertViewDelegate>{
    
    __weak IBOutlet UITextField *txtEmail;
    __weak IBOutlet UITextField *txtPassword;
    
    __weak IBOutlet UIImageView *imgFbLogin;
    
    
    __weak IBOutlet UIView *vwEmail;
    __weak IBOutlet UIView *vwPassword;
    __weak IBOutlet UIView *vwSignIn;
    
    __weak IBOutlet UIButton *btnLogin;
    
    AFHTTPRequestOperationManager *manager;
}

@end
