//
//  InsetLabel.m
//  BuzzLynkRedesign
//
//  Created by SOTSYS037 on 24/05/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import "InsetLabel.h"

@implementation InsetLabel

- (id)initWithFrame:(CGRect)frame{
	self = [super initWithFrame:frame];
	if (self) {
		self.edgeInsets = UIEdgeInsetsMake(2, 3, 2, 3);
	}
	return self;
}

- (void)drawTextInRect:(CGRect)rect {
	[super drawTextInRect:UIEdgeInsetsInsetRect(rect, self.edgeInsets)];
}

- (CGSize)intrinsicContentSize
{
	CGSize size = [super intrinsicContentSize];
	size.width  += self.edgeInsets.left + self.edgeInsets.right;
	size.height += self.edgeInsets.top + self.edgeInsets.bottom;
	return size;
}

@end
