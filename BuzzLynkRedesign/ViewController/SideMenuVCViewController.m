//
//  SideMenuVCViewController.m
//  BuzzLynkRedesign
//
//  Created by sotsys036 on 02/03/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import "SideMenuVCViewController.h"
#import "ViewController.h"
#import "SideMenuCustomCell.h"
#import "SignInVC.h"
#import "FavoriteVC.h"
#import "HelpVC.h"
#import "LegalVC.h"

@interface SideMenuVCViewController ()

@end

@implementation SideMenuVCViewController

@synthesize selectedIndex;

- (void)viewDidLoad {
    [super viewDidLoad];
    //arryMenu = [NSArray arrayWithObjects:@"Favorites",@"Help",@"Legal",nil];
	
	
	
	
    [lblSignIn setFont:[lblSignIn.font fontWithSize:[CommonMethod LeftMenuMenuTitle]]];
    
    UITapGestureRecognizer *buzzTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(buzzIconTapped:)];
    buzzTap.numberOfTapsRequired = 1;
    buzzTap.numberOfTouchesRequired = 1;
    
    [imgLogo addGestureRecognizer:buzzTap];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(changeLabelText:)
                                                 name:@"changeStatus"
                                               object:nil];
    
    if([[NSUserDefaults standardUserDefaults]boolForKey:@"isLogin"]){
        [lblSignIn setText:@"Sign out"];
        [imgSignIn setImage:[UIImage imageNamed:@"signout"]];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if(!self.selectedIndex){
        self.selectedIndex = [NSString stringWithFormat:@"0"];
    }
}

#pragma  mark - UITableViewDelegate and DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arryMenu.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    SideMenuCustomCell *cell = (SideMenuCustomCell *)[tableView dequeueReusableCellWithIdentifier:@"SideMenuCell"];
    
    cell.backgroundColor = [UIColor clearColor];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.lblMenuname.text = [arryMenu objectAtIndex:indexPath.row];
    cell.imgMenu.image = [UIImage imageNamed:[arryMenu objectAtIndex:indexPath.row]];
    
    return cell;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 60.0;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    self.selectedIndex = [NSString stringWithFormat:@"%ld",(long)indexPath.row];
    
    switch (indexPath.row) {
        case 0:
            [self favoriteMenuButtonPressed];
            break;
        case 1:
            [self helpPressed];
            break;
        case 2:
            //[self helpMenuPressed];
            break;
        case 3:
            //[self legalMenuPressed];
            break;
        default:
            break;
    }
}

#pragma mark - Private Methods
-(void)favoriteMenuButtonPressed{
    NSString *str =[[NSUserDefaults standardUserDefaults]objectForKey:@"user_id"];
   // NSLog(@"%@ ",[[NSUserDefaults standardUserDefaults]objectForKey:@"user_id"]);
  // NSLog(@"str --- %@ ",str);
    
    if([str length] == 0){
        DisplayAlert(@"You are not login, Please login first");
        
    }else{
        //DisplayAlert(@"Fav Screen");
        FavoriteVC *favVC = [self.storyboard instantiateViewControllerWithIdentifier:@"FavoriteVC"];
        AppDel.slideMenuVC.mainViewController=favVC;
    }
}

-(void)helpPressed{
	
    HelpVC *hlpvc = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpVC"];
    AppDel.slideMenuVC.mainViewController = hlpvc;
}
-(void)contacusPressed
{
	//NSLog(@"contacus");
	if ([MFMailComposeViewController canSendMail])
	{
		MFMailComposeViewController *mailController = [[MFMailComposeViewController alloc] init];
		mailController.mailComposeDelegate = self;
		NSArray *toArray = [NSArray arrayWithObjects:@"Support@buzzlynk.com", nil];
		[mailController setToRecipients:toArray];
		[self presentViewController:mailController animated:YES completion:nil];
	}
	else
	{
		//NSLog(@"Done email");
		//[Common showNWAlert:self.view andMessage:[dict objectForKey:@"msgConfigureMail"]];
	}
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
	switch (result)
	{
		case MFMailComposeResultCancelled:
			//  NSLog(@"canceled");
			break;
		case MFMailComposeResultSaved:
			// NSLog(@"saved");
			break;
		case MFMailComposeResultSent:
			  NSLog(@"sent");
			//[self showAlertMessage:[dict objectForKey:@"msgEmailSent"]];
			
			break;
		case MFMailComposeResultFailed:
			// NSLog(@"failed");
			//[self showAlertMessage:[dict objectForKey:@"msgEmailFailed"]];
			break;
		default:
			// NSLog(@"not sent");
			break;
	}
	[self dismissViewControllerAnimated:YES completion:nil];
}


-(void)legalButtonPressed{
    LegalVC *legalVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LegalVC"];
    AppDel.slideMenuVC.mainViewController = legalVC;
}


#pragma mark - Notification method
-(void)changeLabelText:(NSNotification *)notification{
    if([[notification.userInfo objectForKey:@"SignInStatus"] isEqualToString:@"Yes"]){
        
        [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"isLogin"];
        
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [lblSignIn setText:@"Sign out"];
        [imgSignIn setImage:[UIImage imageNamed:@"signout"]];
        
    }else{
        [[NSUserDefaults standardUserDefaults]setBool:false forKey:@"isLogin"];
        
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [lblSignIn setText:@"Sign in"];
        [imgSignIn setImage:[UIImage imageNamed:@"signIn"]];
    }
}

#pragma mark - UIAlertView Delegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 0){
        
        //[[NSUserDefaults standardUserDefaults]setObject:@"nil" forKey:@"user_id"];
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"user_id"];
        
        [lblSignIn setText:@"Sign in"];
        [imgSignIn setImage:[UIImage imageNamed:@"signIn"]];
        
        [[NSUserDefaults standardUserDefaults]setBool:false forKey:@"isLogin"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        SignInVC *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SignInVC"];
        [AppDel.slideMenuVC setMainViewController:loginVC];
        
    }else{
        //Do Nothing.
    }
}

#pragma mark - UITapGesture Methods
-(void)buzzIconTapped:(UITapGestureRecognizer *)gesture{
    ViewController *mainVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [AppDel.slideMenuVC setMainViewController:mainVC];
    
}

#pragma mark - ButtonActions
- (IBAction)menuOptionPressed:(id)sender {
    
    UIButton *btn = (UIButton *)sender;
    switch (btn.tag) {
        case 55:
            [self favoriteMenuButtonPressed];
            break;
        case 56:
            [self helpPressed];
            break;
        case 57:
            [self legalButtonPressed];
            break;
		case 60:
			[self contacusPressed];
			break;
			
			
        default:
            break;
    }
    
}


-(IBAction)signInButtonPressed:(id)sender{
    
    if([[NSUserDefaults standardUserDefaults]boolForKey:@"isLogin"]){
        
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"BuzzLynk" message:@"Are you sure want to sign out?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
        
        [alertView show];
        
    }else{
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        SignInVC *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SignInVC"];
        [appDelegate.slideMenuVC setMainViewController:loginVC];
    }
}

#pragma mark - Receive Memory Warning.
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
