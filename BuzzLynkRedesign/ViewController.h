//
//  ViewController.h
//  BuzzLynkRedesign
//
//  Created by sotsys036 on 02/03/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CAPSPageMenu.h"
#import "CommonMethod.h"
@interface ViewController : UIViewController<CAPSPageMenuDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource>{
    
	IBOutlet UIView *navigationView;
	IBOutlet NSLayoutConstraint *filterViewHeightConstraint;
	IBOutlet NSLayoutConstraint *filterViewTopConstraint;
    __weak IBOutlet UIButton *btnDown;
    __weak IBOutlet UILabel *lblTitle;
    __weak IBOutlet UICollectionView *clgDay;
    __weak IBOutlet UIView *vwFilter;
    
	IBOutlet UIView *viewdays;
	IBOutlet NSLayoutConstraint *topconstrain;
	
	IBOutlet NSLayoutConstraint *heightConstant;
	
    __weak IBOutlet UIButton *btnDay;
    __weak IBOutlet UIButton *btnNight;
    
    NSString *strDaySelected;
    NSMutableArray *arrData;
    
    NSMutableDictionary *dictFilter;
    
}

@property (nonatomic, strong) UICollectionViewFlowLayout *flowLayout;

-(IBAction)menuBtnPressed:(id)sender;
-(IBAction)searchBtnPressed:(id)sender;
-(IBAction)downArrowBtnPressed:(id)sender;

@end

