//
//  ViewController.m
//  BuzzLynkRedesign
//
//  Created by sotsys036 on 02/03/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import "ViewController.h"
#import "AtlBuzzVC.h"
#import "DaySelectionCell.h"
#import "UpComingViewController.h"
#import "BuzzViewController.h"
#import "Constants.h"
#import "SearchVC.h"

@interface ViewController ()
@property (nonatomic) CAPSPageMenu *pageMenu;
@end

@implementation ViewController

@synthesize flowLayout;
#pragma mark - View Controller
- (void)viewDidLoad {
	[super viewDidLoad];
	
	filterViewTopConstraint.constant = filterViewTopConstraint.constant - filterViewHeightConstraint.constant;
	
	[self.view bringSubviewToFront:navigationView];
	
	[AppDel.slideMenuVC.navigationController setNavigationBarHidden:YES];
	
	arrData = [NSMutableArray array];
	
	dictFilter = [NSMutableDictionary dictionary];
	
	strDaySelected = @"0";
	
	[lblTitle setText:[CommonMethod setDayStatus]];
	
	if([lblTitle.text isEqualToString:@"Today"]){
		
		btnDay.selected = true;
		btnNight.selected = false;
		
	}else{
		
		btnNight.selected = true;
		btnDay.selected = false;
	}
	
	// Configure layout
	self.flowLayout = [[UICollectionViewFlowLayout alloc] init];
	[self.flowLayout setItemSize:CGSizeMake(CGRectGetWidth(self.view.frame)/7, 40)];
	[self.flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
	
	
	int spaceing;
	if(IS_IPHONE_4 )
	{
		spaceing=0;
	}else if(IS_IPHONE_5)
	{
		spaceing=0;
	}else if(IS_IPHONE_6)
	{
		spaceing = 9;
	}else
	{
		spaceing = 16;
	}

	self.flowLayout.minimumInteritemSpacing =spaceing;
	[clgDay setCollectionViewLayout:self.flowLayout];
	clgDay.bounces = YES;
	[clgDay setShowsHorizontalScrollIndicator:NO];
	[clgDay setShowsVerticalScrollIndicator:NO];
	self.flowLayout.estimatedItemSize = CGSizeMake(CGRectGetWidth(self.view.frame)/7, 40);

	[self setUpDayCollectionView];
	[self setupTopMenu];
}

-(void)viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
	[self.navigationController setNavigationBarHidden:YES];
	[AppDel.slideMenuVC.navigationController setNavigationBarHidden:YES];
	
}
-(void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
	
	[self.navigationController setNavigationBarHidden:YES];
	[AppDel.slideMenuVC.navigationController setNavigationBarHidden:YES];
	
}

-(void)viewDidDisappear:(BOOL)animated{
	[super viewDidDisappear:animated];
	
	//[[NSNotificationCenter defaultCenter]removeObserver:self name:@"RefreshView" object:nil];
	//[[NSNotificationCenter defaultCenter]removeObserver:self name:@"showFiterView" object:nil];
}

#pragma mark - UICollectionViewDelegate and Datasource
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
	
	return arrData.count;
	
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
	DaySelectionCell *cell = (DaySelectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"DayListCell" forIndexPath:indexPath];
	
	[cell.lblDay setText:[NSString stringWithFormat:@"%@",[arrData objectAtIndex:indexPath.row]]];
	
	if([strDaySelected isEqualToString:[NSString stringWithFormat:@"%ld",(long)indexPath.row]]){
		cell.lblDay.textColor = [UIColor whiteColor];
	}else
	{
		cell.lblDay.textColor = [UIColor colorWithRed:(167.0/255.0) green:(189.0/255.0) blue:(220.0/255.0) alpha:1.0];
	}
	
	
	cell.backgroundColor = [UIColor clearColor];
	
	return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
	
	[self animationStop];
	
	strDaySelected = [NSString stringWithFormat:@"%ld",(long)indexPath.row];
	
	[clgDay reloadData];
	
	if(dictFilter.count > 0){
		[dictFilter removeAllObjects];
	}
	
	if([[arrData objectAtIndex:indexPath.row]isEqualToString:@"Today"] || [[arrData objectAtIndex:indexPath.row] isEqualToString:@"Tonight"])
	{
		[lblTitle setText:[arrData objectAtIndex:indexPath.row]];
		
	}else if([[arrData objectAtIndex:indexPath.row] isEqualToString:@"Mon"]){
		[lblTitle setText:@"Monday"];
	}else if([[arrData objectAtIndex:indexPath.row] isEqualToString:@"Tue"]){
		[lblTitle setText:@"Tuesday"];
	}else if([[arrData objectAtIndex:indexPath.row] isEqualToString:@"Wed"]){
		[lblTitle setText:@"Wednesday"];
	}else if([[arrData objectAtIndex:indexPath.row] isEqualToString:@"Thu"]){
		[lblTitle setText:@"Thursday"];
	}else if([[arrData objectAtIndex:indexPath.row] isEqualToString:@"Fri"]){
		[lblTitle setText:@"Friday"];
	}else if([[arrData objectAtIndex:indexPath.row] isEqualToString:@"Sat"]){
		[lblTitle setText:@"Saturday"];
	}else if ([[arrData objectAtIndex:indexPath.row] isEqualToString:@"Sun"]){
		[lblTitle setText:@"Sunday"];
	}
	
	[dictFilter setObject:strDaySelected forKey:@"week_day"];
	
	if([btnDay isSelected])
	{
		[dictFilter setObject:@"day" forKey:@"day_night"];
	}else if([btnNight isSelected])
	{
		[dictFilter setObject:@"night" forKey:@"day_night"];
	}else{
		//Do nothing.
	}
	
	[btnDown setSelected:NO];
	
	if(AppDel.isFromCategory){
		
		[[NSNotificationCenter defaultCenter]postNotificationName:@"dayFilteration"object:self
														 userInfo:dictFilter];
		
	}else{
		
		[[NSNotificationCenter defaultCenter]postNotificationName:@"dayFilterationOFAtl"object:self
														 userInfo:dictFilter];
		
	}
	
}


//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
//	
//	//NSLog(@"%d",CGSizeMake(collectionView.frame.size.width / 7, collectionView.frame.size.height ));
//	
//	//  return CGSizeMake(self.view.frame.size.width / 7, collectionView.frame.size.height );
//	
//	return CGSizeMake(collectionView.frame.size.width / 7, collectionView.frame.size.height );
//	
//	//paresh
//	
//}


#pragma mark - Private Method

-(void)setupTopMenu{
	
	AtlBuzzVC *atlBuzz = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"AtlBuzzVC"];
	atlBuzz.title = @"ATL Buzz";
	
	UpComingViewController *upcoming = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"upcomingVC"];
	upcoming.title = @"Upcoming";
	
	BuzzViewController *buzz = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"BuzzId"];
	buzz.title = @"Categories";
	
	NSArray *controllerArray = @[atlBuzz, buzz, upcoming];
	
	NSDictionary *parameters = @{
								 CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor whiteColor],
								 CAPSPageMenuOptionViewBackgroundColor: [UIColor whiteColor],
								 CAPSPageMenuOptionSelectionIndicatorColor: [UIColor colorWithRed:(54.0/255.0) green:(105.0/255.0) blue:(189.0/255.0) alpha:1.0],
								 /*CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor colorWithRed:(54.0/255.0) green:(105.0/255.0) blue:(189.0/255.0) alpha:1.0],*/
								 CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor lightGrayColor],
								 
								 // CAPSPageMenuOptionSelectedMenuItemLabelColor:[UIColor redColor],
								 
								 CAPSPageMenuOptionSelectedMenuItemLabelColor:[UIColor colorWithRed:(54.0/255.0) green:(105.0/255.0) blue:(189.0/255.0) alpha:1.0],
								 
								 
								 CAPSPageMenuOptionUnselectedMenuItemLabelColor : [UIColor colorWithRed:(155.0/255.0) green:(155.0/255.0) blue:(155.0/255.0) alpha:1.0],
								 
								 CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"Helvetica Neue" size:[CommonMethod pageMenuFontSize]],
								 //paresh
								 
								 
								 
								 // Helvetica Neue-Bold
								 
								 // Neue-Bold
								 
								 CAPSPageMenuOptionMenuHeight: @(35.0),
								 CAPSPageMenuOptionMenuItemWidthBasedOnTitleTextWidth: @(NO),
								 CAPSPageMenuOptionMenuItemWidth: @([CommonMethod pageMenuSepWidth]),
								 
								 CAPSPageMenuOptionCenterMenuItems: @(NO),
								 CAPSPageMenuOptionSelectionIndicatorHeight : @(2.0),
								 };
	
	if(_pageMenu){
		[_pageMenu.view removeFromSuperview];
		_pageMenu.delegate = nil;
		_pageMenu = nil;
	}
	
	_pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, 64.0, self.view.frame.size.width, self.view.frame.size.height-64.0) options:parameters];
	
	_pageMenu.delegate = self;
	
	if([btnDown isSelected]){
		_pageMenu.menuScrollView.hidden = YES;
	}else{
		_pageMenu.menuScrollView.hidden = NO;
	}
	
	[self.view addSubview:_pageMenu.view];
}

-(void)setUpDayCollectionView{
	
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"EE"];
	
	NSString *dayName = [dateFormatter stringFromDate:[NSDate date]];
	
	NSDate *now = [NSDate date];
	
	for (int i = 0; i < 7; i++)
	{
		NSDate *date = [NSDate dateWithTimeInterval:+(i * (60 * 60 * 24)) sinceDate:now];
		
		if([dayName isEqualToString:[dateFormatter stringFromDate:date]])
		{
			[arrData addObject:[CommonMethod setDayStatus]];
		}else{
			[arrData addObject:[dateFormatter stringFromDate:date]];
		}
	}
	
	clgDay.delegate = self;
	clgDay.dataSource = self;
	
	[clgDay reloadData];
	
}

#pragma mark - ButtonAction
-(IBAction)menuBtnPressed:(id)sender{
	[CommonMethod hideLoader];
	[AppDel.slideMenuVC toggleMenu];
}
-(IBAction)downArrowBtnPressed:(id)sender{
	
	
	
	if([btnDown isSelected])
	{
		[self animationStop];
		[btnDown setSelected:NO];
		
	}else{
		
		[self animationStart];
		[btnDown setSelected:YES];
		[clgDay reloadData];
		
		
//		NSString *str = [[NSUserDefaults standardUserDefaults]objectForKey:@"user_id"] ;
//		
//		if(str.length != 0){
//			
//			
//			
//		}else{
//			
//			DisplayAlert(@"You are not login. Please login first");
//			
//		}
	}
}

- (IBAction)dayBtnPressed:(id)sender {
	
	if(btnDay.isSelected)
	{
		btnDay.selected = NO;
		
	}else
	{
		btnDay.selected = YES;

	}
	
	if(dictFilter.count > 0)
	{
		[dictFilter removeAllObjects];
	}
	
	[dictFilter setObject:strDaySelected forKey:@"week_day"];
	
	if([btnDay isSelected])
	{
		[dictFilter setObject:@"day" forKey:@"day_night"];
	}else if([btnNight isSelected])
	{
		[dictFilter setObject:@"night" forKey:@"day_night"];
	}else{
		//Do nothing.
	}
	
	[[NSNotificationCenter defaultCenter]postNotificationName:@"dayFilteration"object:self
													 userInfo:dictFilter];
	
	[btnDown setSelected:NO];
	
	[self animationStop];
	
}

- (IBAction)nightBtnPressed:(id)sender {
	
	if(btnNight.isSelected){
		btnNight.selected = NO;
	}else{
		btnNight.selected = YES;
	}
	
	if(dictFilter.count > 0){
		[dictFilter removeAllObjects];
	}
	
	[dictFilter setObject:strDaySelected forKey:@"week_day"];
	
	if([btnDay isSelected]){
		[dictFilter setObject:@"day" forKey:@"day_night"];
	}else if([btnNight isSelected]){
		[dictFilter setObject:@"night" forKey:@"day_night"];
	}else{
		//Do nothing.
	}
	
	[[NSNotificationCenter defaultCenter]postNotificationName:@"dayFilteration"object:self
													 userInfo:dictFilter];
	
	[btnDown setSelected:NO];
	
	[self animationStop];
	
	}


-(IBAction)searchBtnPressed:(id)sender{
	SearchVC *searchvc = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchVC"];
	searchvc.strTitle = lblTitle.text;
	[AppDel.slideMenuVC.mainViewController.navigationController pushViewController:searchvc animated:YES];
}

#pragma mark - CAPSPageMenuDelegate
-(void)willMoveToPage:(UIViewController *)controller index:(NSInteger)index{
	switch (index) {
		case 0:{
			[lblTitle setText:[CommonMethod setDayStatus]];
			[btnDown setHidden:NO];
			AppDel.isFromCategory = false;
		}
			break;
		case 2:{
			[lblTitle setText:@"Upcoming Events"];
			[btnDown setHidden:YES];
			AppDel.isFromCategory = false;
		}
			break;
		case 1:{
			[lblTitle setText:[CommonMethod setDayStatus]];
			[btnDown setHidden:NO];
			AppDel.isFromCategory = true;
		}
			
		default:
			break;
	}
}
-(void)animationStart
{
	_pageMenu.view.userInteractionEnabled = FALSE;
	
	//Animation for open
	[UIView animateWithDuration:0.5
						  delay:0.01
						options: UIViewAnimationOptionAllowUserInteraction
					 animations:^{
						 [self.view bringSubviewToFront:vwFilter];
						 [self.view bringSubviewToFront:navigationView];
						 filterViewTopConstraint.constant = filterViewTopConstraint.constant + filterViewHeightConstraint.constant;
						 [vwFilter layoutIfNeeded];
						 
					 }
					 completion:^(BOOL finished){
						 
					 }];
	
}
-(void)animationStop
{
	btnDown.userInteractionEnabled = NO;
	_pageMenu.view.userInteractionEnabled = TRUE;
	//[vwFilter setHidden:YES];
	[UIView animateWithDuration:0.5
						  delay:0.4
						options: UIViewAnimationOptionAllowUserInteraction
					 animations:^{
						 //							 [self.view bringSubviewToFront:navigationView];
						 filterViewTopConstraint.constant = filterViewTopConstraint.constant - filterViewHeightConstraint.constant;
						 [vwFilter layoutIfNeeded];
					 }
					 completion:^(BOOL finished){
						 btnDown.userInteractionEnabled = YES;
					 }];
	
}

-(void)didMoveToPage:(UIViewController *)controller index:(NSInteger)index{
	
	
}

#pragma mark - Receive Memort Warning,
- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
