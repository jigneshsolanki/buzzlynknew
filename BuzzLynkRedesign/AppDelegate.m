//
//  AppDelegate.m
//  BuzzLynkRedesign
//
//  Created by sotsys036 on 02/03/16.
//  Copyright © 2016 sotsys036. All rights reserved.
//

#import "AppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "IQKeyboardManager.h"
#import <GoogleMaps/GoogleMaps.h>
#import "Constants.h"

@interface AppDelegate ()

@end

@implementation AppDelegate;
@synthesize isFromUpcoming;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
	
	[self findLocation];
	
	isFromUpcoming = FALSE;
	
	//  [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
	
	//[[UIApplication sharedApplication] setStatusBarHidden:YES];
	
	[[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
	
//	[[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];
//	[UIApplication sharedApplication].keyWindow.frame=CGRectMake(0, 0, 320, 480);
//	
//	//[application setStatusBarStyle:UIStatusBarStyleBlackTranslucent];
	
	
    
    sleep(4);
    
    [self setSidePanel];
    //[self.navigationController setNavigationBarHidden:YES];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setSidePanelNotification)
                                                 name:@"FromSign"
                                               object:nil];
    
    [IQKeyboardManager sharedManager].enable = true;
    [FBSDKProfile enableUpdatesOnAccessTokenChange:YES];
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                    didFinishLaunchingWithOptions:launchOptions];
    
    //return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
	
	
	
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}

#pragma mark - CLLocationManager Delegate
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
	
    
    _crntLat = locations.lastObject.coordinate.latitude ;
    _crntLon = locations.lastObject.coordinate.longitude;
	
	
	latitude  = [NSNumber numberWithDouble:locations.lastObject.coordinate.latitude];
	longitude = [NSNumber numberWithDouble:locations.lastObject.coordinate.longitude];
	
	NSMutableDictionary *dict = [NSMutableDictionary new];
	[dict setObject:latitude forKey:@"lat"];
	[dict setObject:longitude forKey:@"lon"];
	[[NSUserDefaults standardUserDefaults] setObject:dict forKey:@"coordinates"];
	[[NSUserDefaults standardUserDefaults] synchronize];
	
	[locationmanager startUpdatingLocation];
	
   // [manager stopUpdatingLocation];
}

//-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
//	
//	if (status == kCLAuthorizationStatusDenied)
//     {
//		//location denied, handle accordingly
//		// NSLog(@"Dont allow");
//		 
//	}
//	else if (status == kCLAuthorizationStatusAuthorized)
//    {
//		//NSLog(@"Allow");
//		//hooray! begin startTracking
//		
//		
//	}
//	
//}
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    
    _crntLat = 0.0;
    _crntLon = 0.0;
    
    [manager stopUpdatingLocation];
}


#pragma mark - Private Method
-(void)findLocation{
	
	locationmanager = [[CLLocationManager alloc] init];
	locationmanager.delegate = self;
#ifdef __IPHONE_8_0
	if(IS_OS_8_OR_LATER) {
		[locationmanager requestAlwaysAuthorization];
	}
#endif
	[locationmanager startUpdatingLocation];
	crnLoc = [locationmanager location];
	longitude = [NSNumber numberWithDouble:crnLoc.coordinate.longitude];
	// NSLog(@"longitude%@",longitude);
	latitude = [NSNumber numberWithDouble:crnLoc.coordinate.latitude];
	//  NSLog(@"longitude%@",latitude);
	[locationmanager startUpdatingLocation];

	
}

- (void)locationManager:(CLLocationManager *)manager
	didUpdateToLocation:(CLLocation *)newLocation
		   fromLocation:(CLLocation *)oldLocation
{
	
	CLLocation *myLoc = newLocation;
	[locationmanager stopUpdatingLocation];
	latitude = [NSNumber numberWithDouble:myLoc.coordinate.latitude];
	longitude = [NSNumber numberWithDouble:myLoc.coordinate.longitude];

	NSMutableDictionary *dict = [NSMutableDictionary new];
	[dict setObject:latitude forKey:@"lat"];
	[dict setObject:longitude forKey:@"lon"];
	[[NSUserDefaults standardUserDefaults] setObject:dict forKey:@"coordinates"];
	[[NSUserDefaults standardUserDefaults] synchronize];

}


-(void)setSidePanel{
    self.slideMenuVC = [[HKSlideMenu3DController alloc] init];
    self.slideMenuVC.view.frame =  [[UIScreen mainScreen] bounds];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    
    _sideMenuObj = [mainStoryboard instantiateViewControllerWithIdentifier:@"LeftMenuVC"];
    _sideMenuObj.view.backgroundColor = [UIColor colorWithRed:(54.0/255.0) green:(106.0/255.0) blue:(159.0/255.0) alpha:1.0];
    //    navMain = (HKRotationNavigationController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"job_alert"];
    _navMainObj = [[HKRotationNavigationController alloc] initWithRootViewController:[mainStoryboard instantiateViewControllerWithIdentifier: @"ViewController"]];
    _navMainObj.navigationBarHidden = YES;
    
    self.slideMenuVC.menuViewController = _sideMenuObj;//side view
    
    self.slideMenuVC.mainViewController = _navMainObj;//center controller
    self.slideMenuVC.backgroundImage = [UIImage imageNamed:@""];
    self.slideMenuVC.backgroundImageContentMode = UIViewContentModeTopLeft;
    
    self.slideMenuVC.enablePan = NO;
    [self.navigationController setNavigationBarHidden:YES];
    self.navigationController = [[UINavigationController alloc]initWithRootViewController:self.slideMenuVC];
    [self.window setRootViewController:self.navigationController];
}
-(void)setSidePanelNotification{
    self.slideMenuVC = [[HKSlideMenu3DController alloc] init];
    self.slideMenuVC.view.frame =  [[UIScreen mainScreen] bounds];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    
    _sideMenuObj = [mainStoryboard instantiateViewControllerWithIdentifier:@"LeftMenuVC"];
    _sideMenuObj.view.backgroundColor = [UIColor colorWithRed:(54.0/255.0) green:(106.0/255.0) blue:(159.0/255.0) alpha:1.0];
    //    navMain = (HKRotationNavigationController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"job_alert"];
    _navMainObj = [[HKRotationNavigationController alloc] initWithRootViewController:[mainStoryboard instantiateViewControllerWithIdentifier: @"ViewController"]];
    _navMainObj.navigationBarHidden = YES;
    
    self.slideMenuVC.menuViewController = _sideMenuObj;//side view
    
    self.slideMenuVC.mainViewController = _navMainObj;//center controller
    self.slideMenuVC.backgroundImage = [UIImage imageNamed:@""];
    self.slideMenuVC.backgroundImageContentMode = UIViewContentModeTopLeft;
    
    self.slideMenuVC.enablePan = NO;
    [self.navigationController setNavigationBarHidden:YES];
    self.navigationController = [[UINavigationController alloc]initWithRootViewController:self.slideMenuVC];
    [self.window setRootViewController:self.navigationController];
}

@end
